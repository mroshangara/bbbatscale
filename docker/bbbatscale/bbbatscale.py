#!/usr/bin/env python3

import argparse
import os
import sys
from time import sleep
from typing import Optional

import django
from django.core import management
from django.core.management import BaseCommand, CommandError
from psycopg2 import OperationalError

from BBBatScale.settings import default_settings_module


def add_db_arguments(parser: argparse.ArgumentParser):
    parser.add_argument(
        "--db-conn-attempts",
        type=int,
        default=1,
        dest="database_connection_attempts",
        metavar="attempts",
        help="Specifies how many attempts should be made to connect to the database before aborting."
        " If the supplied number is less than one, the process will wait indefinitely"
        " until a connection could be established.",
    )
    parser.add_argument(
        "--db-conn-delay",
        type=int,
        default=5,
        dest="database_connection_delay",
        metavar="delay",
        help="Specifies the time to wait between two attempts.",
    )


def await_database_connection(attempts: Optional[int], delay: int) -> None:
    from django.db import connections

    if attempts < 1:
        attempts = None

    attempt = 0

    print("Try to connect to database", flush=True)
    while attempts is None or attempt < attempts:
        attempt += 1
        print("Attempt", attempt, flush=True)

        try:
            for connection in connections.all():
                connection.connect()
            print("Connection successfully established", flush=True)
            return
        except OperationalError:
            pass
        finally:
            for connection in connections.all():
                connection.close()

        if attempts is None or attempt < attempts:
            sleep(delay)

    print("Could not connect to the database", file=sys.stderr, flush=True)
    sys.exit(1)


def command_run(args: argparse.Namespace) -> None:
    import uvicorn
    from django.contrib.staticfiles.management.commands import collectstatic
    from django.core.management.commands import compilemessages, migrate

    await_database_connection(args.database_connection_attempts, args.database_connection_delay)

    try:
        if args.migrate:
            management.call_command(migrate.Command(), interactive=False)

        if args.collect_statics or args.collect_statics_clear:
            management.call_command(collectstatic.Command(), interactive=False, clear=args.collectstatic_clear)

        if args.compile_messages:
            management.call_command(compilemessages.Command())
    except CommandError as exc:
        print(exc.args, file=sys.stderr, flush=True)
        sys.exit(1)

    sys.exit(uvicorn.run("BBBatScale.asgi:application", host=args.host, port=args.port, loop="asyncio"))


def command_await_database_connection(args: argparse.Namespace) -> None:
    sys.exit(await_database_connection(args.database_connection_attempts, args.database_connection_delay))


def command_webhooks_worker(args: argparse.Namespace) -> None:
    from django_rq.management.commands import rqworker

    await_database_connection(args.database_connection_attempts, args.database_connection_delay)

    try:
        management.call_command(rqworker.Command(), "webhooks", with_scheduler=True)
        sys.exit()
    except CommandError as exc:
        print(exc.args, file=sys.stderr, flush=True)
        sys.exit(1)


def django_command(args: argparse.Namespace) -> None:
    if "database_connection_attempts" in args and "database_connection_delay" in args:
        await_database_connection(args.database_connection_attempts, args.database_connection_delay)
        delattr(args, "database_connection_attempts")
        delattr(args, "database_connection_delay")

    try:
        command = args.django_command
        delattr(args, "django_command")
        command.execute(**vars(args))
        sys.exit()
    except CommandError as exc:
        print(exc.args, file=sys.stderr, flush=True)
        sys.exit(1)


def create_django_parser(name: str, command: BaseCommand) -> argparse.ArgumentParser:
    parser = subparsers.add_parser(name, help=command.help, parser=command.create_parser(sys.argv[0], name))
    parser.set_defaults(subcommand=django_command, django_command=command)
    return parser


if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", default_settings_module)
    django.setup()

    root_parser = argparse.ArgumentParser()
    subparsers = root_parser.add_subparsers(
        required=True,
        metavar="command",
        parser_class=lambda parser=None, **kwargs: parser if parser else argparse.ArgumentParser(**kwargs),
    )

    run_parser = subparsers.add_parser("run")
    run_parser.set_defaults(subcommand=command_run)
    add_db_arguments(run_parser)
    run_parser.add_argument(
        "--migrate",
        action="store_true",
        help="Run migrations before proceeding.",
    )
    run_parser.add_argument(
        "--collect-statics",
        action="store_true",
        help="Collect statics before proceeding.",
    )
    run_parser.add_argument(
        "--collect-statics-clear",
        action="store_true",
        help="Clear and collect static files before proceeding.",
    )
    run_parser.add_argument(
        "--compile-messages",
        action="store_true",
        help="Compile messages before proceeding.",
    )
    run_parser.add_argument(
        "--host",
        default="0.0.0.0",
        help="The host address to bind to. Defaults to '0.0.0.0'.",
    )
    run_parser.add_argument(
        "--port",
        type=int,
        default=8000,
        help="The port to bind to. Defaults to '8000'.",
    )

    await_database_connection_parser = subparsers.add_parser(
        "await-database-connection", help="Wait until the database is ready."
    )
    await_database_connection_parser.set_defaults(subcommand=command_await_database_connection)
    add_db_arguments(await_database_connection_parser)

    from django.core.management.commands import migrate

    add_db_arguments(create_django_parser("migrate", migrate.Command()))

    from core.management.commands import tenant

    add_db_arguments(create_django_parser("tenant", tenant.Command()))

    from core.management.commands import fake_data

    add_db_arguments(create_django_parser("fake-data", fake_data.Command()))

    from core.management.commands import createsuperuser

    add_db_arguments(create_django_parser("create-superuser", createsuperuser.Command()))

    from django.contrib.staticfiles.management.commands import collectstatic

    create_django_parser("collect-statics", collectstatic.Command())

    from django.core.management.commands import compilemessages

    create_django_parser("compile-messages", compilemessages.Command())

    from core.management.commands import collect_all_room_occupancy

    add_db_arguments(create_django_parser("collect-all-room-occupancy", collect_all_room_occupancy.Command()))

    from core.management.commands import collect_server_stats

    add_db_arguments(create_django_parser("collect-server-stats", collect_server_stats.Command()))

    from core.management.commands import reset_stucked_rooms

    add_db_arguments(create_django_parser("reset-stucked-rooms", reset_stucked_rooms.Command()))

    from core.management.commands import house_keeping

    add_db_arguments(create_django_parser("house-keeping", house_keeping.Command()))

    from django_rq.management.commands import rqworker

    webhooks_worker_parser = subparsers.add_parser("webhooks-worker", help=rqworker.Command.help)
    webhooks_worker_parser.set_defaults(subcommand=command_webhooks_worker)
    add_db_arguments(webhooks_worker_parser)

    from core.management.commands import clean_media_files

    add_db_arguments(create_django_parser("clean-media-files", clean_media_files.Command()))

    from core.management.commands import delete_recordings

    add_db_arguments(create_django_parser("delete-recordings", delete_recordings.Command()))

    from support_chat.management.commands import inactivate_all_supporters

    add_db_arguments(create_django_parser("inactivate-all-supporters", inactivate_all_supporters.Command()))

    namespace = root_parser.parse_args()
    subcommand = namespace.subcommand
    delattr(namespace, "subcommand")
    if subcommand:
        subcommand(namespace)
    else:
        root_parser.print_help()
        root_parser.exit(1)

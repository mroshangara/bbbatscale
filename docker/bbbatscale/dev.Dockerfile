ARG BBBATSCALE_IMAGE

FROM $BBBATSCALE_IMAGE

ENV DJANGO_SETTINGS_MODULE=BBBatScale.settings.no_database

USER root:root

RUN cd / && pipenv install --system --deploy --dev

CMD ["bash"]

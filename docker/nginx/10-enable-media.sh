#!/usr/bin/env sh
set -e

# Remove possibly previously created files.
rm -f /etc/nginx/templates-include/locations_media.template /etc/nginx/include/locations_media

# Check if variable is defined (empty values will also pass).
if [ -n "${BBBATSCALE_MEDIA_SECRET_KEY+variableIsDefined}" ]; then
  cp /etc/nginx/templates-include/locations_media.template.disabled /etc/nginx/templates-include/locations_media.template
else
  touch /etc/nginx/include/locations_media
fi

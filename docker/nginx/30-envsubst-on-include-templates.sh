#!/usr/bin/env sh
set -e

NGINX_ENVSUBST_TEMPLATE_DIR=/etc/nginx/templates-include \
  NGINX_ENVSUBST_OUTPUT_DIR=/etc/nginx/include \
  . /docker-entrypoint.d/20-envsubst-on-templates.sh

#!/usr/bin/env sh
set -e

config="${NGINX_CONFIG:-http}"
confDir="/etc/nginx/conf.d-available/$config"

if ! [ -d "$confDir" ]; then
  echo NGINX_CONFIG is invalid. You have to specify a valid configuration. Available configurations are: >&2
  ls -x /etc/nginx/conf.d-available/ >&2
  exit 1
fi

rm -rf /etc/nginx/conf.d/*.conf
ln -s "$confDir"/*.conf /etc/nginx/conf.d/

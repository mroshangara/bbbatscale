{{/* render template values */}}
{{- define "rendertemplate" -}}
{{ if kindIs "string" .value -}}
{{ tpl .value .context }}
{{- else -}}
{{ tpl (.value | toYaml) .context }}
{{- end }}
{{- end }}

{{/* BBB@Scale returns the supplied secret or string as value for an env variable entry */}}
{{- define "bbbatscale.asEnvValueFromSecretOrString" -}}
{{ if kindIs "string" . -}}
value: {{ . | quote }}
{{- else -}}
valueFrom:
  secretKeyRef:
    name: {{ .secretName | quote }}
    key: {{ .secretKey | quote }}
{{- end }}
{{- end }}

{{/* BBB@Scale database name as value for an env variable entry */}}
{{- define "bbbatscale.database.database.asEnvValue" -}}
{{ if .Values.postgresql.enabled -}}
value: {{ .Values.postgresql.postgresqlDatabase | quote }}
{{- else if (index .Values "postgresql-ha" "enabled") -}}
value: {{ (index .Values "postgresql-ha" "postgresql" "database") | quote }}
{{- else }}
{{- include "bbbatscale.asEnvValueFromSecretOrString" (index .Values "postgresql-extern" "database") }}
{{- end }}
{{- end }}

{{/* BBB@Scale database host as value for an env variable entry */}}
{{- define "bbbatscale.database.host.asEnvValue" -}}
{{ if .Values.postgresql.enabled -}}
value: {{ printf "%s-postgresql" .Release.Name | quote }}
{{- else if (index .Values "postgresql-ha" "enabled") -}}
value: {{ printf "%s-postgresql-ha-pgpool" .Release.Name | quote }}
{{- else }}
{{- include "bbbatscale.asEnvValueFromSecretOrString" (index .Values "postgresql-extern" "host") }}
{{- end }}
{{- end }}

{{/* BBB@Scale database password as value for an env variable entry */}}
{{- define "bbbatscale.database.password.asEnvValue" -}}
{{ if .Values.postgresql.enabled -}}
valueFrom:
  secretKeyRef:
    name: {{ printf "%s-postgresql" .Release.Name | quote }}
    key: postgresql-password
{{- else if (index .Values "postgresql-ha" "enabled") -}}
valueFrom:
  secretKeyRef:
    name: {{ printf "%s-postgresql-ha-postgresql" .Release.Name | quote }}
    key: postgresql-password
{{- else }}
{{- include "bbbatscale.asEnvValueFromSecretOrString" (index .Values "postgresql-extern" "password") }}
{{- end }}
{{- end }}

{{/* BBB@Scale database port as value for an env variable entry */}}
{{- define "bbbatscale.database.port.asEnvValue" -}}
{{ if .Values.postgresql.enabled -}}
value: {{ .Values.postgresql.service.port | quote }}
{{- else if (index .Values "postgresql-ha" "enabled") -}}
value: {{ (index .Values "postgresql-ha" "service" "port") | quote }}
{{- else }}
{{- with (index .Values "postgresql-extern" "port") | default 5432 }}
{{- if (kindIs "int" .) -}}
value: {{ . | quote }}
{{- else }}
{{- include "bbbatscale.asEnvValueFromSecretOrString" . }}
{{- end }}
{{- end }}
{{- end }}
{{- end }}

{{/* BBB@Scale database username as value for an env variable entry */}}
{{- define "bbbatscale.database.username.asEnvValue" -}}
{{ if .Values.postgresql.enabled -}}
value: {{ .Values.postgresql.postgresqlUsername | quote }}
{{- else if (index .Values "postgresql-ha" "enabled") -}}
value: {{ (index .Values "postgresql-ha" "postgresql" "username") | quote }}
{{- else }}
{{- include "bbbatscale.asEnvValueFromSecretOrString" (index .Values "postgresql-extern" "username") }}
{{- end }}
{{- end }}

{{/* BBB@Scale environment variables */}}
{{- define "bbbatscale.env" -}}
- name: POSTGRES_DB
  {{- include "bbbatscale.database.database.asEnvValue" . | nindent 2 }}
- name: POSTGRES_USER
  {{- include "bbbatscale.database.username.asEnvValue" . | nindent 2 }}
- name: POSTGRES_PASSWORD
  {{- include "bbbatscale.database.password.asEnvValue" . | nindent 2 }}
- name: POSTGRES_HOST
  {{- include "bbbatscale.database.host.asEnvValue" . | nindent 2 }}
- name: POSTGRES_PORT
  {{- include "bbbatscale.database.port.asEnvValue" . | nindent 2 }}
- name: REDIS_HOST
  value: {{ .Release.Name }}-redis-master
- name: REDIS_PORT
  value: {{ .Values.redis.master.service.port | quote }}
- name: REDIS_PASSWORD
  valueFrom:
    secretKeyRef:
      name: {{ .Release.Name }}-redis
      key: redis-password
- name: DJANGO_SECRET_KEY
  valueFrom:
    secretKeyRef:
      name: bbbatscale-secret
      key: djangoSecretKey
- name: RECORDINGS_SECRET
  valueFrom:
    secretKeyRef:
      name: bbbatscale-secret
      key: recordingsSecret
- name: DJANGO_SETTINGS_MODULE
  value: {{ .Values.bbbatscale.djangoSettingsModule }}
- name: SECURE_PROXY_SSL_HEADER
  value: HTTP_X_FORWARDED_PROTO
{{ if .Values.bbbatscale.webhooks.enabled -}}
- name: WEBHOOKS
  value: enabled
{{- end }}
{{ if .Values.bbbatscale.media.enabled -}}
- name: BBBATSCALE_MEDIA
  value: enabled
- name: BBBATSCALE_MEDIA_URL_EXPIRY_SECONDS
  value: {{ .Values.bbbatscale.media.expirySeconds | quote }}
- name: BBBATSCALE_MEDIA_SECRET_KEY
  valueFrom:
    secretKeyRef:
      name: bbbatscale-secret
      key: bbbatscaleMediaSecretKey
{{- end }}
{{- end }}

{{/* BBB@Scale config volumes */}}
{{- define "bbbatscale.configVolumes" -}}
- name: bbbatscale-config
  configMap:
    name: bbbatscale-configmap
    defaultMode: 0444
{{- end }}

{{/* BBB@Scale config volume mounts */}}
{{- define "bbbatscale.configVolumeMounts" -}}
{{ if .Values.bbbatscale.logging.enabled -}}
- name: bbbatscale-config
  readOnly: true
  mountPath: /bbbatscale/BBBatScale/settings/logging_config.py
  subPath: loggingConfig
{{- end }}
{{- end }}

{{/* BBB@Scale media volume */}}
{{- define "bbbatscale.mediaVolume" -}}
{{ if .Values.bbbatscale.media.enabled -}}
- name: bbbatscale-media
  persistentVolumeClaim:
    {{ if kindIs "string" .Values.bbbatscale.media.pvc -}}
    claimName: {{ include "rendertemplate" (dict "value" .Values.bbbatscale.media.pvc "context" .) | quote }}
    {{- else -}}
    claimName: bbbatscale-media-pvc
    {{- end }}
{{- end }}
{{- end }}

{{/* BBB@Scale media volume mount */}}
{{- define "bbbatscale.mediaVolumeMount" -}}
{{ if .Values.bbbatscale.media.enabled -}}
- name: bbbatscale-media
  readOnly: {{ ternary "true" "false" (default false .nginx) }}
  mountPath: {{ ternary "/var/www/bbbatscale/media/" "/bbbatscale/media/" (default false .nginx) }}
  {{ with .Values.bbbatscale.media.subPath -}}
  subPath: {{ . | quote }}
  {{- end }}
{{- end }}
{{- end }}

{{/* BBB@Scale database backup volume */}}
{{- define "bbbatscale.databaseBackupVolume" -}}
- name: bbbatscale-database-backup
  persistentVolumeClaim:
    {{ if kindIs "string" .pvc -}}
    claimName: {{ include "rendertemplate" (dict "value" .pvc "context" $) | quote }}
    {{- else -}}
    claimName: bbbatscale-database-backup-pvc
    {{- end }}
{{- end }}

{{/* BBB@Scale database backup mount */}}
{{- define "bbbatscale.databaseBackupVolumeMount" -}}
- name: bbbatscale-database-backup
  mountPath: /bbbatscale/database-backup/
  {{ with .subPath -}}
  subPath: {{ . | quote }}
  {{- end }}
{{- end }}

{{/* Common Labels */}}
{{- define "commonLabels" -}}
app.kubernetes.io/managed-by: {{ .Release.Service | quote }}
app.kubernetes.io/instance: {{ .Release.Name | quote }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
helm.sh/chart: {{ printf "%s-%s" .Chart.Name (.Chart.Version | replace "+" "_") | quote }}
helm.sh/revision: {{ .Release.Revision | quote }}
{{- end }}

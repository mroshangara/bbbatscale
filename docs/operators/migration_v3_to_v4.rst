##############################
Migration Path from 3.X to 4.0
##############################

In the 4.0 release we changed the internal handling of rooms and
meetings. Furthermore, we dropped the support of managing and serving
your recordings with the standard BBB recordings setup. Since BBB
Recordings handling does not scale well, we decided to create our own
setup for storing and delivering recordings for both internal and
external usage. If you did not use our previous (not updated) recordings
setup with `BBB2mp4`_ or any of
your own recording setup, you don’t have to worry that this upgrade will
break anything for you. If you used an up and running solution for
recordings within BBB@Scale you have a couple of options. For detailed
information read the migration section.

*********************
Updating your project
*********************

For updating please read `GettingStarted.md update section`_.

.. note::
   The migration process could take a while if you have many meetings in
   the database, so do not worry if it takes a few minutes depending on
   your used hardware and amount of meetings.

.. admonition:: Meeting configurations linked to all tenants
   :class: note

   After migrating to version 4, the meeting configurations will be linked to
   one or more tenants. Since in version 3 all configurations where accessible
   by every tenant, all configurations will be linked to all tenants. This
   can be changed after the migration.

.. admonition:: No meetings in the overview
   :class: warning

   After migrating to version 4, the meetings will internally be changed
   and in the future be connected to one or more tenants. Since the 3.X
   meetings do not provide enough information to link them to their tenant(s),
   you have to do this manually by your own if you need them to show up in the
   meetings overview of the corresponding tenant.

**********
Recordings
**********

Our new recordings setup uses two components:
`bbbatscale-agent`_ & `recman`_. We will post a guide
for setting up the `agent`:code: and `recman`:code: in the upcoming weeks on
their specific gitlab pages.

***********************
Setup explained (short)
***********************

Our `bbbatscale-agent`:code: currently consists of two main features. It
registers servers for scheduling and handles processed recordings. As
soon as a recording finished processing, the agent packs it up inside a
`zip`:code: and moves it into a shared file storage (currently only
supporting `CephFS`:code:). At this point `recman`:code: picks up and manages
all recordings send by the agent. For replaying we use the
`bbb-player`_.

****************
bbbatscale-agent
****************

Currently our `bbbatscale-agent`:code: provides two features, and we plan to
extend the feature set in the future. Beside the convenience of
registering your server within BBB@Scale, the agent plays an essential
role in our recording setup. We provide a complete guide for setting up
the `bbbatscale-agent`_.

******
recman
******

`recman`:code: consists of two parts - `recman`:code: and `recserve`:code:. With
`recman`:code: we provide a way to manage your recordings outside the actual
BBB server, which allows you to build a large scaled recording setup.
After a recording is finished and stored properly `recserve`:code: takes
over and serves it. We provide a complete guide for setting up
`recman`_.

************************
Migrating old recordings
************************

Currently the only tested approach to migrate your recordings from the
old BBB server stored version to our newly created version, is to
re-process your recordings with an installed and setup of
`bbbatscale-agent`:code: and `recman`:code:. Please read the sections and guide for
setting up these components.

.. _BBB2mp4: https://gitlab.com/bbbatscale/bbb2mp4
.. _`GettingStarted.md update section`: ../GettingStarted.md#update
.. _bbbatscale-agent: https://gitlab.com/bbbatscale/bbbatscale-agent
.. _recman: https://gitlab.com/bbbatscale/recman
.. _bbb-player: https://gitlab.com/bbbatscale/bbb-playback

#################
Development setup
#################

This document is a quick guide to set up an BBB@Scale development environment.

*************
Prerequisites
*************

Make sure the following tools are installed on the machine:

* `docker`_
* `docker-compose`_
* `python`_ with the version specified in `Pipfile`:code:
* `pip`_
* `pipenv`_

.. note::
   Paths in this document are stated relative to the repository root folder.

**************
Docker compose
**************
BBB@Scale relies on additional services like nginx, redis and PostgreSQL.
These services are containerized and managed via docker-compose in order to
create a production-like environment locally.

In the docker-compose file `docker/development/docker-compose.yml`:code:
are all needed services defined.

PostgreSQL
==========

1. Copy `configuration/env/postgres.env.dummy`:code: to
   `configuration/env/postgres.env`:code:
2. You may change the values of the environment variables, but it is not
   needed.

Nginx
=====

3. Copy `configuration/env/nginx.env.dummy`:code: to
   `configuration/env/nginx.env`:code:
4. You may change the values of the environment variables.
   It is recommended to change `NGINX_SERVER_NAME=example.org`:code: to
   `NGINX_SERVER_NAME=_`:code:.

Run docker-compose
==================

5. Run the following command to start docker-compose:
   `docker-compose -f docker/development/docker-compose.yml up`:code:

*************
PyCharm setup
*************


Project structure
=================

PyCharm supports django projects but requires a defined project structure to
evaluate modules. Therefore, mark your project folders as follows:

**Source Folders**

* `BBBatScale`:code:

**Template Folders**

* `BBBatScale/templates`:code:
* `BBBatScale/core/templates`:code:
* `BBBatScale/support_chat/templates`:code:

Pipenv
======

PyCharm is able to make use of pipenv. This enables the use of a pipenv based
interpreter and updating dependencies automatically. You may follow along with
the sections below or visit the official `PyCharm pipenv`_ instructions.

Configuration
-------------

1. Open the settings and navigate to ``Tools -> Python Integrated Tools``

2. Check whether PyCharm has the correct path to the pipenv executable.

Interpreter settings
--------------------

3. In the settings navigate to ``Project: BBBatScale -> Python Interpreter``.
   Click on the gear and click ``Add...``.

4. In the opened dialog select ``Pipenv Environment``.
   Check the paths and ensure that ``Install packages from Pipfile`` is
   checked. Click ``Ok``.

   At this point PyCharm uses pipenv to create the virtual environment and
   install dependencies. It also activates a pipenv shell for the in PyCharm's
   integrated terminal.

5. Check whether the added interpreter is used by PyCharm.
   On the bottem right corner you should see something like
   ``Pipenv (BBBatScale) [Python 3.9.X]``


Run configuration
=================

To actually run the project you can configure a *Run/Debug Configuration* as
follows:

Environment variables
---------------------

You can copy and paste following string as the environment variables of the
configuration:

`PYTHONUNBUFFERED=1;DJANGO_SETTINGS_MODULE=BBBatScale.settings.development`:code:

At this point you could also enable different features like the webhooks or
media to be available via the environment variables `WEBHOOKS=enabled`:code: and
`BBBATSCALE_MEDIA=enabled`:code:

Working directory
-----------------
The working directory should be set to `BBBatScale/`:code: (as an absolute
path).

Run
---

At this point you should be able to run the configuration by clicking the play
button in the upper right corner of PyCharm.

************
Initial data
************

It is highly advisable to configure the database and fill it with data. Run the
following commands from your shell with **active pipenv** to set it up:

1. Apply all schema changes:

   `BBBatScale/manage.py migrate`:code:

2. Create and store initial fake data:

   `BBBatScale/manage.py fake_data --create --personalrooms --homerooms --admin localhost`:code:

   It creates an account with the username and password both being set to
   `admin`:code: as well as a tenant with the domain set to `localhost`:code:.

**************************
Start developing BBB@Scale
**************************

After following all previous steps you should be able to access BBB@Scale in
your browser by simply opening `<http://localhost:8080/>`__.

**Happy Hacking!**


.. _docker: https://docs.docker.com/get-docker/
.. _docker-compose: https://docs.docker.com/compose/install/
.. _python: https://www.python.org/
.. _pip: https://pip.pypa.io/en/stable/
.. _pipenv: https://pipenv.pypa.io/en/stable/
.. _PyCharm pipenv: https://www.jetbrains.com/help/pycharm/pipenv.html

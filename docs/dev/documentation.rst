##############
BBB@Scale docs
##############

The |docs|_ directory in `BBB@Scale repo`_ is the source behind `<https://docs.bbbatscale.de/>`__.

*******
Editing
*******

To edit the documentation you need a GitLab account. Once you have created one
and logged in, you can edit any page by navigating to the corresponding file and
clicking the *Edit in Web IDE* button. This will create a so called "fork" and a "merge
request", which will be approved by one of the team members.

..
   TODO: uncomment the following lines as soon as a Sphinx setup has been created.

   In the long run, learning to use Git_ and running Sphinx_ on your computer is
   beneficial.

   After following the contribution setup instructions the first steps to run it locally are::

      cd docs
      make html
      # open _build/html/index.html

***********
Style guide
***********

As the style guide for our documentation we stick to the `python style guides`_,
although the `affirmative tone`_ is not that strict. We do not encourage to write
warning boxes, but if necessary they are permitted, however an issue has to be
created to tackle the problem described by that warning box if that problem is
not desired or could be solved a more UX friendly way. For example if
checking a checkbox in the UI only takes effect if another one is also checked,
this could be marked with a warning, **but** in this case a corresponding issue
has to be written.

Since section types in reST are normally not assigned to a certain characters
as structure is determined from the succession of headings, we here also stick
to the `python section convention`_

*********
Structure
*********

The documentation is divided into an index page (`index.rst`:code:) and various
subsections. The sections are:

- Introductory information in `intro`:code:.
- Information for operators in `operators`:code:.
- Information for users in `users`:code:.
- Information for developers in `dev`:code:.

The documentation uses the `rst format`_. For a starting point check out the
`reStructuredText Primer`_.


.. |docs| replace:: `/docs`:code:
.. _docs: https://gitlab.com/bbbatscale/bbbatscale/-/tree/main/docs
.. _`BBB@Scale repo`: https://gitlab.com/bbbatscale/bbbatscale
.. _Git: https://www.git-scm.com/
.. _Sphinx: https://www.sphinx-doc.org/
.. _`python style guides`: https://devguide.python.org/documenting/#style-guide
.. _`affirmative tone`: https://devguide.python.org/documenting/#affirmative-tone
.. _`python section convention`: https://devguide.python.org/documenting/#sections
.. _`rst format`: https://docutils.sourceforge.io/docs/ref/rst/restructuredtext.html
.. _`reStructuredText Primer`: https://www.sphinx-doc.org/en/master/usage/restructuredtext/basics.html


import os

from django.conf import settings
from django.core.wsgi import get_wsgi_application

from BBBatScale.settings import default_settings_module

os.environ.setdefault("DJANGO_SETTINGS_MODULE", default_settings_module)
application = get_wsgi_application()

if settings.DEBUG:
    try:
        import django.views.debug  # noqa
    except ImportError:
        pass

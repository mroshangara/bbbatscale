import os

os.environ.setdefault("WEBHOOKS", "enabled")
os.environ.setdefault("BBBATSCALE_MEDIA", "enabled")

from .development import *  # noqa: F401,F403,E402

import os

from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/3.0/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = os.environ["DJANGO_SECRET_KEY"]

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

ALLOWED_HOSTS = ["*"]

RECORDINGS_SECRET = os.environ["RECORDINGS_SECRET"]

DEFAULT_AUTO_FIELD = "django.db.models.BigAutoField"

# Allow a proxy, which handles TLS, to tell Django it has been contacted using HTTPS by using a
# HTTP-Header-Line (e.g. by setting a header called X-Forwarded-Proto to https)
if "SECURE_PROXY_SSL_HEADER" in os.environ.keys():
    SECURE_PROXY_SSL_HEADER = (os.environ["SECURE_PROXY_SSL_HEADER"], "https")

LOGIN_URL = reverse_lazy("login")
LOGOUT_URL = reverse_lazy("logout")

LOGIN_REDIRECT_URL = "/"
LOGOUT_REDIRECT_URL = "/"

# Application definition

WEBHOOKS_ENABLED = os.environ.get("WEBHOOKS", "").lower() == "enabled"

INSTALLED_APPS = [
    "core",
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "django.contrib.sites",
    "crispy_forms",
    "django_guid",
    "support_chat",
    "channels",
    "oidc_authentication",
]

if WEBHOOKS_ENABLED:
    INSTALLED_APPS.append("django_rq")

MIDDLEWARE = [
    "django_guid.middleware.guid_middleware",
    "django.middleware.security.SecurityMiddleware",
    "whitenoise.middleware.WhiteNoiseMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.locale.LocaleMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
    "oidc_authentication.middleware.TenantSessionRefresh",
]

ROOT_URLCONF = "BBBatScale.urls"

WSGI_APPLICATION = "BBBatScale.wsgi.application"
ASGI_APPLICATION = "BBBatScale.routing.application"

# Database
# https://docs.djangoproject.com/en/3.0/ref/settings/#databases


# Password validation
# https://docs.djangoproject.com/en/3.0/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.MinimumLengthValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.CommonPasswordValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.NumericPasswordValidator",
    },
]

# Internationalization
# https://docs.djangoproject.com/en/3.0/topics/i18n/

LANGUAGE_CODE = "en"

LANGUAGES = (
    ("en", _("English")),
    ("de", _("German")),
    ("it", _("Italian")),
    ("es", _("Spanish")),
    ("gl", _("Galician")),
    ("ru", _("Russian")),
    ("cs", _("Czech")),
)

TIME_ZONE = "UTC"

USE_I18N = True

USE_L10N = True

USE_TZ = True

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [os.path.join(BASE_DIR, "templates")],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
                "core.context_processors.general_parameter",
                "core.context_processors.auth_urls",
                "core.context_processors.is_enabled",
                "support_chat.context_processors.support_chat_parameter",
            ],
        },
    },
]

STATIC_ROOT = os.path.join(BASE_DIR, "staticfiles")

STATIC_URL = "/static/"

STATICFILES_FINDERS = (
    "django.contrib.staticfiles.finders.FileSystemFinder",
    "django.contrib.staticfiles.finders.AppDirectoriesFinder",
)

STATICFILES_DIRS = (os.path.join(BASE_DIR, "static"),)

CRISPY_TEMPLATE_PACK = "bootstrap4"

EVENT_COLLECTION_SYNC_SYNC_HOURS = 24

LOCALE_PATHS = (
    os.path.join(BASE_DIR, "locale"),
    os.path.join(BASE_DIR, "support_chat/locale"),
)

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql",
        "NAME": os.environ["POSTGRES_DB"],
        "USER": os.environ["POSTGRES_USER"],
        "PASSWORD": os.environ["POSTGRES_PASSWORD"],
        "HOST": os.environ["POSTGRES_HOST"],
        "PORT": int(os.environ.setdefault("POSTGRES_PORT", "5432")),
        "ATOMIC_REQUESTS": True,
        "OPTIONS": {
            "connect_timeout": 5,
        },
    },
}

CHANNEL_LAYERS = {
    "default": {
        "BACKEND": "channels_redis.core.RedisChannelLayer",
        "CONFIG": {
            "hosts": [
                {
                    "address": (os.environ["REDIS_HOST"], int(os.environ.setdefault("REDIS_PORT", "6379"))),
                    "db": int(os.environ.setdefault("REDIS_DATABASE", "0")),
                    "password": os.getenv("REDIS_PASSWORD", None),
                },
            ],
        },
    },
}

if WEBHOOKS_ENABLED:
    RQ_QUEUES = {
        "webhooks": {
            "HOST": os.environ["REDIS_HOST"],
            "PORT": int(os.environ.setdefault("REDIS_PORT", "6379")),
            "DB": int(os.environ.setdefault("WEBHOOKS_REDIS_DATABASE", "1")),
            "PASSWORD": os.getenv("REDIS_PASSWORD", None),
        }
    }

DJANGO_GUID = {
    "GUID_HEADER_NAME": "Correlation-ID",
    "VALIDATE_GUID": True,
    "RETURN_HEADER": True,
    "EXPOSE_HEADER": True,
    "INTEGRATIONS": [],
}

LOGGING = {}

try:
    from .logging_config import LOGGING as CUSTOM_LOGGING  # noqa: F401,F403

    LOGGING = CUSTOM_LOGGING

except ImportError:
    log_format = (
        "%(levelno)s, "
        "%(levelname)s, "
        "%(asctime)s, "
        "%(correlation_id)s, "
        "%(module)s, "
        "%(filename)s, "
        "%(funcName)s, "
        "%(lineno)s, "
        "%(process)s, "
        "%(processName)s, "
        "%(thread)s, "
        "%(threadName)s, "
        "%(message)s"
    )

    LOGGING = {
        "version": 1,
        "disable_existing_loggers": False,
        "filters": {"correlation_id": {"()": "django_guid.log_filters.CorrelationId"}},
        "formatters": {
            "verbose": {
                "format": log_format,
            },
            "json_verbose": {
                "()": "pythonjsonlogger.jsonlogger.JsonFormatter",
                "format": log_format,
            },
        },
        "handlers": {
            "console": {
                "class": "logging.StreamHandler",
                "formatter": "verbose",
                "filters": ["correlation_id"],
            },
            "console_json": {
                "class": "logging.StreamHandler",
                "formatter": "json_verbose",
                "filters": ["correlation_id"],
            },
        },
        "root": {
            "handlers": ["console"],
            "level": "INFO",
        },
    }

AUTH_USER_MODEL = "core.User"

MODERATORS_GROUP = "moderator"

AUTHENTICATION_BACKENDS = [
    "django.contrib.auth.backends.ModelBackend",
    "oidc_authentication.auth.TenantOIDCAuthenticationBackend",
]

USE_THOUSAND_SEPARATOR = True

BBBATSCALE_MEDIA_ENABLED = os.environ.get("BBBATSCALE_MEDIA", "").lower() == "enabled"
if BBBATSCALE_MEDIA_ENABLED:
    MEDIA_ROOT = os.path.join(BASE_DIR, "media")
    MEDIA_URL = "/media/"
    BBBATSCALE_MEDIA_URL_EXPIRY_SECONDS = int(os.environ.get("BBBATSCALE_MEDIA_URL_EXPIRY_SECONDS", 30))
    BBBATSCALE_MEDIA_SECRET_KEY = os.environ["BBBATSCALE_MEDIA_SECRET_KEY"]

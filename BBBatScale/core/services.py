import json
import logging
import re
import time
import uuid
from dataclasses import dataclass, field
from datetime import timedelta
from hashlib import sha1
from pathlib import Path
from typing import Callable, Dict, Iterable, List, Optional, Tuple
from urllib import parse

import grpc
import xmltodict
from core.constants import (
    GUEST_POLICY_APPROVAL,
    MEETING_STATE_FINISHED,
    MEETING_STATE_RUNNING,
    MODERATION_MODE_ALL,
    MODERATION_MODE_MODERATORS,
    MODERATION_MODE_STARTER,
    ROOM_STATE_ACTIVE,
    ROOM_STATE_INACTIVE,
    SERVER_STATE_DISABLED,
    SERVER_STATE_ERROR,
    SERVER_STATE_UP,
)
from core.event_collectors.base import EventCollectorContext
from core.models import (
    AgentConfiguration,
    ApiToken,
    GeneralParameter,
    HomeRoom,
    Meeting,
    MeetingConfiguration,
    PersonalRoom,
    Room,
    RoomEvent,
    SchedulingStrategy,
    Server,
    ServerType,
    User,
)
from core.utils import BigBlueButton, RecordingsGRPC
from django.conf import settings
from django.contrib.sites.models import Site
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.db import IntegrityError, transaction
from django.db.models import OuterRef, Q, Subquery
from django.db.models.functions import Coalesce
from django.urls import reverse
from django.utils import timezone
from django.utils.translation import gettext
from xmltodict import unparse

logger = logging.getLogger(__name__)

AgentConfigurationSelector = Callable[[], Optional[AgentConfiguration]]
AgentIntervalSelector = Callable[[AgentConfiguration], Optional[timedelta]]


def get_interval(
    agent_config_selector: AgentConfigurationSelector, interval_select: AgentIntervalSelector
) -> Optional[timedelta]:
    agent_config = agent_config_selector()
    if agent_config:
        return interval_select(agent_config)
    return None


def apitoken_agent_config_selector(communication_token: ApiToken) -> AgentConfigurationSelector:
    def by_token():
        ss = communication_token.scheduling_strategy
        return ss.agent_configuration if ss else None

    return by_token


def select_registration_interval(agent_config: AgentConfiguration) -> Optional[timedelta]:
    return agent_config.registration_interval


def select_stats_interval(agent_config: AgentConfiguration) -> Optional[timedelta]:
    return agent_config.stats_interval


def moderator_message(request, name, access_code, only_prompt_guests_for_access_code, guest_policy, max_participants):
    logger.debug(
        f"Moderator message will be created with following parameters: name: {name}, access_code: {access_code},"
        f" only_prompt_guests_for_access_code: {only_prompt_guests_for_access_code}, guest_policy: {guest_policy},"
        f" maxParticipants: {max_participants}"
    )
    join_url = request.build_absolute_uri(reverse("join_redirect", args=[name]))
    message = gettext("Meeting link: {}").format(join_url)
    if access_code:
        if only_prompt_guests_for_access_code:
            ac_message = gettext("This room is protected by an access code only for guests: {}").format(access_code)
        else:
            ac_message = gettext("This room is protected by an access code: {}").format(access_code)
    else:
        ac_message = None
    gp_message = gettext("The room has a guest lobby enabled") if guest_policy == GUEST_POLICY_APPROVAL else None
    max_participants_message = (
        gettext("The room is limited to {} participants").format(max_participants) if max_participants else None
    )
    logger.debug("Moderator message created.")
    return "<br/>".join(filter(None, [message, ac_message, gp_message, max_participants_message]))


def create_web_hook_bbb(request, bbb):
    logger.debug("Creating web hook for {} on {}.".format(bbb.url, request.get_host()))
    web_hook_params = {"callbackURL": request.build_absolute_uri(reverse("callback_bbb"))}
    return bbb.create_web_hook(web_hook_params)


def create_meeting_and_bbb_parameter(request, meeting, create_parameter):
    try:
        if meeting.external_meeting:
            logger.debug("Creating parameter for external BBB meeting")
            create_parameter.update(
                {
                    "meetingID": meeting.id,
                    "meta_creator": meeting.creator,
                    "meta_roomsmeetingid": meeting.pk,
                    "meta_bbb-origin": "BBB@Scale",
                }
            )
            logger.debug(f"External meeting parameter created: {create_parameter}.")
            return create_parameter
        logger.debug("Creating parameter for BBB Meeting.")

        logger.debug(f"Meeting created for {meeting.room_name} by {meeting.creator}.")
        meeting_config: MeetingConfiguration = meeting.configuration
        _create_parameter = {
            "name": meeting.room_name,
            "meetingID": meeting.id,
            "attendeePW": meeting.attendee_pw,
            "moderatorPW": meeting.moderator_pw,
            "logoutURL": meeting_config.logoutUrl
            if meeting_config.logoutUrl
            else request.build_absolute_uri(reverse("home")),
            "muteOnStart": "true" if meeting_config.mute_on_start else "false",
            "moderatorOnlyMessage": moderator_message(
                request,
                meeting.room_name,
                meeting_config.access_code,
                meeting_config.only_prompt_guests_for_access_code,
                meeting_config.guest_policy,
                meeting_config.maxParticipants,
            ),
            "lockSettingsDisableCam": "true" if meeting_config.disable_cam else "false",
            "lockSettingsDisableMic": "true" if meeting_config.disable_mic else "false",
            "lockSettingsDisableNote": "true" if meeting_config.disable_note else "false",
            "lockSettingsDisablePublicChat": "true" if meeting_config.disable_public_chat else "false",
            "lockSettingsDisablePrivateChat": "true" if meeting_config.disable_private_chat else "false",
            "allowModsToUnmuteUsers": "true" if meeting_config.allow_unmuteusers else "false",
            "allowModsToEjectCameras": "true" if meeting_config.allow_ejectcam else "false",
            "learningDashboardEnabled": "true" if meeting_config.allow_learningdashboard else "false",
            "meetingLayout": meeting_config.meetingLayout,
            "guestPolicy": meeting_config.guest_policy,
            "record": "true" if meeting_config.allow_recording else "false",
            "allowStartStopRecording": "true" if meeting_config.allow_recording else "false",
            "meta_creator": meeting.creator,
            "meta_roomsmeetingid": meeting.pk,
            "meta_bbb-origin": "BBB@Scale",
            "meta_streamingUrl": meeting_config.streamingUrl,
        }
        if meeting_config.welcome_message:
            _create_parameter["welcome"] = meeting_config.welcome_message
        if meeting_config.dialNumber:
            _create_parameter["dialNumber"] = meeting_config.dialNumber
        if meeting_config.maxParticipants:
            _create_parameter["maxParticipants"] = meeting_config.maxParticipants
        logger.debug(f"Meeting parameter created: {_create_parameter}.")
        return _create_parameter
    except KeyError as e:
        logger.critical(f"Create meeting aborted. Key error {str(e)}")
        return None


def meeting_create(request, create_parameter, scheduling_strategy: SchedulingStrategy, meeting: Meeting, body=None):
    logger.debug(f"Start BBB meeting creation for {meeting.room_name}.")
    # create Webhook & Return Server
    server = scheduling_strategy.get_server_for_room()
    logger.debug(f"Selected server for {meeting.room_name} is {server.dns}.")
    bbb = BigBlueButton(server.dns, server.shared_secret)
    web_hook_response = create_web_hook_bbb(request, bbb)
    if meeting.configuration:
        slides = map(
            lambda _slides: (
                request.build_absolute_uri(_slides.file.url),
                _slides.name + Path(_slides.file.name).suffix,
            ),
            meeting.configuration.ordered_slides,
        )
    else:
        slides = iter([])

    if BigBlueButton.validate_create_web_hook(web_hook_response):
        logger.debug(f"Webhook created on {server.dns}.")
        try:
            response = bbb.create(
                create_meeting_and_bbb_parameter(request, meeting, create_parameter),
                slides,
                body,
            )
            logger.debug("Meeting created on {} with meeting id: {} .".format(server.dns, meeting.id))
            logger.debug("End creating meeting.")

            if meeting.external_meeting:
                return response
            return BigBlueButton.validate_create(response)
        except (ConnectionError, TimeoutError) as e:
            logger.error(f"Create meeting on server {server.dns} took longer than expected. Error message: {str(e)}")
            return True
    else:
        Server.objects.filter(pk=server.pk).update(state=SERVER_STATE_ERROR)
        logger.critical(f"Could not create webhook at {server.dns}. Server state is set to ERROR.")
        return False if not meeting.external_meeting else web_hook_response


def create_join_meeting_url(meeting, user, password):
    logger.debug(f"Join URL for meeting: {meeting} on server: {meeting.server} .")
    join_parameter = {"meetingID": meeting.id, "password": password, "fullName": str(user), "redirect": "true"}
    if isinstance(user, User):
        join_parameter["userdata-bbb_auto_join_audio"] = json.dumps(user.bbb_auto_join_audio)
        join_parameter["userdata-bbb_listen_only_mode"] = json.dumps(user.bbb_listen_only_mode)
        join_parameter["userdata-bbb_skip_check_audio"] = json.dumps(user.bbb_skip_check_audio)
        join_parameter["userdata-bbb_skip_check_audio_on_first_join"] = json.dumps(
            user.bbb_skip_check_audio_on_first_join
        )
        join_parameter["userdata-bbb_auto_share_webcam"] = json.dumps(user.bbb_auto_share_webcam)
        join_parameter["userdata-bbb_record_video"] = json.dumps(user.bbb_record_video)
        join_parameter["userdata-bbb_skip_video_preview"] = json.dumps(user.bbb_skip_video_preview)
        join_parameter["userdata-bbb_skip_video_preview_on_first_join"] = json.dumps(
            user.bbb_skip_video_preview_on_first_join
        )
        join_parameter["userdata-bbb_mirror_own_webcam"] = json.dumps(user.bbb_mirror_own_webcam)
        join_parameter["userdata-bbb_force_restore_presentation_on_new_events"] = json.dumps(
            user.bbb_force_restore_presentation_on_new_events
        )
        join_parameter["userdata-bbb_auto_swap_layout"] = json.dumps(user.bbb_auto_swap_layout)
        join_parameter["userdata-bbb_show_participants_on_login"] = json.dumps(user.bbb_show_participants_on_login)
        join_parameter["userdata-bbb_show_public_chat_on_login"] = json.dumps(user.bbb_show_public_chat_on_login)

    logger.debug(f"Join parameter: {join_parameter}.")
    bbb = BigBlueButton(meeting.server.dns, meeting.server.shared_secret)
    return bbb.join(join_parameter)


def create_join_parameters(room: Room, attempt_direct_join: bool, provided_join_secret: Optional[str]) -> dict:
    join_parameters = {
        "room_id": room.pk,
        "timestamp": round(time.time() * 1000),
        "valid_secret": False,
        "direct_join": False,
    }

    if room.direct_join_secret:
        join_parameters["valid_secret"] = provided_join_secret == room.direct_join_secret
        join_parameters["direct_join"] = attempt_direct_join

    return join_parameters


@dataclass
class MeetingStats:
    meeting_id: uuid.UUID
    bbb_meeting_id: str
    rooms_meeting_id: uuid.UUID

    name: str
    participant_count: int
    video_count: int
    attendee_pw: str
    moderator_pw: str
    creator: str

    is_breakout: bool

    @classmethod
    def from_api(cls, payload: Dict):
        return cls(
            meeting_id=uuid.UUID(payload["meetingID"]),
            bbb_meeting_id=payload["BBBMeetingID"],
            rooms_meeting_id=uuid.UUID(payload["roomsMeetingID"]),
            name=payload["name"],
            participant_count=payload["participantCount"],
            video_count=payload["videoCount"],
            attendee_pw=payload["attendeePW"],
            moderator_pw=payload["moderatorPW"],
            creator=payload["creator"],
            is_breakout=False,
        )

    @classmethod
    def from_bbb(cls, meeting: Dict):
        return cls(
            meeting_id=uuid.UUID(meeting["meetingID"]),
            bbb_meeting_id=meeting["internalMeetingID"],
            rooms_meeting_id=uuid.UUID(meeting["metadata"]["roomsmeetingid"]),
            name=meeting["meetingName"],
            participant_count=meeting["participantCount"],
            video_count=meeting["videoCount"],
            attendee_pw=meeting["attendeePW"],
            moderator_pw=meeting["moderatorPW"],
            creator=meeting["metadata"]["creator"],
            is_breakout=meeting["isBreakout"] == "true",
        )


@dataclass
class ServerStats:
    dns: str
    meeting_stats: List[MeetingStats] = field(default_factory=list)

    @classmethod
    def from_api(cls, payload: Dict):
        return cls(payload["dns"], [MeetingStats.from_api(mp) for mp in payload["meetings"]])


def sync_meeting_stats(server: Server, m: MeetingStats) -> None:
    with transaction.atomic():
        try:
            meeting, _ = Meeting.objects.update_or_create(
                id=m.meeting_id,
                defaults={
                    "server": server,
                    "room_name": m.name,
                    "state": MEETING_STATE_RUNNING,
                    "attendee_pw": m.attendee_pw,
                    "moderator_pw": m.moderator_pw,
                    "participant_count": m.participant_count,
                    "videostream_count": m.video_count,
                    "creator": m.creator,
                    "bbb_meeting_id": m.bbb_meeting_id,
                    "last_health_check": timezone.now(),
                },
            )

            if meeting.room:
                Room.objects.filter(pk=meeting.room.pk).update(state=ROOM_STATE_ACTIVE, last_running=timezone.now())

        except ValidationError as e:
            logger.error("provided meeting stats are invalid: %s", str(e))


def sync_server_stats(server: Server, meeting_stats: List[MeetingStats]):
    for m_stats in meeting_stats:
        sync_meeting_stats(server, m_stats)

    if server.state != SERVER_STATE_DISABLED:
        server.state = SERVER_STATE_UP
    server.last_health_check = timezone.now()
    server.save()


def create_bbb(dns: str, shared_secret):
    return BigBlueButton(dns, shared_secret)


def collect_server_stats(bbb_creator: Callable = create_bbb) -> None:
    for server in Server.objects.all().exclude(state=SERVER_STATE_DISABLED):
        logger.debug("Call collect_stats on server={}".format(server))
        bbb = bbb_creator(server.dns, server.shared_secret)
        try:
            meetings = bbb.validate_get_meetings(bbb.get_meetings())
        except Exception as e:
            logger.error("setting server state of {} to ERROR".format(server.dns) + str(e))
            server.state = SERVER_STATE_ERROR
            server.save()
            continue

        meeting_stats = []
        for m in meetings:
            try:
                if BigBlueButton.meeting_has_origin(m, "BBB@Scale") and not BigBlueButton.meeting_is_breakout(m):
                    stats = MeetingStats.from_bbb(m)
                    meeting_stats.append(stats)
            except KeyError as e:
                logger.debug("missing field in meeting response from BBB: %s" % str(e))
            except ValueError as e:
                logger.debug("not parsable value in meeting response from BBB: %s" % str(e))

        sync_server_stats(server, meeting_stats)

    logger.debug("Done collecting server stats")


def end_meeting(meeting: Meeting):
    logger.debug("Resetting meeting %s after meeting finished on BBB server" % meeting.id)
    meeting.state = MEETING_STATE_FINISHED
    room = meeting.room
    if room and not meeting.is_breakout:
        room.state = ROOM_STATE_INACTIVE
        room.last_running = None
        room.save()
        logger.debug("Room %s resetting done" % room.name)
    meeting.save()
    logger.debug("Meeting %s resetting done" % meeting.id)


def collect_room_occupancy(room_pk):
    room = Room.objects.get(pk=room_pk)
    logger.debug(f"Starting collection of room occupancy for {room.name}.")
    context = EventCollectorContext(room.event_collection_strategy)
    context.collect_events(room_pk, room.event_collection_parameters)
    logger.debug(f"Ended collection of room occupancy for {room.name}.")


def room_click(room_pk):
    from django.db.models import F

    Room.objects.filter(pk=room_pk).update(click_counter=F("click_counter") + 1)
    logger.debug(f"Updated click counter for {room_pk}.")


def get_rooms_with_current_next_event(tenant, room_visibilities):
    logger.debug("Adding current and next event to Room query.")
    current = RoomEvent.objects.filter(
        room=OuterRef("pk"), start__lte=timezone.now(), end__gte=timezone.now()
    ).order_by("end")
    next = RoomEvent.objects.filter(room=OuterRef("pk"), start__gt=timezone.now(), end__gt=timezone.now()).order_by(
        "start"
    )
    meeting = Meeting.objects.filter(room=OuterRef("pk"), state=MEETING_STATE_RUNNING)

    query = (
        Room.objects.filter(
            tenants__in=[tenant], default_meeting_configuration__isnull=False, visibility__in=room_visibilities
        )
        .exclude(pk__in=Subquery(PersonalRoom.objects.values_list("pk", flat=True)))
        .exclude(pk__in=Subquery(HomeRoom.objects.values_list("pk", flat=True)))
        .annotate(room_occupancy_current=Subquery(current.values("name")[:1]))
        .annotate(room_occupancy_next=Subquery(next.values("name")[:1]))
        .annotate(participant_count=Coalesce(Subquery(meeting.values("participant_count")[:1]), 0))
        .prefetch_related("default_meeting_configuration")
    )

    return query


def get_join_password(user, meeting: Meeting, creator_name):
    creator = meeting.creator
    meeting_config = meeting.configuration
    if (
        user.is_superuser
        or (user.is_staff and user.tenant in meeting.tenants.all())
        or (meeting_config.moderation_mode == MODERATION_MODE_STARTER and creator == creator_name)
        or (
            meeting.room
            and (
                meeting.room.is_personalroom()
                and (meeting.room.personalroom.owner == user or meeting.room.personalroom.has_co_owner(user))
            )
        )
    ):
        password = meeting.moderator_pw
    elif meeting_config.guest_policy == GUEST_POLICY_APPROVAL and not creator == creator_name:
        password = meeting.attendee_pw
    elif meeting_config.moderation_mode == MODERATION_MODE_ALL or (
        meeting_config.moderation_mode == MODERATION_MODE_MODERATORS
        and user.groups.filter(name=settings.MODERATORS_GROUP).exists()
        and user.tenant in meeting.tenants.all()
    ):
        password = meeting.moderator_pw
    else:
        password = meeting.attendee_pw
    logger.debug(f"Password for joining meeting {password} for user {user if user.is_authenticated else creator_name}")
    return password


def create_getrecordings_response_xml(meetings: Iterable[Meeting]):
    recordings = []
    for meeting in meetings:
        recording_metadata_xml_response = meeting.get_recordings_xml()
        if recording_metadata_xml_response:
            recordings.append(
                (
                    xmltodict.parse(recording_metadata_xml_response.text),
                    meeting.replay_url,
                    meeting.external_meeting.external_meeting_id if meeting.external_meeting else meeting.id,
                )
            )
    xml_recordings = create_recordings_xml_bbb(recordings)
    return (
        {"response": {"returncode": "SUCCESS", "recordings": {"recording": xml_recordings}}}
        if len(recordings) >= 1
        else None
    )


def create_recordings_xml_bbb(recordings: Iterable[Tuple[Dict, str, str]]):
    _recordings = []
    logger.debug("start processing and converting recordings metadata.xml for external usage")
    for rec, url, external_meeting_id in recordings:
        try:
            record: Dict = {
                "recordID": rec["recording"]["id"],
                "meetingID": external_meeting_id,
                "internalMeetingID": rec["recording"]["meeting"]["@id"],
                "name": rec["recording"]["meeting"]["@name"],
                "isBreakout": rec["recording"]["meeting"]["@breakout"],
                "published": rec["recording"]["published"],
                "state": rec["recording"]["state"],
                "startTime": rec["recording"]["start_time"],
                "endTime": rec["recording"]["end_time"],
                "participants": rec["recording"]["participants"],
                "metadata": rec["recording"]["meta"],
                "playback": {
                    "format": {
                        "type": rec["recording"]["playback"]["format"],
                        "url": url,
                        "processingTime": rec["recording"]["playback"]["processing_time"],
                        "length": rec["recording"]["playback"]["duration"],
                        "size": rec["recording"]["playback"]["size"],
                    }
                },
            }
            if "preview" in (rec["recording"]["playback"].get("extensions") or dict()):
                record["playback"]["format"]["preview"] = (
                    rec["recording"]["playback"]["extensions"]["preview"] or dict()
                )
            try:
                for image in record["playback"]["format"]["preview"]["images"]["image"]:
                    image_url = parse.urlsplit(image["#text"])
                    replay_url = parse.urlsplit(url)
                    image["#text"] = parse.SplitResult(
                        query=replay_url.query,
                        fragment=replay_url.fragment,
                        scheme=replay_url.scheme,
                        netloc=replay_url.netloc,
                        path=replay_url.path.removesuffix("/")
                        + image_url.path.removeprefix(f"/presentation/{record['recordID']}"),
                    ).geturl()
            except KeyError:
                logger.debug(f"KeyError for recording ({url}): no images found in preview; metadata.xml: {rec}")
                record["playback"]["format"].pop("preview", None)
            _recordings.append(record)
        except KeyError as key:
            logger.error(f"KeyError for recording ({url}): {key} not found.; metadata.xml: {rec}")
    return _recordings


def update_publish_recordings(params: Dict, rpc, meta: bool):
    meetings = get_meetings_for_record_ids(params["recordID"])
    logger.debug(f"starting update/publish recording process for {params['recordID']}")
    if not meetings:
        logger.error(f"no meetings found for {params['recordID']}")
        raise ObjectDoesNotExist()
    for meeting in meetings:
        _meta_updates = (
            [(k, v) for k, v in params.items() if k.startswith("meta_")]
            if meta
            else [("published", True if params["publish"] == "true" else False)]
        )
        metadata_dict = generate_updated_metadata_xml_as_dict(meeting, meta, _meta_updates)
        if not metadata_dict:
            logger.error(f"no metadata xml for meeting {meeting.id} returned")
            raise ObjectDoesNotExist()
        try:
            rpc.update_recording(meeting.replay_id.replace("--", "/"), unparse(metadata_dict).encode())
        except grpc.RpcError as e:
            logger.error(f"GRPC error during update/publish recording for meeting {meeting.id}: {str(e)}")
            raise e
        except Exception as e:
            logger.error(
                f"Unhandled exception during update/publish recording happened for meeting {meeting.id}: {str(e)}"
            )
            raise e


def get_meetings_for_record_ids(record_id: str):
    recording_ids = record_id.split(",")
    meetings = Meeting.objects.filter(
        Q(replay_id__in=recording_ids) | Q(replay_id__in=[f"recordings--{x}" for x in recording_ids])
    )
    return meetings


def generate_updated_metadata_xml_as_dict(meeting: Meeting, meta: bool, update_parameters: List[Tuple[str, any]]):
    metadata_xml = meeting.get_recordings_xml()
    if not metadata_xml:
        return None
    metadata_dict = xmltodict.parse(metadata_xml.text)
    for k, v in update_parameters:
        if meta:
            metadata_dict["recording"]["meta"][k.replace("meta_", "")] = v
            if k == "meta_meetingName":
                metadata_dict["recording"]["meeting"]["@name"] = v
        else:
            metadata_dict["recording"][k] = v
    return metadata_dict


def delete_recordings(record_id: str, rpc):
    logger.debug("Start delete recordings.")
    meetings = get_meetings_for_record_ids(record_id)
    if not meetings:
        logger.error(f"no meetings found for {record_id}")
        raise ObjectDoesNotExist()
    for meeting in meetings:
        try:
            rpc.delete_recording(meeting.replay_id.replace("--", "/"))
            meeting.delete_recording()
        except grpc.RpcError as e:
            logger.error(f"GRPC error during delete recordings for meeting {meeting.id}: {str(e)}")
            raise e
        except Exception as e:
            logger.error(f"Unhandled exception during delete recordings happened for meeting {meeting.id}: {str(e)}")
            raise e


def parse_dict_to_xml(response_dict):
    logger.debug("Parsing dict to XML.")
    return unparse(response_dict)


def join_or_create_meeting_permissions(user, room):
    logger.debug(f"Returning permission to create meeting for {user} and {room}")
    if room.default_meeting_configuration.everyone_can_start:
        return True
    if user.is_authenticated:
        if user.is_superuser:
            return True
        if room.is_homeroom():
            return room.homeroom.owner == user
        if room.is_personalroom():
            return room.personalroom.owner == user or room.personalroom.has_co_owner(user)
        if user.tenant in room.tenants.all():
            if room.default_meeting_configuration.authenticated_user_can_start:
                return True
            if user.groups.filter(name=settings.MODERATORS_GROUP).exists():
                return True
    return False


def remove_double_spaces(string):
    return re.sub(r"\s{2,}", " ", string)


def validate_bbb_api_call(api_method, params, slug):
    token = ApiToken.objects.get(slug=slug)
    checksum = params.pop("checksum")
    _checksum = sha1(
        "{}{}{}".format(api_method, BigBlueButton.create_params(params), token.secret).encode("utf-8")
    ).hexdigest()
    return True if checksum[0] == _checksum else False


@dataclass
class RegistrationDetails:
    scheduling_strategy: SchedulingStrategy

    dns: str
    machine_id: str
    shared_secret: str

    @classmethod
    def from_api(cls, communication_token: ApiToken, data: dict) -> "RegistrationDetails":
        return cls(
            dns=data["hostname"],
            machine_id=data["machine_id"],
            shared_secret=data["shared_secret"],
            scheduling_strategy=communication_token.scheduling_strategy,
        )

    def get_server_details(self):
        return {
            "machine_id": self.machine_id,
            "shared_secret": self.shared_secret,
            "scheduling_strategy": self.scheduling_strategy,
        }


def register_server(registration_details: RegistrationDetails) -> bool:
    server, server_created = Server.objects.update_or_create(
        dns=registration_details.dns, defaults=registration_details.get_server_details()
    )
    # For now just add the 'worker' type to all servers enrolled through this
    # endpoint. Going forward we may accept additional types taken from the
    # POSTed request.
    server.server_types.add(ServerType.objects.get_or_create(name="worker")[0])

    return server_created


def user_can_interact_with_recording(user: User, recording: Meeting) -> bool:
    return user.is_superuser or user_is_staff_of_tenant(user, recording.tenants.all()) or recording.is_owned_by(user)


def user_is_staff_of_tenant(user: User, tenants: Iterable[Site]) -> bool:
    return user.is_staff and user.tenant in tenants


def delete_recordings_internal():
    for recording in Meeting.objects.for_deletion():
        try:
            gp: GeneralParameter = GeneralParameter.load(recording.tenants.first())
            rpc: RecordingsGRPC = RecordingsGRPC(gp.recording_management_url, gp.recording_key, gp.recording_cert)
            rpc.delete_recording(recording.replay_id.replace("--", "/"))
            recording.delete_recording()
        except IntegrityError as e:
            logger.error(f"recording with id {recording.id} has no tenants assosiated; {str(e)}")
        except grpc.RpcError as e:
            logger.error(f"recordings rpc error {str(e)}")
        except Exception as e:
            logger.error(f"uncaught exception: {str(e)}")


def collect_get_meetings_items(meetings: Iterable, bbb_creator=create_bbb) -> Iterable:
    get_meetings_items = (fetch_get_meetings_item(m, bbb_creator) for m in meetings)
    return [clean_get_meetings_item(m) for m in get_meetings_items if m]


def fetch_get_meetings_item(meeting: Meeting, bbb_creator=create_bbb) -> Optional[dict]:
    bbb = bbb_creator(meeting.server.dns, meeting.server.shared_secret)
    response = bbb.get_meeting_infos(meeting.id)
    return BigBlueButton.validate_get_meeting_infos(response)


def clean_get_meetings_item(meeting_response: dict) -> dict:
    try:
        cleaned_meeting_infos = meeting_response["response"]
        del cleaned_meeting_infos["returncode"]
    except KeyError:
        return meeting_response
    return cleaned_meeting_infos

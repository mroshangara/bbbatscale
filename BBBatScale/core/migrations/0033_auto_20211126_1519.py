# Generated by Django 3.2.9 on 2021-11-26 15:19

from core.constants import ROOM_VISIBILITY_INTERNAL, ROOM_VISIBILITY_PRIVATE, ROOM_VISIBILITY_PUBLIC
from django.db import migrations, models


def reverse_func_set_visibility_of_rooms_based_on_is_public(apps, schema_editor):
    Room = apps.get_model("core", "Room")
    db_alias = schema_editor.connection.alias
    Room.objects.using(db_alias).filter(visibility=ROOM_VISIBILITY_PUBLIC).update(is_public=True)
    Room.objects.using(db_alias).filter(visibility__in=[ROOM_VISIBILITY_PRIVATE, ROOM_VISIBILITY_INTERNAL]).update(
        is_public=False
    )


def forwards_func_set_visibility_of_rooms_based_on_is_public(apps, schema_editor):
    Room = apps.get_model("core", "Room")
    db_alias = schema_editor.connection.alias
    Room.objects.using(db_alias).filter(is_public=True).update(visibility=ROOM_VISIBILITY_PUBLIC)


class Migration(migrations.Migration):

    dependencies = [
        ("core", "0032_auto_20211013_1209"),
    ]

    operations = [
        migrations.AddField(
            model_name="room",
            name="visibility",
            field=models.CharField(
                choices=[("public", "Public"), ("internal", "Only logged-in users"), ("private", "Nobody")],
                default="private",
                max_length=8,
            ),
        ),
        migrations.RunPython(
            forwards_func_set_visibility_of_rooms_based_on_is_public,
            reverse_func_set_visibility_of_rooms_based_on_is_public,
        ),
        migrations.RemoveField(
            model_name="room",
            name="is_public",
        ),
    ]

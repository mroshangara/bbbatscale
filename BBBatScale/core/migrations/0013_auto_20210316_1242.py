# Generated by Django 3.1.2 on 2021-03-16 12:42

import django.db.models.deletion
from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("core", "0012_auto_20210213_1155"),
    ]

    operations = [
        migrations.AlterField(
            model_name="generalparameter",
            name="home_room_room_configuration",
            field=models.ForeignKey(
                blank=True,
                help_text="Define which room configuration will be used for new Home rooms.",
                null=True,
                on_delete=django.db.models.deletion.SET_NULL,
                to="core.roomconfiguration",
                verbose_name="Room configuration",
            ),
        ),
        migrations.AlterField(
            model_name="generalparameter",
            name="home_room_tenant",
            field=models.ForeignKey(
                blank=True,
                help_text="Define which tenant will be used for new Home rooms.",
                null=True,
                on_delete=django.db.models.deletion.SET_NULL,
                to="core.tenant",
                verbose_name="Tenant",
            ),
        ),
        migrations.AlterField(
            model_name="homeroom",
            name="owner",
            field=models.OneToOneField(
                on_delete=django.db.models.deletion.CASCADE, related_name="homeroom_ptr", to=settings.AUTH_USER_MODEL
            ),
        ),
    ]

import base64
import hashlib
import time

from django.conf import settings
from django.core.files.storage import FileSystemStorage
from django.utils.crypto import get_random_string
from django.utils.deconstruct import deconstructible
from django.utils.encoding import force_bytes


@deconstructible
class FileSystemStorageWithRestrictedAccess(FileSystemStorage):
    def url(self, name):
        url = super().url(name)
        salt = get_random_string(8)

        if getattr(settings, "BBBATSCALE_MEDIA_URL_EXPIRY_SECONDS", 0) <= 0:
            expires = ""
        else:
            expires = int(time.time() + settings.BBBATSCALE_MEDIA_URL_EXPIRY_SECONDS)

        md5 = (
            base64.urlsafe_b64encode(
                hashlib.md5(force_bytes(f"{expires}{salt}{url} {settings.BBBATSCALE_MEDIA_SECRET_KEY}")).digest()
            )
            .strip(b"=")
            .decode()
        )

        signed_url = f"{url}?md5={md5}&salt={salt}"
        if expires:
            signed_url += f"&expires={expires}"

        return signed_url

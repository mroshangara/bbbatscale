import logging

from core.forms import HomeRoomForm
from core.models import GeneralParameter, HomeRoom
from core.views import create_view_access_logging_message
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.sites.shortcuts import get_current_site
from django.shortcuts import get_object_or_404, redirect, render
from django.utils.translation import gettext_lazy as _

logger = logging.getLogger(__name__)


@login_required
def home_room_update(request, home_room):
    logger.info(create_view_access_logging_message(request, home_room))
    instance = get_object_or_404(HomeRoom, pk=home_room)
    form = HomeRoomForm(
        request.POST or None,
        general_parameter=GeneralParameter.load(get_current_site(request)),
        user=request.user,
        instance=instance,
    )
    if request.method == "POST" and form.is_valid():
        _room = form.save(commit=False)
        _room.name = instance.homeroom.name
        _room.save()
        form.save_m2m()
        if instance.owner == request.user or request.user.is_superuser:
            messages.success(request, _("Home room was updated successfully."))
        else:
            messages.error(request, _("Only owner can update their own home rooms!"))
            logger.debug("{} has tried to update home room of {}".format(request.user, instance.owner))
        return redirect("home")
    return render(request, "home_rooms_update.html", {"form": form})

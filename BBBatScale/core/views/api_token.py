import logging

from core.decorators import tenant_based_permission_required
from core.forms import ApiTokenForm
from core.models import ApiToken, GeneralParameter
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.sites.shortcuts import get_current_site
from django.shortcuts import get_object_or_404, redirect, render
from django.utils.translation import gettext_lazy as _

logger = logging.getLogger(__name__)


@login_required
@tenant_based_permission_required("core.view_apitoken", elevate_staff=False, raise_exception=True)
def token_overview(request):
    tokens = ApiToken.objects.all()
    return render(
        request,
        "api_token_overview.html",
        {"tokens": tokens},
    )


@login_required
@tenant_based_permission_required("core.add_apitoken", elevate_staff=False, raise_exception=True)
def token_create(request):
    form = ApiTokenForm(request.POST or None, general_parameter=GeneralParameter.load(get_current_site(request)))
    if request.method == "POST" and form.is_valid():
        form.save()
        messages.success(request, _("Token successfully created"))
        return redirect("token_overview")
    return render(request, "api_token_create.html", {"form": form})


@login_required
@tenant_based_permission_required("core.delete_apitoken", elevate_staff=False, raise_exception=True)
def token_delete(request, token):
    instance = get_object_or_404(ApiToken, pk=token)
    instance.delete()
    messages.success(request, _("Token successfully deleted"))
    return redirect("token_overview")


@login_required
@tenant_based_permission_required(
    ["core.view_apitoken", "core.change_apitoken"], elevate_staff=False, raise_exception=True
)
def token_update(request, token):
    instance = get_object_or_404(ApiToken, pk=token)
    form = ApiTokenForm(
        request.POST or None, instance=instance, general_parameter=GeneralParameter.load(get_current_site(request))
    )
    if request.method == "POST" and form.is_valid():
        form.save()
        messages.success(request, _("Token successfully updated"))
        return redirect("token_overview")
    return render(request, "api_token_create.html", {"form": form})

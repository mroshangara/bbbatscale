import logging

from core.decorators import tenant_based_permission_required
from core.forms import TenantForm
from core.views import create_view_access_logging_message
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.sites.models import Site
from django.shortcuts import get_object_or_404, redirect, render
from django.utils.translation import gettext_lazy as _
from oidc_authentication.models import AuthParameter

logger = logging.getLogger(__name__)


@login_required
@tenant_based_permission_required("core.view_site", elevate_staff=False, raise_exception=True)
def tenant_overview(request):
    logger.info(create_view_access_logging_message(request))
    return render(
        request,
        "tenant_overview.html",
        {"tenants": Site.objects.all()},
    )


@login_required
@tenant_based_permission_required("core.add_site", elevate_staff=False, raise_exception=True)
def tenant_create(request):
    logger.info(create_view_access_logging_message(request))
    form = TenantForm(request.POST or None)
    if request.method == "POST" and form.is_valid():
        instance = form.save()
        messages.success(
            request,
            _("Tenant with domain {} and name {} was created successfully.").format(instance.domain, instance.name),
        )
        return redirect("tenant_overview")
    return render(
        request,
        "tenant_create.html",
        {"form": form},
    )


@login_required
@tenant_based_permission_required(["core.view_site", "core.change_site"], elevate_staff=False, raise_exception=True)
def tenant_update(request, tenant):
    logger.info(create_view_access_logging_message(request))
    instance = get_object_or_404(Site, pk=tenant)
    form = TenantForm(request.POST or None, instance=instance)

    if AuthParameter.objects.filter(tenant=instance, oidc_username_standard=False).exists():
        messages.warning(
            request,
            _(
                "If you change the domain of any tenant, be aware that the username contains this domain as an "
                "unique identifier. You might have to take care of migrating your "
                "users username for the new domain."
            ),
        )
    if request.method == "POST" and form.is_valid():
        instance = form.save()
        messages.success(
            request,
            _("Tenant with domain {} and name {} was updated successfully.").format(instance.domain, instance.name),
        )
        return redirect("tenant_overview")
    return render(
        request,
        "tenant_create.html",
        {"form": form},
    )


@login_required
@tenant_based_permission_required("core.delete_site", elevate_staff=False, raise_exception=True)
def tenant_delete(request, tenant):
    logger.info(create_view_access_logging_message(request))
    instance = get_object_or_404(Site, pk=tenant)
    instance.delete()
    messages.success(request, _("Tenant was deleted successfully."))
    return redirect("tenant_overview")

import logging

from core.decorators import tenant_based_permission_required
from core.forms import MeetingConfigurationsTemplateForm
from core.models import MeetingConfigurationTemplate
from core.views import create_view_access_logging_message
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.sites.shortcuts import get_current_site
from django.db.models.functions import Lower
from django.http import HttpRequest, HttpResponse
from django.shortcuts import get_object_or_404, redirect, render
from django.utils.translation import gettext_lazy as _

logger = logging.getLogger(__name__)


@login_required
@tenant_based_permission_required("core.view_meetingconfigurationtemplate", raise_exception=True)
def room_config_overview(request: HttpRequest) -> HttpResponse:
    logger.info(create_view_access_logging_message(request))
    tenant = get_current_site(request)

    context = {"configs": MeetingConfigurationTemplate.objects.filter(tenants__in=[tenant]).order_by(Lower("name"))}
    return render(request, "room_configs_overview.html", context)


@login_required
@tenant_based_permission_required("core.add_meetingconfigurationtemplate", raise_exception=True)
def room_config_create(request: HttpRequest) -> HttpResponse:
    logger.info(create_view_access_logging_message(request))
    tenant = get_current_site(request)
    form = MeetingConfigurationsTemplateForm(request.POST or None)
    if request.method == "POST" and form.is_valid():
        room_config = form.save(commit=False)
        logger.debug("POST and valid request; room_default_meeting_configuration=%s", room_config)

        room_config.save()

        room_config.tenants.add(tenant)
        messages.success(request, _("Room configuration was created successfully."))
        logger.debug("Room configuration (%S) was created successfully.", room_config)
        # return to URL
        return redirect("room_configs_overview")
    return render(request, "room_configs_create.html", {"form": form})


@login_required
@tenant_based_permission_required("core.delete_meetingconfigurationtemplate", raise_exception=True)
def room_config_delete(request: HttpRequest, room_config: int) -> HttpResponse:
    logger.info(create_view_access_logging_message(request, room_config))

    instance = get_object_or_404(MeetingConfigurationTemplate, pk=room_config)
    logger.debug("Delete room_default_meeting_configuration=%s", instance)
    instance.delete()

    messages.success(request, _("Room configuration was deleted successfully."))
    logger.debug("Room configuration was deleted successfully.")

    return redirect("room_configs_overview")


@login_required
@tenant_based_permission_required(
    ["core.view_meetingconfigurationtemplate", "core.change_meetingconfigurationtemplate"], raise_exception=True
)
def room_config_update(request: HttpRequest, room_config: int) -> HttpResponse:
    logger.info(create_view_access_logging_message(request, room_config))

    instance = get_object_or_404(MeetingConfigurationTemplate, pk=room_config)
    form = MeetingConfigurationsTemplateForm(request.POST or None, instance=instance)
    if request.method == "POST" and form.is_valid():
        _room_config = form.save(commit=False)
        _room_config.save()
        messages.success(request, _("Room configuration was updated successfully."))
        logger.debug("Room configuration was updated successfully for %s", room_config)
        return redirect("room_configs_overview")

    logger.debug("Room configuration was NOT updated successfully for %s", room_config)
    return render(request, "room_configs_create.html", {"form": form})

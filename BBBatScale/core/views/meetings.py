import logging
from collections import namedtuple
from typing import Iterator, Optional, Tuple, Union
from uuid import UUID

from core.constants import MEETING_STATE
from core.decorators import tenant_based_permission_required
from core.models import Meeting, User
from core.utils import has_tenant_based_perms, paginate
from core.views import create_view_access_logging_message
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import AnonymousUser
from django.contrib.sites.models import Site
from django.contrib.sites.shortcuts import get_current_site
from django.db.models import Q
from django.http import HttpRequest, HttpResponse
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.utils import formats
from django.utils.translation import gettext_lazy as _

logger = logging.getLogger(__name__)


class MeetingDetail:
    Entry = namedtuple("Entry", ["value", "url"])

    class SingleValueEntry(Entry):
        def __new__(cls, name: str, value: Optional[str], url: Optional[str]) -> "MeetingDetail.SingleValueEntry":
            self = MeetingDetail.Entry.__new__(cls, value, url)

            self.name = name

            return self

        @staticmethod
        def has_multiple_values() -> bool:
            return False

    class MultiValueEntry:
        def __init__(self, name: str, values: Iterator[Tuple[Optional[str], Optional[str]]]) -> None:
            self.name = name
            self.values = list(values)

        def __iter__(self) -> Iterator["MeetingDetail.Entry"]:
            return (MeetingDetail.Entry(*entry) for entry in self.values)

        @staticmethod
        def has_multiple_values() -> bool:
            return True

    def __init__(self, meeting: Meeting, user: Union[User, AnonymousUser], tenant: Site) -> None:
        can_update_tenant = has_tenant_based_perms(
            user, ["core.view_tenant", "core.change_tenant"], tenant, elevate_staff=False
        )

        self.data = [
            self.SingleValueEntry(_("ID"), str(meeting.id), None),
            self.SingleValueEntry(_("BBB Meeting ID"), meeting.bbb_meeting_id or None, None),
            self.SingleValueEntry(
                _("External Meeting ID"),
                meeting.external_meeting.external_meeting_id if meeting.external_meeting_id is not None else None,
                None,  # TODO add link as soon as ExternalMeetings have a detail view.
            ),
            self.SingleValueEntry(
                _("Room"),
                meeting.room_name or None,
                reverse("room_update", args=[meeting.room_id])
                if meeting.room_id is not None
                and has_tenant_based_perms(user, ["core.view_room", "core.change_room"], tenant)
                else None,
            ),
            self.SingleValueEntry(
                _("Server"),
                meeting.server.dns if meeting.server_id is not None else None,
                reverse("server_update", args=[meeting.server_id])
                if meeting.server_id is not None
                and has_tenant_based_perms(
                    user, ["core.view_server", "core.change_server"], tenant, elevate_staff=False
                )
                else None,
            ),
            self.SingleValueEntry(_("State"), meeting.get_state_display(), None),
            self.MultiValueEntry(
                _("Tenants"),
                list(
                    (name, reverse("tenant_update", args=[meeting_id]) if can_update_tenant else None)
                    for name, meeting_id in meeting.tenants.values_list("name", "id")
                ),
            ),
            self.SingleValueEntry(_("Creator"), meeting.creator or None, None),
            self.SingleValueEntry(_("Replay ID"), meeting.replay_id or None, meeting.replay_url or None),
            self.SingleValueEntry(_("Participant count"), str(meeting.participant_count), None),
            self.SingleValueEntry(_("Videostream count"), str(meeting.videostream_count), None),
            self.SingleValueEntry(
                _("Configuration"),
                meeting.configuration.name if meeting.configuration_id is not None else None,
                None,  # TODO add link as soon as MeetingConfigurations have a detail view.
            ),
            self.SingleValueEntry(_("Attendee Password"), meeting.attendee_pw, None),
            self.SingleValueEntry(_("Moderator Password"), meeting.moderator_pw, None),
            self.SingleValueEntry(_("Started"), formats.localize(meeting.started), None),
            self.SingleValueEntry(_("Last health check"), formats.localize(meeting.last_health_check), None),
            self.SingleValueEntry(_("Is breakout"), _("True") if meeting.is_breakout else _("False"), None),
            self.SingleValueEntry(_("Published"), _("True") if meeting.published else _("False"), None),
            self.SingleValueEntry(_("To delete"), formats.localize(meeting.to_delete), None),
        ]

    def __iter__(self) -> Iterator[Union[SingleValueEntry, MultiValueEntry]]:
        return iter(self.data)


def map_meeting(meeting: Meeting) -> dict:
    return {
        "detailUrl": reverse("meeting_detail", args=[meeting.id]),
        "roomName": meeting.room_name,
        "creator": meeting.creator,
        "state": meeting.state,
        "server": meeting.server.dns if meeting.server_id is not None else None,
        "started": meeting.started.isoformat(),
        "participantCount": meeting.participant_count,
        "videostreamCount": meeting.videostream_count,
    }


@login_required
@tenant_based_permission_required("core.view_meeting", raise_exception=True)
def meetings_overview(request: HttpRequest) -> HttpResponse:
    logger.info(create_view_access_logging_message(request))
    return render(request, "meetings_overview.html", {"meeting_states": MEETING_STATE})


@login_required
@tenant_based_permission_required("core.view_meeting", raise_exception=True)
def meeting_detail(request: HttpRequest, meeting_id: UUID) -> HttpResponse:
    logger.info(create_view_access_logging_message(request, meeting_id))

    meeting = get_object_or_404(Meeting, id=meeting_id)
    # A list of tuples. The first tuple element is the name of the field, the second the value
    # and the third an optional link to a detail view of the value.
    # If the value is None, it will be rendered as an itallic "Unknown".

    return render(
        request,
        "meeting_detail.html",
        {"meeting_detail": MeetingDetail(meeting, request.user, get_current_site(request))},
    )


@login_required
@tenant_based_permission_required("core.view_meeting", raise_exception=True)
def get_meetings(request: HttpRequest) -> HttpResponse:
    tenant = get_current_site(request)

    query = Meeting.objects.filter(tenants__in=[tenant])
    order_by = request.GET.get("orderBy")
    if order_by not in ("state", "server", "started"):
        order_by = "started"
    prefix = "-" if request.GET.get("descending", "").lower() == "true" else ""

    query = query.order_by(prefix + order_by, prefix + "pk")

    search_filter = request.GET.get("filter-search")
    if search_filter:
        query = query.filter(
            Q(room_name__icontains=search_filter)
            | Q(creator__icontains=search_filter)
            | Q(server__dns__icontains=search_filter)
        )

    state_filter = request.GET.get("filter-state")
    if state_filter in (state[0] for state in MEETING_STATE):
        query = query.filter(state=state_filter)

    return paginate(query, int(request.GET.get("page", 1)), map_meeting)

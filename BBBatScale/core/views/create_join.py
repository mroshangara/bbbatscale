import hmac
import logging
import time
from typing import Dict, Optional, Union
from urllib.parse import quote

from core.constants import MEETING_STATE_CREATING, MEETING_STATE_RUNNING, ROOM_STATE_CREATING, ROOM_STATE_INACTIVE
from core.forms import ConfigureMeetingForm
from core.models import GeneralParameter, Meeting, MeetingConfiguration, Room
from core.services import (
    create_join_meeting_url,
    create_join_parameters,
    get_join_password,
    join_or_create_meeting_permissions,
    meeting_create,
    room_click,
)
from core.utils import generate_login_url, has_tenant_based_perm
from core.views import create_view_access_logging_message
from django.conf import settings
from django.contrib import messages
from django.contrib.sites.shortcuts import get_current_site
from django.db.transaction import atomic, non_atomic_requests
from django.http import HttpResponse, HttpResponseNotAllowed, JsonResponse
from django.shortcuts import get_object_or_404, redirect, render
from django.utils import timezone
from django.utils.timezone import now
from django.utils.translation import gettext_lazy as _

logger = logging.getLogger(__name__)


class JoinParameters:
    def __init__(self, valid_secret: bool = False, direct_join: bool = False) -> None:
        self.valid_secret = valid_secret
        self.direct_join = direct_join

    @staticmethod
    def validate_session_join_parameters(
        room_id: int, join_parameters: Optional[Dict[str, Union[int, str, bool]]]
    ) -> "JoinParameters":
        try:
            if (
                join_parameters
                and join_parameters["room_id"] == room_id
                and ((join_parameters["timestamp"] + 300_000) > round(time.time() * 1000))
            ):
                return JoinParameters(join_parameters["valid_secret"], join_parameters["direct_join"])
        except KeyError as e:
            logger.debug("KeyError during validation of session join parameters: %s" % str(e))
        return JoinParameters()


def join_redirect(request, room):
    logger.info(create_view_access_logging_message(request))
    provided_join_secret = request.GET.get("joinSecret")
    attempt_direct_join = request.GET.get("directJoin", "").lower() == "true"

    try:
        _room = Room.objects.get(name=room)
        room_click(_room.id)

        request.session["join_parameters"] = create_join_parameters(_room, attempt_direct_join, provided_join_secret)

        return redirect("join_or_create_meeting", _room.id)
    except Room.DoesNotExist:
        messages.error(request, _("Unable to find Room with given name: '{}'.").format(room))
        return redirect("home")


def join_or_create_meeting(request, room_id):
    logger.info(create_view_access_logging_message(request, room_id))

    _room = get_object_or_404(Room, id=room_id)
    _meeting = Meeting.objects.filter(room=_room, state=MEETING_STATE_RUNNING).first()

    join_parameters = JoinParameters.validate_session_join_parameters(_room.pk, request.session.get("join_parameters"))

    if request.user.is_anonymous and not join_parameters.valid_secret:
        if "username" not in request.session:
            logger.debug(
                "user is not authenticated and 'username' not in request.session; redirect to set_username_pre_join"
            )
            return redirect("set_username_pre_join", room_id)

    if _meeting:
        logger.debug("Redirect to 'join_meeting'")
        return redirect("join_meeting", _meeting.id)

    if _room.state == ROOM_STATE_INACTIVE and (
        join_or_create_meeting_permissions(request.user, _room) or join_parameters.valid_secret
    ):
        logger.debug("room.state is INACTIVE and permissions for user are sufficient; redirect to 'create_meeting'")
        return redirect("create_meeting", _room.id)

    logger.debug(
        f"No running meeting for {_room} and user has no sufficient permissions; redirect to 'join_waiting_room'"
    )
    return redirect("join_waiting_room", _room.id)


@non_atomic_requests
def create_meeting(request, room_id):
    logger.info(create_view_access_logging_message(request, room_id))

    logger.debug("Query Database Object and lock it for further requests!")
    room_instance = get_object_or_404(Room, id=room_id)
    join_parameters = JoinParameters.validate_session_join_parameters(
        room_instance.pk, request.session.get("join_parameters")
    )
    tenant = get_current_site(request)
    general_parameter = GeneralParameter.load(tenant)
    if request.user.is_anonymous and "username" not in request.session and not join_parameters.valid_secret:
        logger.debug(
            "user is not authenticated and username is not in request.session; "
            + "redirect to 'set_username_pre_join', room_id=%s",
            room_id,
        )
        return redirect("set_username_pre_join", room_id)

    if not join_or_create_meeting_permissions(request.user, room_instance) and not join_parameters.valid_secret:
        logger.debug("user has no join_or_create_meeting_permissions; redirect to 'join_meeting'")
        return redirect("join_waiting_room", room_instance.id)

    enforced_slides = general_parameter.slides.union(room_instance.slides.all())
    room_default_meeting_configuration = room_instance.default_meeting_configuration.instantiate(save=False)

    form = ConfigureMeetingForm(
        request.POST or None,
        enable_recordings=general_parameter.enable_recordings,
        enable_adminunmute=general_parameter.enable_adminunmute,
        enforced_slides=enforced_slides,
        initial_slides=room_instance.initial_slides or general_parameter.initial_slides or None,
        selectable_slides_pks=request.user.owned_slides.values_list("pk", flat=True)
        if has_tenant_based_perm(request.user, "core.view_own_slides", general_parameter.tenant)
        else None,
        general_parameter=general_parameter,
        instance=room_default_meeting_configuration,
    )
    if request.user.is_authenticated:
        creator_name = request.user.username
    elif "username" in request.session:
        creator_name = request.session["username"]
    else:
        creator_name = room_instance.name
    logger.debug("creator_name set to %s", creator_name)

    if not ((request.method == "POST" and form.is_valid()) or join_parameters.direct_join):
        logger.debug("Redirect to configure_room_for_meeting due to invalid form")
        return render(request, "configure_room_for_meeting.html", {"form": form, "room": room_instance})
    meeting_config = room_default_meeting_configuration if join_parameters.direct_join else form.save(commit=False)
    with atomic(durable=True):
        logger.debug("Start transactions for room creation")
        if (
            Room.objects.filter(pk=room_id, state=ROOM_STATE_INACTIVE).update(
                state=ROOM_STATE_CREATING, last_running=now()
            )
            == 0
        ):
            messages.warning(
                request,
                _("The meeting was started by another moderator." " You will be redirected to the meeting shortly."),
            )
            logger.debug(
                "Meeting was started by another moderator in parallel; " + "redirect to 'join_or_create_meeting'"
            )

            return redirect("join_or_create_meeting", room_instance.pk)
        meeting = room_instance.meeting_set.filter(state=MEETING_STATE_RUNNING).first()
        if meeting:
            return redirect("join_meeting", meeting.id)
        meeting_config.name = f"{room_instance.name}-config-{timezone.now().time().isoformat()[-3:]}"
        meeting_config.save()
        if not join_parameters.direct_join:
            form.save_m2m()
        meeting = Meeting.objects.create(
            room=room_instance,
            room_name=room_instance.name,
            configuration=meeting_config,
            creator=creator_name,
            state=MEETING_STATE_CREATING,
        )
        meeting.tenants.add(tenant)
    meeting_create(
        request,
        None,
        room_instance.scheduling_strategy,
        meeting,
    )
    return redirect("join_waiting_room", room_instance.id)


def join_waiting_room(request, room_id):
    logger.info(create_view_access_logging_message(request, room_id))

    _room = get_object_or_404(Room, id=room_id)
    meeting = _room.get_running_meeting()

    if meeting:
        logger.debug(f"room {_room} got a running meeting; redirect to 'join_meeting'")
        return redirect("join_meeting", meeting.id)
    # Meeting is not running or access code is needed
    logger.debug(f"room {_room} does not have a running meeting; rendering waiting room for room")
    return render(
        request,
        "join_waiting_room.html",
        {
            "room": _room,
            "login_url": generate_login_url("join_or_create_meeting", _room.id),
        },
    )


def join_meeting(request, meeting_id):
    logger.info(create_view_access_logging_message(request, meeting_id))

    meeting = get_object_or_404(Meeting, id=meeting_id)
    meeting_config: MeetingConfiguration = meeting.configuration
    room: Room = meeting.room
    join_parameters = JoinParameters.validate_session_join_parameters(room.id, request.session.get("join_parameters"))
    user_needs_access_code = meeting_config.access_code and (
        not meeting_config.only_prompt_guests_for_access_code or request.user.is_anonymous
    )

    username = request.user.username
    if request.user.is_anonymous and not join_parameters.valid_secret:
        if not meeting_config.allow_guest_entry:
            messages.warning(request, _("The moderator did not allow guest entry. Please login to access the meeting."))
            logger.debug(
                "The moderator did not allow guest entry. Please login to access the meeting; redirect to 'login'"
            )
            return redirect(settings.LOGIN_URL + "?next=" + quote(request.get_full_path_info()))
        elif "username" in request.session:
            username = request.session["username"]
        else:
            return redirect("set_username_pre_join", room.pk)

    if join_parameters.valid_secret and not username:
        username = room.name

    if meeting.state == MEETING_STATE_RUNNING:
        logger.debug(f"Meeting.state ({meeting.pk}) is RUNNING")
        password = (
            meeting.moderator_pw if join_parameters.valid_secret else get_join_password(request.user, meeting, username)
        )

        join_user = request.user if request.user.is_authenticated else username
        if user_needs_access_code:
            access_code = request.POST.get("access_code", None)
            if access_code:
                if hmac.compare_digest(access_code.encode("UTF-8"), meeting_config.access_code.encode("UTF-8")):
                    request.session.pop("username", None)
                    return redirect(create_join_meeting_url(meeting, join_user, password))
                else:
                    messages.error(request, _("Incorrect access code."))
        else:
            request.session.pop("username", None)
            return redirect(create_join_meeting_url(meeting, join_user, password))

    # Meeting is not running or access code is needed
    context = {
        "meeting": meeting,
    }
    return render(request, "join_meeting.html", context)


def session_set_username(request):
    logger.info(create_view_access_logging_message(request))

    if request.method != "POST":
        return HttpResponseNotAllowed(["POST"])
    request.session["username"] = request.POST.get("username")
    return HttpResponse("ok")


def session_set_username_pre_join(request, room_id):
    logger.info(create_view_access_logging_message(request, room_id))

    if request.user.is_authenticated or "username" in request.session:
        return redirect("join_or_create_meeting", room_id)
    return render(request, "set_username.html", {"room": Room.objects.filter(id=room_id).first()})


def get_room_status(request, room_id):
    logger.info(create_view_access_logging_message(request, room_id))

    room = get_object_or_404(Room, id=room_id)
    logger.debug("Was asked for state of {}. Answer is {}".format(room, room.state))
    return JsonResponse({"is_running": bool(room.get_running_meeting())})

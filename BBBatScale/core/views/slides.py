import logging
from typing import Optional

from core.decorators import tenant_based_permission_required
from core.forms import SlidesAdminForm, SlidesForm
from core.models import GeneralParameter, Slides, User
from core.views import create_view_access_logging_message
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.sites.shortcuts import get_current_site
from django.core.exceptions import PermissionDenied
from django.http import HttpRequest, HttpResponse, HttpResponseNotFound
from django.shortcuts import get_object_or_404, redirect, render
from django.utils.translation import gettext_lazy as _

logger = logging.getLogger(__name__)


@login_required
@tenant_based_permission_required("core.view_slides", raise_exception=True)
def slides_overview(request: HttpRequest) -> HttpResponse:
    if not GeneralParameter.load(get_current_site(request)).media_enabled:
        return HttpResponseNotFound()

    logger.info(create_view_access_logging_message(request))

    slides_query = Slides.objects.all()

    return render(
        request,
        "slides_overview.html",
        {
            "slides": slides_query,
            "slides_count": slides_query.count(),
            "show_slides_owner": True,
            "create_url_name": "slides_create",
            "update_url_name": "slides_update",
            "delete_url_name": "slides_delete",
        },
    )


@login_required
@tenant_based_permission_required("core.view_own_slides", raise_exception=True)
def owned_slides_overview(request: HttpRequest) -> HttpResponse:
    if not GeneralParameter.load(get_current_site(request)).media_enabled:
        return HttpResponseNotFound()

    logger.info(create_view_access_logging_message(request))

    slides_query = Slides.objects.filter(owner=request.user)

    return render(
        request,
        "slides_overview.html",
        {
            "slides": slides_query,
            "slides_count": slides_query.count(),
            "create_url_name": "owned_slides_create",
            "update_url_name": "owned_slides_update",
            "delete_url_name": "owned_slides_delete",
        },
    )


@login_required
@tenant_based_permission_required("core.delete_slides", raise_exception=True)
def slides_delete(request: HttpRequest, slides_id: int) -> HttpResponse:
    if not GeneralParameter.load(get_current_site(request)).media_enabled:
        return HttpResponseNotFound()

    logger.info(create_view_access_logging_message(request, slides_id))

    get_object_or_404(Slides, id=slides_id).delete()

    messages.success(request, _("Slides successfully deleted."))
    return redirect("slides_overview")


@login_required
@tenant_based_permission_required("core.delete_own_slides", raise_exception=True)
def owned_slides_delete(request: HttpRequest, slides_id: int) -> HttpResponse:
    if not GeneralParameter.load(get_current_site(request)).media_enabled:
        return HttpResponseNotFound()

    logger.info(create_view_access_logging_message(request, slides_id))

    get_object_or_404(Slides, id=slides_id, owner=request.user).delete()

    messages.success(request, _("Slides successfully deleted."))
    return redirect("owned_slides_overview")


@login_required
def slides_create_update(request: HttpRequest, slides_id: Optional[int] = None) -> HttpResponse:
    if not GeneralParameter.load(get_current_site(request)).media_enabled:
        return HttpResponseNotFound()

    tenant = get_current_site(request)
    user: User = request.user

    if slides_id is not None:
        if not user.has_tenant_based_perm("core.change_slides", tenant):
            raise PermissionDenied

        slides = get_object_or_404(Slides, id=slides_id)
    else:
        if not user.has_tenant_based_perm("core.add_slides", tenant):
            raise PermissionDenied

        slides = None

    logger.info(create_view_access_logging_message(request, slides_id))

    form = SlidesAdminForm(
        request.POST or None,
        request.FILES or None,
        tenant=slides.tenant if slides else tenant,
        instance=slides,
    )
    if request.method == "POST":
        if form.is_valid():
            messages.success(request, _("Slides {} successfully updated.").format(form.save().name))
            return redirect("slides_overview")
    return render(request, "slides_create_update.html", {"form": form})


@login_required
def owned_slides_create_update(request: HttpRequest, slides_id: Optional[int] = None) -> HttpResponse:
    if not GeneralParameter.load(get_current_site(request)).media_enabled:
        return HttpResponseNotFound()

    tenant = get_current_site(request)
    user: User = request.user

    if slides_id is not None:
        if not user.has_tenant_based_perm("core.change_own_slides", tenant):
            raise PermissionDenied

        slides = get_object_or_404(Slides, id=slides_id, owner=user)
    else:
        if not user.has_tenant_based_perm("core.add_own_slides", tenant):
            raise PermissionDenied

        slides = None

    logger.info(create_view_access_logging_message(request, slides_id))

    form = SlidesForm(
        request.POST or None,
        request.FILES or None,
        tenant=slides.tenant if slides else tenant,
        instance=slides,
    )
    if request.method == "POST":
        if form.is_valid():
            form.instance.owner = request.user
            messages.success(request, _("Slides {} successfully updated.").format(form.save().name))
            return redirect("owned_slides_overview")
    return render(request, "slides_create_update.html", {"form": form})

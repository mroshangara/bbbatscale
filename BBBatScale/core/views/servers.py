import logging

from core.decorators import tenant_based_permission_required
from core.filters import ServerFilter
from core.forms import ServerFilterFormHelper, ServerForm
from core.models import GeneralParameter, Server
from core.views import create_view_access_logging_message
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.sites.shortcuts import get_current_site
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, redirect, render
from django.utils.translation import gettext_lazy as _

logger = logging.getLogger(__name__)


@login_required
@tenant_based_permission_required("core.view_server", elevate_staff=False, raise_exception=True)
def servers_overview(request):
    logger.info(create_view_access_logging_message(request))
    tenant = get_current_site(request)
    qs = Server.objects.filter(scheduling_strategy__tenants__in=[tenant]).prefetch_related("scheduling_strategy")
    f = ServerFilter(request.GET, queryset=qs, request=request)
    f.form.helper = ServerFilterFormHelper
    return render(request, "servers_overview.html", {"filter": f})


@login_required
@tenant_based_permission_required("core.add_server", elevate_staff=False, raise_exception=True)
def server_create(request):
    logger.info(create_view_access_logging_message(request))

    form = ServerForm(request.POST or None, general_parameter=GeneralParameter.load(get_current_site(request)))
    if request.method == "POST" and form.is_valid():
        instance = form.save()
        messages.success(request, _("Server: {} was created successfully.").format(instance.dns))
        # return to URL
        return redirect("servers_overview")
    return render(request, "servers_create.html", {"form": form})


@login_required
@tenant_based_permission_required("core.delete_server", elevate_staff=False, raise_exception=True)
def server_delete(request, server):
    logger.info(create_view_access_logging_message(request, server))

    instance = get_object_or_404(Server, pk=server)
    instance.delete()
    messages.success(request, _("Server was deleted successfully"))
    return HttpResponseRedirect(request.META.get("HTTP_REFERER", "/"))


@login_required
@tenant_based_permission_required(["core.view_server", "core.change_server"], elevate_staff=False, raise_exception=True)
def server_update(request, server):
    logger.info(create_view_access_logging_message(request, server))

    instance = get_object_or_404(Server, pk=server)
    form = ServerForm(
        request.POST or None, instance=instance, general_parameter=GeneralParameter.load(get_current_site(request))
    )
    if request.method == "POST":
        if form.is_valid():
            _server = form.save()
            messages.success(request, _("Server: {} was updated successfully.").format(_server.dns))
            return redirect("servers_overview")
    return render(request, "servers_create.html", {"form": form})

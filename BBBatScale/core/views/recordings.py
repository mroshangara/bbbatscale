import logging
from datetime import timedelta

from core.forms import RecordingForm
from core.models import GeneralParameter, Meeting, User
from core.services import update_publish_recordings, user_can_interact_with_recording
from core.utils import RecordingsGRPC, has_tenant_based_perm
from core.views import create_view_access_logging_message
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.sites.shortcuts import get_current_site
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Q
from django.shortcuts import get_object_or_404, redirect, render
from django.urls import reverse
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

logger = logging.getLogger(__name__)


@login_required
def recordings_list(request):
    logger.info(create_view_access_logging_message(request))
    tenant = get_current_site(request)
    user = request.user

    recordings = Meeting.objects.filter(tenants__in=[tenant]).exclude(Q(replay_id="") | Q(replay_url=""))
    if not has_tenant_based_perm(user, "core.view_recording", tenant):
        recordings = recordings.filter_by_user(user)

    prepared_recordings = [
        (
            recording,
            request.build_absolute_uri(reverse("recording_redirect", args=[recording.replay_id])),
            User.objects.filter(username=recording.creator).first().email
            if User.objects.filter(username=recording.creator).count() == 1
            else None,
        )
        for recording in recordings
    ]

    return render(
        request, "recordings_overview.html", {"recordings": prepared_recordings, "recordings_count": recordings.count()}
    )


@login_required
def recording_redirect(request, replay_id):
    logger.info(create_view_access_logging_message(request, replay_id))
    tenant = get_current_site(request)
    meeting = get_object_or_404(Meeting, Q(replay_id=replay_id) | Q(replay_id=f"recordings--{replay_id}"))
    gp = GeneralParameter.load(tenant)
    redirect_url = meeting.replay_url if meeting.replay_url else gp.playback_url + meeting.replay_id
    return redirect(redirect_url)


@login_required
def recording_update(request, recording_id):
    logger.info(create_view_access_logging_message(request, recording_id))
    recording = get_object_or_404(Meeting, id=recording_id)
    gp: GeneralParameter = GeneralParameter.load(get_current_site(request))
    form = RecordingForm(request.POST or None, instance=recording)

    if not gp.recording_management_url or not gp.recording_key or not gp.recording_cert:
        messages.error(request, _("Editing of recordings is not enabled. Please contact the admin."))
        return redirect("recordings_list")

    if request.method == "POST" and form.is_valid():
        try:
            rpc: RecordingsGRPC = RecordingsGRPC(gp.recording_management_url, gp.recording_key, gp.recording_cert)
            _recording = form.save()
            params = {
                "recordID": recording.replay_id,
                "meta_meetingName": _recording.replay_title,
            }
            update_publish_recordings(params, rpc, True)
            messages.success(request, _("Recording title successfully updated."))
            logger.info("Title of recording with the ID {} was updated.".format(recording_id))
        except ObjectDoesNotExist:
            messages.error(request, _("Requested recording could not be found."))
            logger.error(
                "Requested recording associated with the meeting UUID {} could not be found.".format(recording_id)
            )
        except Exception as e:
            messages.error(
                request, _("There was an internal error with the requested recording. Please contact the support.")
            )
            logger.error("Internal server error for recording with UUID {} : {}".format(recording_id, str(e)))
        finally:
            del rpc
        return redirect("recordings_list")
    return render(request, "recording_update.html", {"form": form})


@login_required
def recording_mark_for_deletion(request, recording_id):
    logger.info(create_view_access_logging_message(request, recording_id))
    recording = get_object_or_404(Meeting, id=recording_id)
    if not user_can_interact_with_recording(request.user, recording):
        logger.warning(f"user {request.user} wants to mark {recording_id} for deletion and has no permissions")
        messages.warning(request, _("You are not allowed to interact with this recording"))
        return redirect("recordings_list")

    recording_deletion_period = GeneralParameter.objects.values_list("recording_deletion_period", flat=True).get(
        tenant=get_current_site(request)
    )
    logger.debug(f"user {request.user} wants to mark {recording_id} for deletion")
    recording.mark_for_deletion(timezone.now() + timedelta(hours=recording_deletion_period))
    messages.success(request, _("Recording was successfully marked for deletion"))
    return redirect("recordings_list")


@login_required
def recording_unmark_for_deletion(request, recording_id):
    logger.info(create_view_access_logging_message(request, recording_id))
    recording = get_object_or_404(Meeting, id=recording_id)
    if not user_can_interact_with_recording(request.user, recording):
        logger.warning(f"user {request.user} wants to unmark {recording_id} for deletion and has no permissions")
        messages.warning(request, _("You are not allowed to interact with this recording"))
        return redirect("recordings_list")
    logger.debug(f"user {request.user} wants to unmark {recording_id} for deletion")
    recording.unmark_for_deletion()
    messages.success(request, _("Recording was successfully unmarked for deletion"))
    return redirect("recordings_list")


@login_required
def recording_download_zip_redirect(request, recording_id):
    logger.info(create_view_access_logging_message(request, recording_id))
    recording = get_object_or_404(Meeting, id=recording_id)
    if not user_can_interact_with_recording(request.user, recording):
        logger.warning(f"user {request.user} wants to download zip for {recording_id} and has no permissions")
        messages.warning(request, _("You are not allowed to interact with this recording"))
        return redirect("recordings_list")
    logger.debug(f"user {request.user} wants to download zip for {recording_id}")
    return redirect(recording.get_zip_url())

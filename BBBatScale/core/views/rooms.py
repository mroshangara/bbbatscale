import logging

from core.constants import MEETING_STATE_CREATING, ROOM_STATE_CREATING, ROOM_STATE_INACTIVE
from core.decorators import tenant_based_permission_required
from core.filters import RoomFilter
from core.forms import RoomFilterFormHelper, RoomForm
from core.models import GeneralParameter, Room
from core.services import end_meeting
from core.views import create_view_access_logging_message
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.sites.shortcuts import get_current_site
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, redirect, render
from django.utils.translation import gettext_lazy as _

logger = logging.getLogger(__name__)


@login_required
@tenant_based_permission_required("core.view_room", raise_exception=True)
def rooms_overview(request):
    logger.info(create_view_access_logging_message(request))
    tenant = get_current_site(request)
    qs = Room.objects.filter(tenants__in=[tenant]).prefetch_related(
        "default_meeting_configuration", "scheduling_strategy"
    )
    f = RoomFilter(request.GET, queryset=qs, request=request)
    f.form.helper = RoomFilterFormHelper
    return render(request, "rooms_overview.html", {"filter": f})


@login_required
@tenant_based_permission_required("core.add_room", raise_exception=True)
def room_create(request):
    logger.info(create_view_access_logging_message(request))
    tenant = get_current_site(request)
    form = RoomForm(request.POST or None, user=request.user, general_parameter=GeneralParameter.load(tenant))
    if request.method == "POST" and form.is_valid():
        _room = form.save(commit=False)
        _room.save()
        _room.tenants.add(tenant)
        form.save_m2m()
        messages.success(request, _("Room {} was created successfully.").format(_room.name))
        # return to URL
        return redirect("rooms_overview")
    return render(request, "rooms_create.html", {"form": form})


@login_required
@tenant_based_permission_required("core.delete_room", raise_exception=True)
def room_delete(request, room):
    logger.info(create_view_access_logging_message(request, room))

    instance = get_object_or_404(Room, pk=room)
    instance.delete()
    messages.success(request, _("Room was deleted successfully."))
    return HttpResponseRedirect(request.META.get("HTTP_REFERER", "/"))


@login_required
@tenant_based_permission_required(["core.view_room", "core.change_room"], raise_exception=True)
def room_update(request, room):
    logger.info(create_view_access_logging_message(request, room))

    instance = get_object_or_404(Room, pk=room)
    tenant = get_current_site(request)
    form = RoomForm(
        request.POST or None, user=request.user, general_parameter=GeneralParameter.load(tenant), instance=instance
    )
    if request.method == "POST" and form.is_valid():
        _room = form.save(commit=False)
        _room.save()
        form.save_m2m()
        messages.success(request, _("Room {} was updated successfully.").format(_room.name))
        return redirect("rooms_overview")
    return render(request, "rooms_create.html", {"form": form})


@login_required
@tenant_based_permission_required("core.change_room", raise_exception=True)
def force_end_meeting(request, room_pk):
    logger.info(create_view_access_logging_message(request, room_pk))

    room = get_object_or_404(Room, pk=room_pk)
    if room.end_meeting():
        messages.success(request, _("Meeting in {} successfully ended.").format(room.name))
        logger.debug("Meeting in {} successfully ended.".format(room.name))
    else:
        messages.error(request, _("Error while trying to end meeting in {}.").format(room.name))
        logger.debug("Error while trying to end meeting in {}.".format(room.name))
    return redirect("rooms_overview")


@login_required
@tenant_based_permission_required("core.change_room", raise_exception=True)
def room_reset_stucked(request, room):
    logger.info(create_view_access_logging_message(request, room))

    instance = get_object_or_404(Room, pk=room)
    if instance.state == ROOM_STATE_CREATING:
        meeting = instance.meeting_set.filter(state=MEETING_STATE_CREATING).first()
        if meeting:
            end_meeting(meeting)
        else:
            instance.state = ROOM_STATE_INACTIVE
            instance.last_running = None
            instance.save()
        logger.debug("Room ({}) was in state 'creating' and was manual reset by {}.".format(room, request.user))
    else:
        logger.debug("Room ({}) was not in state 'creating' and will not be reset.".format(room))

    return redirect("rooms_overview")

import logging
from typing import Dict, Union

from core.constants import MEETING_STATE_RUNNING, ROOM_VISIBILITY_INTERNAL, ROOM_VISIBILITY_PUBLIC
from core.models import HomeRoom, Meeting, PersonalRoom, Room, User
from core.services import get_rooms_with_current_next_event
from core.views import create_view_access_logging_message
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import AnonymousUser
from django.contrib.sites.shortcuts import get_current_site
from django.core.paginator import Paginator
from django.db.models import Exists, OuterRef, Q, QuerySet, Subquery
from django.db.models.functions import Coalesce
from django.http import HttpRequest, HttpResponse, JsonResponse
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.views.decorators.http import require_http_methods
from support_chat.models import SupportChatParameter

logger = logging.getLogger(__name__)


def home(request: HttpRequest) -> HttpResponse:
    logger.info(create_view_access_logging_message(request))
    tenant = get_current_site(request)

    if SupportChatParameter.objects.load(tenant).enabled:
        disable_chat_if_offline = (
            "true" if SupportChatParameter.objects.load(tenant).disable_chat_if_offline else "false"
        )
    else:
        disable_chat_if_offline = "false"

    visibility = [ROOM_VISIBILITY_PUBLIC]
    if request.user.is_authenticated:
        visibility.append(ROOM_VISIBILITY_INTERNAL)

    return render(
        request,
        "home.html",
        {
            "disable_chat_if_offline": disable_chat_if_offline,
        },
    )


def get_rooms(request: HttpRequest) -> HttpResponse:
    tenant = get_current_site(request)
    search = request.GET.get("filter")

    visibility = [ROOM_VISIBILITY_PUBLIC]
    if request.user.is_authenticated:
        visibility.append(ROOM_VISIBILITY_INTERNAL)

    rooms_query = get_rooms_with_current_next_event(tenant, visibility).filter(
        Q(name__icontains=search)
        | Q(room_occupancy_current__icontains=search)
        | Q(room_occupancy_next__icontains=search)
    )

    rooms_query = annotate_is_favorite(rooms_query, request.user)
    if is_filter_only_favorite_rooms(request):
        rooms_query = filter_only_favorites_for_user(rooms_query, request.user)

    order_by = request.GET.get("orderBy")
    if order_by not in ("name", "state", "participant_count"):
        order_by = "name"

    return paginate(
        order_by, request.GET.get("descending", "").lower() == "true", rooms_query, int(request.GET.get("page", 1))
    )


def get_personal_rooms(request: HttpRequest) -> HttpResponse:
    tenant = get_current_site(request)
    search = request.GET.get("filter")

    visibility = [ROOM_VISIBILITY_PUBLIC]
    if request.user.is_authenticated:
        visibility.append(ROOM_VISIBILITY_INTERNAL)

    meeting = Meeting.objects.filter(room=OuterRef("pk"), state=MEETING_STATE_RUNNING)
    rooms_query = (
        PersonalRoom.objects.filter(
            Q(name__icontains=search) | Q(owner__first_name__icontains=search) | Q(owner__last_name__icontains=search),
            default_meeting_configuration__isnull=False,
            tenants__in=[tenant],
            visibility__in=visibility,
        )
        .annotate(participant_count=Coalesce(Subquery(meeting.values("participant_count")[:1]), 0))
        .prefetch_related("default_meeting_configuration", "owner")
    )

    rooms_query = annotate_is_favorite(rooms_query, request.user)
    if is_filter_only_favorite_rooms(request):
        rooms_query = filter_only_favorites_for_user(rooms_query, request.user)

    order_by = request.GET.get("orderBy")
    if order_by not in ("name", "owner", "state", "participant_count"):
        order_by = "name"

    if order_by == "owner":
        order_by = "owner__last_name"

    return paginate(
        order_by, request.GET.get("descending", "").lower() == "true", rooms_query, int(request.GET.get("page", 1))
    )


def get_home_rooms(request: HttpRequest) -> HttpResponse:
    tenant = get_current_site(request)
    search = request.GET.get("filter")

    visibility = [ROOM_VISIBILITY_PUBLIC]
    if request.user.is_authenticated:
        visibility.append(ROOM_VISIBILITY_INTERNAL)

    meeting = Meeting.objects.filter(room=OuterRef("pk"), state=MEETING_STATE_RUNNING)
    rooms_query = (
        HomeRoom.objects.filter(
            Q(owner__first_name__icontains=search) | Q(owner__last_name__icontains=search),
            default_meeting_configuration__isnull=False,
            tenants__in=[tenant],
            visibility__in=visibility,
        )
        .annotate(participant_count=Coalesce(Subquery(meeting.values("participant_count")[:1]), 0))
        .prefetch_related("default_meeting_configuration", "owner")
    )

    rooms_query = annotate_is_favorite(rooms_query, request.user)
    if is_filter_only_favorite_rooms(request):
        rooms_query = filter_only_favorites_for_user(rooms_query, request.user)

    order_by = request.GET.get("orderBy")
    if order_by not in ("owner", "state", "participant_count"):
        order_by = "owner"

    if order_by == "owner":
        order_by = "owner__last_name"

    return paginate(
        order_by, request.GET.get("descending", "").lower() == "true", rooms_query, int(request.GET.get("page", 1))
    )


@login_required
@require_http_methods(["POST", "DELETE"])
def favorite(request: HttpRequest, room: int) -> HttpResponse:
    user: User = request.user
    room: Room = get_object_or_404(Room, pk=room)

    if request.method == "POST":
        user.favorite_rooms.add(room)
    elif request.method == "DELETE":
        user.favorite_rooms.remove(room)

    return HttpResponse(status=204)


def paginate(order_by: str, descending: bool, query: QuerySet, page: int) -> JsonResponse:
    prefix = "-" if descending else ""

    # Always order by pk as fallback to prevent inconsistency with non-unique values
    paginator = Paginator(query.order_by(prefix + order_by, prefix + "pk"), 50)

    try:
        page_number = max(1, min(paginator.num_pages, int(page)))
    except ValueError:
        page_number = 1

    def map_room(room: Room) -> Dict[str, Union[str, int, None]]:
        mapped_room = {
            "url": reverse("join_redirect", args=[room.name]),
            "name": room.name,
            "state": room.state,
            "configuration": room.default_meeting_configuration.name,
            "participants": room.participant_count,
            "comment": room.comment_public,
        }

        if isinstance(room, (HomeRoom, PersonalRoom)):
            mapped_room["owner"] = str(room.owner)

        if hasattr(room, "room_occupancy_current") and hasattr(room, "room_occupancy_next"):
            mapped_room["current_occupancy"] = room.room_occupancy_current
            mapped_room["next_occupancy"] = room.room_occupancy_next

        if hasattr(room, "is_favorite"):
            mapped_room["is_favorite"] = room.is_favorite
            mapped_room["fav_url"] = reverse("api_set_favorite", args=[room.id])

        return mapped_room

    return JsonResponse(
        {
            "page": page_number,
            "page_count": paginator.num_pages,
            "rooms": [map_room(room) for room in paginator.page(page_number).object_list],
        }
    )


def annotate_is_favorite(query: QuerySet, user: Union[User, AnonymousUser]) -> QuerySet:
    if user.is_authenticated:
        return query.annotate(is_favorite=Exists(user.favorite_rooms.filter(pk=OuterRef("pk"))))

    return query


def filter_only_favorites_for_user(query: QuerySet, user: User) -> QuerySet:
    if user.is_authenticated:
        return query.filter(Exists(user.favorite_rooms.filter(pk=OuterRef("pk"))))

    return query


def is_filter_only_favorite_rooms(request) -> bool:
    return True if request.GET.get("only_favorites", "").lower() == "true" else False

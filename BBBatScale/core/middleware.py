from types import SimpleNamespace

from channels.db import database_sync_to_async
from channels.middleware import BaseMiddleware
from django.contrib.sites.shortcuts import get_current_site
from django.utils.functional import LazyObject


class TenantLazyObject(LazyObject):
    def __init__(self, host: str):
        super().__init__()
        # Assign to __dict__ to avoid infinite __setattr__ loops.
        self.__dict__["_host"] = host

    def _setup(self):
        raise ValueError("Accessing tenant before it is ready.")


class TenantMiddleware(BaseMiddleware):
    def populate_scope(self, scope):
        host_headers = [value for key, value in scope["headers"] if key == b"host"]

        if len(host_headers) == 0:
            raise ValueError("The required host HTTP-Header is not present.")
        elif len(host_headers) != 1:
            raise ValueError("The host HTTP-Header is specified more than once.")
        elif "tenant" not in scope:
            scope["tenant"] = TenantLazyObject(host_headers[0].decode("ISO-8859-1"))

    async def resolve_scope(self, scope):
        # Create fake request object only supporting the method `get_host` to be able to use `get_current_site`
        fake_request = SimpleNamespace(get_host=lambda: scope["tenant"].__dict__["_host"])
        scope["tenant"]._wrapped = await database_sync_to_async(get_current_site)(fake_request)

from core.models import ApiToken
from django import template
from django.template import RequestContext
from django.urls import reverse

register = template.Library()


@register.simple_tag(takes_context=True, name="build_absolut_token_url")
def build_absolut_token_url(context: RequestContext, token: ApiToken) -> str:
    return context.request.build_absolute_uri(reverse("bbb_api_initial_request", args=[token.slug]))

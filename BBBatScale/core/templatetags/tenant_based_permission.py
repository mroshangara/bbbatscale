from typing import Iterable, Iterator, List, Optional, Tuple, Union

from core.utils import has_tenant_based_perms
from django import template
from django.contrib.sites.shortcuts import get_current_site
from django.template import RequestContext
from django.template.base import (
    FilterExpression,
    Node,
    NodeList,
    Parser,
    TemplateSyntaxError,
    Token,
    VariableDoesNotExist,
)

register = template.Library()


class IfHasTenantBasedPermsNode(Node):
    def __init__(
        self,
        blocks: Iterable[Tuple[Optional[Iterable[FilterExpression]], Union[bool, FilterExpression], NodeList]],
    ) -> None:
        self.blocks = blocks

    def __repr__(self) -> str:
        return "<%s>" % self.__class__.__name__

    def __iter__(self) -> Iterator[NodeList]:
        for _, _, nodelist in self.blocks:
            yield from nodelist

    @property
    def nodelist(self) -> NodeList:
        return NodeList(self)

    def render(self, context: RequestContext) -> str:
        for perm_list, elevate_staff, nodelist in self.blocks:
            if isinstance(elevate_staff, FilterExpression):
                elevate_staff = elevate_staff.resolve(context)
                if not isinstance(elevate_staff, bool):
                    raise TemplateSyntaxError("The parameter 'elevate_staff' expects a boolean.")

            if perm_list is not None:  # if / elif clause
                try:
                    resolved_perm_list = list(perm.resolve(context) for perm in perm_list)
                    match = has_tenant_based_perms(
                        context.request.user,
                        resolved_perm_list,
                        get_current_site(context.request),
                        elevate_staff=elevate_staff,
                    )
                except VariableDoesNotExist:
                    match = None
            else:  # else clause
                match = True

            if match:
                return nodelist.render(context)

        return ""


def parse_bits(parser: Parser, bits: Iterable[str]) -> Tuple[List[str], Union[bool, FilterExpression]]:
    elevate_staff = True
    perm_list = list()

    for bit in bits:
        key, sep, value = bit.partition("=")
        if sep == "=":
            if key == "elevate_staff":
                elevate_staff = parser.compile_filter(value)
        else:
            perm_list.append(parser.compile_filter(bit))

    return perm_list, elevate_staff


@register.tag("if_has_tenant_based_perms")
def do_if(parser: Parser, token: Token) -> IfHasTenantBasedPermsNode:
    # {% if_has_tenant_based_perm ... %}
    bits = token.split_contents()
    perm_list, elevate_staff = parse_bits(parser, bits[1:])
    if len(perm_list) == 0:
        raise TemplateSyntaxError(f"Tag '{bits[0]}' expects at least one permission.")
    nodelist = parser.parse(("elif_has_tenant_based_perms", "else", "endif_has_tenant_based_perms"))
    permlists_nodelists = [(perm_list, elevate_staff, nodelist)]
    token = parser.next_token()

    # {% elif_has_tenant_based_perm ... %} (repeatable)
    while token.contents.startswith("elif_has_tenant_based_perms"):
        bits = token.split_contents()
        perm_list, elevate_staff = parse_bits(parser, bits[1:])
        if len(perm_list) == 0:
            raise TemplateSyntaxError(f"Tag '{bits[0]}' expects at least one permission.")
        nodelist = parser.parse(("elif_has_tenant_based_perms", "else", "endif_has_tenant_based_perms"))
        permlists_nodelists.append((perm_list, elevate_staff, nodelist))
        token = parser.next_token()

    # {% else %} (optional)
    if token.contents == "else":
        nodelist = parser.parse(("endif_has_tenant_based_perms",))
        permlists_nodelists.append((None, False, nodelist))
        token = parser.next_token()

    # {% endif_has_tenant_based_perm %}
    if token.contents != "endif_has_tenant_based_perms":
        raise TemplateSyntaxError('Malformed template tag at line {}: "{}"'.format(token.lineno, token.contents))

    return IfHasTenantBasedPermsNode(permlists_nodelists)


@register.simple_tag(takes_context=True, name="has_tenant_based_perms")
def do_has_tenant_based_perms(context: RequestContext, *perms: str, elevate_staff: bool = True) -> bool:
    return has_tenant_based_perms(
        context.request.user, perms, get_current_site(context.request), elevate_staff=elevate_staff
    )

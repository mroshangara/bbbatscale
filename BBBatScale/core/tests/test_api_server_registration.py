import binascii
import json
from datetime import datetime, timedelta

import pytest
from core.models import AgentConfiguration, ApiToken, SchedulingStrategy, Server
from core.webhooks import webhook_compute_mac_header
from django.test import Client
from freezegun import freeze_time


@pytest.fixture(scope="function")
def some_arbitrary_scheduling_strategy(testserver_tenant):
    return SchedulingStrategy.objects.create(name="jovial-einstein")


@pytest.fixture(scope="function")
def communication_token(some_arbitrary_scheduling_strategy):
    return ApiToken.objects.create(
        scheduling_strategy=some_arbitrary_scheduling_strategy, secret="abcdef", name="jovial-einstein"
    )


@pytest.fixture(scope="function")
def encoded_body():
    body = {
        "hostname": "sad-galileo.example.com.",
        "machine_id": "0aef96f5175145d6a8e6437ea6755122",
        "shared_secret": "a7tfbVF_KRgI2GNkx16dG5iXVrVGeh_l_gPpfqovDnA",
    }
    return json.dumps(body, separators=(",", ":"), sort_keys=True).encode("utf-8")


@freeze_time("2020-04-26T16:29:55+00:00")
@pytest.mark.django_db
def test_api_server_registration_happy_path(communication_token, encoded_body):
    agent_config = AgentConfiguration.objects.create(registration_interval=timedelta(seconds=10))

    communication_token.scheduling_strategy.agent_configuration = agent_config
    communication_token.scheduling_strategy.save()

    c = Client()
    mac_timestamp = int(datetime.now().timestamp())
    sig = webhook_compute_mac_header(binascii.unhexlify(communication_token.secret), mac_timestamp, encoded_body)

    # Initial registration
    resp = c.post(
        "/core/api/servers/registration",
        data=encoded_body,
        content_type="application/json",
        HTTP_X_BBBATSCALE_EXPERIMENTAL_API="iacceptmyfate",
        HTTP_X_BBBATSCALE_TENANT="jovial-einstein",
        HTTP_X_REQUEST_SIGNATURE=sig,
    )

    assert resp.status_code == 200

    resp_parsed = json.loads(resp.content)
    assert resp_parsed == {"interval": 10, "created": True}

    # BBB shared secret has changed. Ensure it is properly updated.
    body = json.loads(encoded_body)
    body["shared_secret"] = "CHANGED"
    new_encoded_body = json.dumps(body, separators=(",", ":"), sort_keys=True).encode("utf-8")
    new_sig = webhook_compute_mac_header(
        binascii.unhexlify(communication_token.secret), mac_timestamp, new_encoded_body
    )

    resp = Client().post(
        "/core/api/servers/registration",
        data=new_encoded_body,
        content_type="application/json",
        HTTP_X_BBBATSCALE_EXPERIMENTAL_API="iacceptmyfate",
        HTTP_X_BBBATSCALE_TENANT="jovial-einstein",
        HTTP_X_REQUEST_SIGNATURE=new_sig,
    )

    assert resp.status_code == 200
    resp_parsed = json.loads(resp.content)
    assert resp_parsed == {"interval": 10, "created": False}

    server = Server.objects.get(machine_id=body["machine_id"])
    assert server.shared_secret == "CHANGED"


@freeze_time("2020-04-26T16:29:55+00:00")
@pytest.mark.django_db
def test_api_server_registration_error_handling(communication_token, encoded_body):
    c = Client()
    mac_timestamp = int(datetime.now().timestamp())
    sig = webhook_compute_mac_header(binascii.unhexlify(communication_token.secret), mac_timestamp, encoded_body)

    # Wrong Content-Type
    resp = c.post(
        "/core/api/servers/registration",
        data=encoded_body,
        content_type="multipart/form-data",
        HTTP_X_BBBATSCALE_EXPERIMENTAL_API="iacceptmyfate",
        HTTP_X_BBBATSCALE_TENANT="jovial-einstein",
        HTTP_X_REQUEST_SIGNATURE=sig,
    )

    assert resp.status_code == 415

    # Wrong method
    resp = c.get(
        "/core/api/servers/registration",
        HTTP_X_BBBATSCALE_EXPERIMENTAL_API="iacceptmyfate",
        HTTP_X_BBBATSCALE_TENANT="jovial-einstein",
    )

    assert resp.status_code == 405

    # Missing opt-in to experimental API surface
    resp = c.post(
        "/core/api/servers/registration",
        data=encoded_body,
        content_type="application/json",
        HTTP_X_BBBATSCALE_TENANT="jovial-einstein",
        HTTP_X_REQUEST_SIGNATURE=sig,
    )

    assert resp.status_code == 400

    # Non-existant scheduling_strategy
    resp = c.post(
        "/core/api/servers/registration",
        data=encoded_body,
        content_type="application/json",
        HTTP_X_BBBATSCALE_EXPERIMENTAL_API="iacceptmyfate",
        HTTP_X_BBBATSCALE_TENANT="tender-franklin",
        HTTP_X_REQUEST_SIGNATURE=sig,
    )

    assert resp.status_code == 403

    # Corrupted signature
    corrupted_sig = sig[:-2] + "ff"
    resp = c.post(
        "/core/api/servers/registration",
        data=encoded_body,
        content_type="application/json",
        HTTP_X_BBBATSCALE_EXPERIMENTAL_API="iacceptmyfate",
        HTTP_X_BBBATSCALE_TENANT="jovial-einstein",
        HTTP_X_REQUEST_SIGNATURE=corrupted_sig,
    )

    assert resp.status_code == 403

    # Correct but stale request (older than 1h)
    stale_sig = webhook_compute_mac_header(
        binascii.unhexlify(communication_token.secret), mac_timestamp - 3601, encoded_body
    )

    resp = c.post(
        "/core/api/servers/registration",
        data=encoded_body,
        content_type="application/json",
        HTTP_X_BBBATSCALE_EXPERIMENTAL_API="iacceptmyfate",
        HTTP_X_BBBATSCALE_TENANT="jovial-einstein",
        HTTP_X_REQUEST_SIGNATURE=stale_sig,
    )

    assert resp.status_code == 403

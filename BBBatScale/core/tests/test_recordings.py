import pytest
from core.models import Meeting, Site, User


@pytest.mark.django_db
def test_recordings_anonymous_user(client):
    response = client.get("/p/some-recordings-id")
    assert response.url == "/login?next=/p/some-recordings-id"


@pytest.mark.django_db
def test_recordings_not_found(client):
    user = User.objects.create_user(
        username="Testuser",
        password="TestUserPassword1",
        tenant=Site.objects.create(domain="testserver", name="testserver"),
    )
    client.force_login(user)
    response = client.get("/p/some-recordings-id")
    assert response.status_code == 404


@pytest.mark.django_db
def test_recordings_found(client):
    Meeting.objects.create(
        room_name="Example Room", replay_id="example-replay-id", replay_url="https://example.org/my-awesome-recording"
    )
    user = User.objects.create_user(
        username="Testuser",
        password="TestUserPassword1",
        tenant=Site.objects.create(domain="testserver", name="testserver"),
    )
    client.force_login(user)
    response = client.get("/p/example-replay-id")
    assert response.status_code == 302
    assert response.url == "https://example.org/my-awesome-recording"

    Meeting.objects.create(
        room_name="Example Room 2",
        replay_id="recordings--example-replay-id-2",
        replay_url="https://example.org/my-awesome-recording-2",
    )

    response = client.get("/p/example-replay-id-2")
    assert response.status_code == 302
    assert response.url == "https://example.org/my-awesome-recording-2"

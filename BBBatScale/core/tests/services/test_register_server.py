from core.constants import SERVER_STATE_WAITING_RESPONSE
from core.models import SchedulingStrategy, Server
from core.services import RegistrationDetails, register_server


def create_registration_details(number: int, scheduling_strategy: SchedulingStrategy):
    return RegistrationDetails(
        scheduling_strategy=scheduling_strategy,
        dns=f"test{number}.com",
        machine_id="1234abcd",
        shared_secret="secret",
    )


def test_register_server_base_ok(example_scheduling_strategy):
    registration_details = create_registration_details(0, example_scheduling_strategy)

    created = register_server(registration_details)

    server = Server.objects.get(dns=registration_details.dns)

    assert created is True
    assert server.shared_secret == registration_details.shared_secret
    assert server.scheduling_strategy == example_scheduling_strategy
    assert server.machine_id == registration_details.machine_id
    assert server.state == SERVER_STATE_WAITING_RESPONSE
    assert server.last_health_check is None

import pytest
from core.models import Room, SchedulingStrategy, Server, User, get_default_room_config
from core.views.home import annotate_is_favorite, filter_only_favorites_for_user
from django.contrib.auth.models import AnonymousUser
from django.contrib.sites.models import Site


@pytest.fixture(scope="function")
def example_tenant(db) -> Site:
    return Site.objects.create(name="example.org", domain="example.org")


@pytest.fixture(scope="function")
def user(db, example_tenant) -> User:
    user = User.objects.create(
        username="user",
        email="user@example.org",
        display_name="example_username",
        is_superuser=False,
        is_staff=True,
        tenant=example_tenant,
    )
    return user


@pytest.fixture(scope="function")
def example(db) -> SchedulingStrategy:
    return SchedulingStrategy.objects.create(
        name="example",
    )


@pytest.fixture(scope="function")
def example_server(db, example) -> Server:
    return Server.objects.create(
        scheduling_strategy=example,
        dns="example.org",
        shared_secret="123456789",
    )


@pytest.fixture(scope="function")
def room(db, example, example_server) -> Room:
    return Room.objects.create(
        scheduling_strategy=example,
        name="room",
        default_meeting_configuration=get_default_room_config(),
    )


@pytest.fixture(scope="function")
def room2(db, example, example_server) -> Room:
    return Room.objects.create(
        scheduling_strategy=example,
        name="room2",
        default_meeting_configuration=get_default_room_config(),
    )


def test_join_favorites_for_user(room, room2, user):
    user.favorite_rooms.add(room)

    rooms_query = Room.objects.all()
    rooms_query = annotate_is_favorite(rooms_query, user)
    queried_room = rooms_query.get(name=room.name)
    queried_room2 = rooms_query.get(name=room2.name)

    assert queried_room.is_favorite is True
    assert queried_room2.is_favorite is False


def test_join_favorites_for_user_anonymous(room):
    anonymous_user = AnonymousUser()

    room_query = Room.objects.filter(name=room.name)
    room_query = annotate_is_favorite(room_query, anonymous_user)
    queried_room = room_query.first()

    assert not hasattr(queried_room, "is_favorite")


def test_filter_only_favorites_for_user(room, room2, user):
    user.favorite_rooms.add(room)

    rooms_query = Room.objects.all()
    rooms_query = filter_only_favorites_for_user(rooms_query, user)

    rooms_query = rooms_query.all()

    assert room in rooms_query
    assert room2 not in rooms_query

import pytest
from core.constants import MODERATION_MODE_MODERATORS
from core.models import Room, SchedulingStrategy, Server


@pytest.fixture(scope="function")
def example(db) -> SchedulingStrategy:
    return SchedulingStrategy.objects.create(
        name="example",
    )


@pytest.fixture(scope="function")
def example_server(db, example) -> Server:
    return Server.objects.create(
        scheduling_strategy=example,
        dns="example.org",
        shared_secret="123456789",
    )


@pytest.fixture(scope="function")
def room_d14_0303(db, example, example_server) -> Room:
    return Room.objects.create(
        scheduling_strategy=example,
        server=example_server,
        name="D14/03.03",
        attendee_pw="test_attendee_password",
        moderator_pw="test_moderator_password",
        moderation_mode=MODERATION_MODE_MODERATORS,
    )


@pytest.fixture(scope="function")
def room_d14_0301(db, example, example_server) -> Room:
    return Room.objects.create(
        scheduling_strategy=example,
        server=example_server,
        name="D14/03.01",
        attendee_pw="test_attendee_password",
        moderator_pw="test_moderator_password",
        moderation_mode=MODERATION_MODE_MODERATORS,
    )

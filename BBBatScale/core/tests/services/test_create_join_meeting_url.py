import pytest
from core.constants import MEETING_STATE_RUNNING
from core.models import Meeting, MeetingConfiguration, Room, SchedulingStrategy, Server, User, get_default_room_config
from core.services import create_join_meeting_url
from django.contrib.sites.models import Site
from faker import Faker


@pytest.fixture(scope="function")
def example(db) -> SchedulingStrategy:
    return SchedulingStrategy.objects.create(
        name="example",
    )


@pytest.fixture(scope="function")
def example_server(db, example) -> Server:
    return Server.objects.create(
        scheduling_strategy=example,
        dns="example.org",
        shared_secret="123456789",
    )


@pytest.fixture(scope="function")
def room(db, example, example_server) -> Room:
    return Room.objects.create(
        scheduling_strategy=example,
        name="room",
        default_meeting_configuration=get_default_room_config(),
    )


@pytest.fixture(scope="function")
def example_meeting(db, example, example_server, room) -> Meeting:
    Faker.seed(1337)
    return Meeting.objects.create(
        room=room,
        room_name=room.name,
        server=example_server,
        id=Faker().uuid4(),
        state=MEETING_STATE_RUNNING,
        attendee_pw="test_attendee_password",
        moderator_pw="test_moderator_password",
        configuration=MeetingConfiguration.objects.create(name="Example Config"),
    )


@pytest.fixture(scope="function")
def example_tenant(db) -> Site:
    return Site.objects.create(name="example.org", domain="example.org")


@pytest.fixture(scope="function")
def user(db, example_tenant) -> User:
    user = User.objects.create(
        username="user",
        email="user@example.org",
        display_name="example_username",
        is_superuser=False,
        is_staff=True,
        tenant=example_tenant,
    )
    return user


def test_create_join_meeting_url(example_meeting, user):
    assert create_join_meeting_url(example_meeting, "test_username", "test_user_pw") == (
        "https://example.org/bigbluebutton/api/join?meetingID=b5bab1cd-8884-47a5-acef-e37b9e250d03&"
        "password=test_user_pw&fullName=test_username&redirect=true"
        "&checksum=5a2eae48c9c5c0ecce41373a8d8cdbc8c54773eb"
    )

    assert create_join_meeting_url(example_meeting, user, "test_user_pw") == (
        "https://example.org/bigbluebutton/api/join?meetingID=b5bab1cd-8884-47a5-acef-e37b9e250d03&"
        "password=test_user_pw&fullName=example_username&redirect=true&userdata-bbb_auto_join_audio=true&"
        "userdata-bbb_listen_only_mode=true&userdata-bbb_skip_check_audio=false&"
        "userdata-bbb_skip_check_audio_on_first_join=false&userdata-bbb_auto_share_webcam=false&"
        "userdata-bbb_record_video=true&userdata-bbb_skip_video_preview=false&"
        "userdata-bbb_skip_video_preview_on_first_join=false&userdata-bbb_mirror_own_webcam=false&"
        "userdata-bbb_force_restore_presentation_on_new_events=false&userdata-bbb_auto_swap_layout=false&"
        "userdata-bbb_show_participants_on_login=true&userdata-bbb_show_public_chat_on_login=true&"
        "checksum=e494d89608f6abb94f0806436f754e9d29b6e26c"
    )

import uuid
from unittest.mock import Mock

import pytest
from core.services import clean_get_meetings_item, collect_get_meetings_items, fetch_get_meetings_item
from core.tests.helper import (
    get_meeting_info_failed_xml,
    get_meeting_info_full_response_xml,
    get_meeting_info_invalid_xml,
    mock_response,
)
from core.utils import BigBlueButton


def test_fetch_get_meetings_item_ok(example_meeting):
    class MockBBB(BigBlueButton):
        def get_meeting_infos(self, meeting_id):
            return mock_response(get_meeting_info_full_response_xml(meeting_id))

    def create_bbb(dns, secret):
        return MockBBB(dns, secret)

    item = fetch_get_meetings_item(example_meeting, create_bbb)

    assert item["response"]["meetingID"] == str(example_meeting.id)


@pytest.mark.parametrize("fail_response_xml", [get_meeting_info_failed_xml(), get_meeting_info_invalid_xml()])
def test_fetch_get_meetings_item(example_meeting, fail_response_xml):
    class MockBBB(BigBlueButton):
        def get_meeting_infos(self, meeting_id):
            return mock_response(fail_response_xml)

    def create_bbb(dns, secret):
        return MockBBB(dns, secret)

    item = fetch_get_meetings_item(example_meeting, create_bbb)
    assert item is None


@pytest.mark.parametrize(
    "input,expected",
    [
        ({"response": {"returncode": "SUCCESS", "meetingID": uuid.UUID(int=0)}}, {"meetingID": uuid.UUID(int=0)}),
        (
            {"returncode": "SUCCESS", "meetingID": uuid.UUID(int=0)},
            {"returncode": "SUCCESS", "meetingID": uuid.UUID(int=0)},
        ),
        ({"meetingID": uuid.UUID(int=0)}, {"meetingID": uuid.UUID(int=0)}),
        ({}, {}),
    ],
)
def test_clean_get_meetings_item(input, expected):
    actual = clean_get_meetings_item(input)
    assert actual == expected


def test_collect_get_meetings_items(example_meeting):
    bbb = Mock(spec=BigBlueButton)
    bbb.get_meeting_infos.side_effect = [
        mock_response(get_meeting_info_full_response_xml(example_meeting.id)),
        mock_response(get_meeting_info_failed_xml()),
        mock_response(get_meeting_info_invalid_xml()),
    ]

    def create_mock_bbb(dns, secret):
        return bbb

    meetings = [example_meeting, example_meeting, example_meeting]

    items = collect_get_meetings_items(meetings, create_mock_bbb)

    assert len(list(items)) == 1

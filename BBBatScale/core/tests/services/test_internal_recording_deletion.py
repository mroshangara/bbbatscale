from datetime import timedelta

import pytest
from core.models import Meeting
from core.services import user_can_interact_with_recording, user_is_staff_of_tenant
from django.utils import timezone
from freezegun import freeze_time


@pytest.fixture(scope="function")
def example_meeting() -> Meeting:
    return Meeting.objects.create(room_name="ExampleMeeting", creator="Example User")


@pytest.fixture(scope="function")
def example_meeting_2() -> Meeting:
    return Meeting.objects.create(room_name="ExampleMeeting2", creator="Example User2")


@pytest.fixture(scope="function")
def example_meeting_3() -> Meeting:
    return Meeting.objects.create(room_name="ExampleMeeting3", creator="Example User3")


@pytest.mark.django_db
def test_mark_recording_for_deletion(example_meeting, current_timestamp):
    with freeze_time(current_timestamp):
        example_meeting.mark_for_deletion(timezone.now() + timedelta(hours=72))

    assert (
        current_timestamp + timedelta(hours=72) == Meeting.objects.filter(room_name="ExampleMeeting").first().to_delete
    )


@pytest.mark.django_db
def test_unmark_recording_for_deletion(example_meeting, current_timestamp):
    with freeze_time(current_timestamp):
        example_meeting.mark_for_deletion(timezone.now() + timedelta(hours=72))
    example_meeting.unmark_for_deletion()
    assert Meeting.objects.filter(room_name="ExampleMeeting").first().to_delete is None


@pytest.mark.django_db
def test_meeting_for_deletion_query(example_meeting, example_meeting_2, example_meeting_3, current_timestamp):
    with freeze_time(current_timestamp):
        example_meeting.mark_for_deletion(timezone.now() + timedelta(hours=-2))
        example_meeting_2.mark_for_deletion(timezone.now() + timedelta(hours=72))
        example_meeting_3.mark_for_deletion(timezone.now())
        assert 2 == Meeting.objects.for_deletion().count()


# Test filter_by_user Meeting query
@pytest.mark.django_db
def test_filter_meeting_by_username(example_user, example_meeting):
    example_meeting.creator = example_user.username
    example_meeting.save()
    assert Meeting.objects.filter_by_user(example_user).count() == 1


@pytest.mark.django_db
def test_filter_meeting_by_lastname_firstname(example_user, example_meeting):
    example_meeting.creator = f"{example_user.last_name}, {example_user.first_name}"
    example_meeting.save()
    assert Meeting.objects.filter_by_user(example_user).count() == 1


@pytest.mark.django_db
def test_filter_meeting_by_display_name(example_user, example_meeting):
    example_meeting.creator = example_user.display_name
    example_meeting.save()
    assert Meeting.objects.filter_by_user(example_user).count() == 1


@pytest.mark.django_db
def test_filter_meeting_by_lastname(example_user, example_meeting):
    example_meeting.creator = example_user.last_name
    example_meeting.save()
    assert Meeting.objects.filter_by_user(example_user).count() == 1


@pytest.mark.django_db
def test_filter_meeting_by_firstname(example_user, example_meeting):
    example_meeting.creator = example_user.first_name
    example_meeting.save()
    assert Meeting.objects.filter_by_user(example_user).count() == 1


@pytest.mark.django_db
def test_filter_meeting_by_email(example_user, example_meeting):
    example_meeting.creator = example_user.email
    example_meeting.save()
    assert Meeting.objects.filter_by_user(example_user).count() == 1


@pytest.mark.django_db
def test_user_is_recordings_owner_username(example_user, example_meeting):
    example_meeting.creator = example_user.username
    assert example_meeting.is_owned_by(example_user) is True


@pytest.mark.django_db
def test_user_is_recordings_owner_last_first_name(example_user, example_meeting):
    example_meeting.creator = f"{example_user.last_name}, {example_user.first_name}"
    assert example_meeting.is_owned_by(example_user) is True


@pytest.mark.django_db
def test_user_is_recordings_owner_display_name(example_user, example_meeting):
    example_meeting.creator = f"{example_user.display_name}"
    assert example_meeting.is_owned_by(example_user) is True


@pytest.mark.django_db
def test_user_is_recordings_owner_last_name(example_user, example_meeting):
    example_meeting.creator = f"{example_user.last_name}"
    assert example_meeting.is_owned_by(example_user) is True


@pytest.mark.django_db
def test_user_is_recordings_owner_first_name(example_user, example_meeting):
    example_meeting.creator = f"{example_user.first_name}"
    assert example_meeting.is_owned_by(example_user) is True


@pytest.mark.django_db
def test_user_is_recordings_owner_email(example_user, example_meeting):
    example_meeting.creator = f"{example_user.email}"
    assert example_meeting.is_owned_by(example_user) is True


@pytest.mark.django_db
def test_user_is_recordings_owner_false(example_user, example_meeting):
    assert example_meeting.is_owned_by(example_user) is False


@pytest.mark.django_db
def test_user_is_staff_of_tenant_staff_not_in_tenants(example_staff_user, example_meeting):
    assert user_is_staff_of_tenant(example_staff_user, example_meeting.tenants.all()) is False


@pytest.mark.django_db
def test_user_is_staff_of_tenant_staff_none_staff(example_user, example_meeting):
    assert user_is_staff_of_tenant(example_user, example_meeting.tenants.all()) is False


@pytest.mark.django_db
def test_user_is_staff_of_tenant_staff(example_staff_user, example_meeting, testserver_tenant):
    example_meeting.tenants.add(testserver_tenant)
    example_meeting.save()
    assert user_is_staff_of_tenant(example_staff_user, example_meeting.tenants.all()) is True


@pytest.mark.django_db
def test_user_can_mark_recording_for_deletion_superuser(example_superuser, example_meeting):
    assert user_can_interact_with_recording(example_superuser, example_meeting) is True


@pytest.mark.django_db
def test_user_can_mark_recording_for_deletion_staff_user(
    example_staff_user, example_meeting, example_meeting_2, testserver_tenant
):
    example_meeting.tenants.add(testserver_tenant)
    assert user_can_interact_with_recording(example_staff_user, example_meeting) is True
    assert user_can_interact_with_recording(example_staff_user, example_meeting_2) is False


@pytest.mark.django_db
def test_user_can_mark_recording_for_deletion_user(example_user, example_meeting, example_meeting_2):
    example_meeting.creator = f"{example_user.email}"
    assert user_can_interact_with_recording(example_user, example_meeting) is True
    assert user_can_interact_with_recording(example_user, example_meeting_2) is False

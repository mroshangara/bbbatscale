import uuid
from datetime import datetime, timezone

import pytest
from core import constants
from core.constants import SERVER_STATE_DOWN, SERVER_STATE_ERROR, SERVER_STATE_UP
from core.models import Meeting, Server
from core.services import MeetingStats, collect_server_stats, sync_meeting_stats, sync_server_stats
from core.utils import BigBlueButton
from freezegun import freeze_time


def example_meeting_stats(number: int, count=0, is_breakout=False):
    return MeetingStats(
        meeting_id=uuid.UUID(int=number),
        bbb_meeting_id=f"internal_id_{number}",
        rooms_meeting_id=uuid.UUID(int=number),
        name=f"meeting_{number}",
        participant_count=count,
        video_count=count,
        attendee_pw=f"attendee_pw_{number}",
        moderator_pw=f"moderator_pw_{number}",
        creator=f"creator_{number}",
        is_breakout=is_breakout,
    )


def assert_meeting(meeting, meeting_stats):
    assert meeting_stats.name == meeting.room_name
    assert constants.MEETING_STATE_RUNNING == meeting.state
    assert meeting_stats.attendee_pw == meeting.attendee_pw
    assert meeting_stats.moderator_pw == meeting.moderator_pw
    assert meeting_stats.participant_count == meeting.participant_count
    assert meeting_stats.video_count == meeting.videostream_count
    assert meeting_stats.creator == meeting.creator
    assert meeting_stats.bbb_meeting_id == meeting.bbb_meeting_id


def test_sync_meeting_stats_create_meeting(example_server, current_timestamp):
    meeting_stats = example_meeting_stats(0)

    with freeze_time(current_timestamp):
        sync_meeting_stats(example_server, meeting_stats)

    meeting = Meeting.objects.get(id=meeting_stats.meeting_id)

    assert_meeting(meeting, meeting_stats)
    assert example_server == meeting.server
    assert current_timestamp == meeting.last_health_check
    assert meeting.room is None


def test_sync_meeting_stats_update_meeting(example_server, example_room, current_timestamp):
    meeting_stats = example_meeting_stats(0)

    example_room.state = constants.ROOM_STATE_CREATING

    Meeting.objects.create(id=meeting_stats.meeting_id, room=example_room)

    with freeze_time(current_timestamp):
        sync_meeting_stats(example_server, meeting_stats)

    meeting = Meeting.objects.get(id=meeting_stats.meeting_id)
    assert_meeting(meeting, meeting_stats)
    assert example_server == meeting.server
    assert current_timestamp == meeting.last_health_check

    assert meeting.room is not None
    assert constants.ROOM_STATE_ACTIVE == meeting.room.state
    assert current_timestamp == meeting.room.last_running


@pytest.mark.parametrize(
    "server_state", [s for s, _ in constants.SERVER_STATES if s != constants.SERVER_STATE_DISABLED]
)
def test_sync_server_stats(example_server, current_timestamp, server_state):
    example_server.state = server_state
    example_server.save()

    with freeze_time(current_timestamp):
        sync_server_stats(example_server, [])

    server = Server.objects.get(dns=example_server.dns)
    assert constants.SERVER_STATE_UP == server.state
    assert current_timestamp == server.last_health_check


def test_sync_server_stats_disabled_server(example_server, current_timestamp):
    example_server.state = constants.SERVER_STATE_DISABLED
    example_server.save()

    with freeze_time(current_timestamp):
        sync_server_stats(example_server, [])

    server = Server.objects.get(dns=example_server.dns)
    assert constants.SERVER_STATE_DISABLED == server.state
    assert current_timestamp == server.last_health_check


def create_meeting_stats_api_payload(number):
    return {
        "meetingID": str(uuid.UUID(int=number)),
        "BBBMeetingID": "internal_meeting_id",
        "roomsMeetingID": str(uuid.UUID(int=number)),
        "name": "name",
        "participantCount": 0,
        "videoCount": 0,
        "attendeePW": "pw_a",
        "moderatorPW": "pw_m",
        "isBreakout": "false",
        "creator": "creator",
    }


def test_meeting_stats_from_api_ok():
    meeting_stats = MeetingStats.from_api(create_meeting_stats_api_payload(0))
    assert uuid.UUID(int=0) == meeting_stats.meeting_id
    assert "internal_meeting_id" == meeting_stats.bbb_meeting_id
    assert uuid.UUID(int=0) == meeting_stats.rooms_meeting_id
    assert "name" == meeting_stats.name
    assert 0 == meeting_stats.participant_count
    assert 0 == meeting_stats.video_count
    assert "pw_a" == meeting_stats.attendee_pw
    assert "pw_m" == meeting_stats.moderator_pw
    assert meeting_stats.is_breakout is False
    assert "creator" == meeting_stats.creator


def test_meeting_stats_from_api_uuid_exception():
    with pytest.raises(ValueError):
        payload = create_meeting_stats_api_payload(0)
        payload["meetingID"] = "1234asdf"
        MeetingStats.from_api(payload)

    with pytest.raises(ValueError):
        payload = create_meeting_stats_api_payload(0)
        payload["roomsMeetingID"] = "1234asdf"
        MeetingStats.from_api(payload)


def test_meeting_stats_from_api_is_breakout_false():
    payload = create_meeting_stats_api_payload(0)
    payload["isBreakout"] = "true"
    stats = MeetingStats.from_api(payload)
    assert stats.is_breakout is False


def create_meeting_stats_bbb_payload(number):
    return {
        "meetingID": str(uuid.UUID(int=number)),
        "internalMeetingID": "internal_meeting_id",
        "meetingName": "name",
        "participantCount": 0,
        "videoCount": 0,
        "attendeePW": "pw_a",
        "moderatorPW": "pw_m",
        "isBreakout": "true",
        "metadata": {"roomsmeetingid": str(uuid.UUID(int=number)), "creator": "creator"},
    }


def test_meeting_stats_from_bbb_ok():
    meeting_stats = MeetingStats.from_bbb(create_meeting_stats_bbb_payload(0))
    assert uuid.UUID(int=0) == meeting_stats.meeting_id
    assert "internal_meeting_id" == meeting_stats.bbb_meeting_id
    assert uuid.UUID(int=0) == meeting_stats.rooms_meeting_id
    assert "name" == meeting_stats.name
    assert 0 == meeting_stats.participant_count
    assert 0 == meeting_stats.video_count
    assert "pw_a" == meeting_stats.attendee_pw
    assert "pw_m" == meeting_stats.moderator_pw
    assert meeting_stats.is_breakout is True
    assert "creator" == meeting_stats.creator


def test_meeting_stats_from_bbb_uuid_exception():
    with pytest.raises(ValueError):
        payload = create_meeting_stats_bbb_payload(0)
        payload["meetingID"] = "1234asdf"
        MeetingStats.from_bbb(payload)

    with pytest.raises(ValueError):
        payload = create_meeting_stats_bbb_payload(0)
        payload["metadata"]["roomsmeetingid"] = "1234asdf"
        MeetingStats.from_bbb(payload)


@pytest.mark.parametrize("is_breakout_value", ("", "false", "False", "True", "1", "0", "asdf"))
def test_meeting_stats_from_bbb_is_breakout_false(is_breakout_value):
    payload = create_meeting_stats_bbb_payload(0)
    payload["isBreakout"] = is_breakout_value
    stats = MeetingStats.from_bbb(payload)
    assert stats.is_breakout is False


@pytest.fixture
def bbb_server():
    return Server.objects.create(
        dns="bbb.com",
        state=SERVER_STATE_DOWN,
        shared_secret="secret",
        last_health_check=datetime(2022, 12, 11, 10, 9, tzinfo=timezone.utc),
    )


@pytest.mark.django_db
def test_collect_server_stats_exception_in_bbb(bbb_server, current_timestamp):
    class MockBBB(BigBlueButton):
        @staticmethod
        def validate_get_meetings(response):
            raise Exception("mock error")

        def get_meetings(self):
            return None

    def create_bbb(dns, secret):
        return MockBBB(dns, secret)

    with freeze_time(current_timestamp):
        collect_server_stats(create_bbb)

    actual_server = Server.objects.get(dns=bbb_server.dns)

    assert actual_server.state == SERVER_STATE_ERROR
    assert actual_server.last_health_check == datetime(2022, 12, 11, 10, 9, tzinfo=timezone.utc)


def bbb_meeting(number: int, overrides=None):
    meeting = {
        "meetingID": str(uuid.UUID(int=number)),
        "internalMeetingID": "1234abcd",
        "name": "name",
        "participantCount": number,
        "videoCount": number,
        "attendeePW": "Apassword",
        "moderatorPW": "Mpassword",
        "metadata": {"roomsmeetingid": str(uuid.UUID(int=number)), "creator": "creator", "bbb-origin": "BBB@Scale"},
        "isBreakout": "false",
    }

    if overrides:
        for k, v in overrides.items():
            meeting[k] = v

    return meeting


@pytest.mark.django_db
@pytest.mark.parametrize(
    "meetings",
    (
        [{"metadata": {"bbb-origin": "BBB@Scale"}, "isBreakout": "false"}],
        [bbb_meeting(0, {"meetingID": "notUUID"})],
        [bbb_meeting(0)],
    ),
)
def test_collect_server_stats_parse_meetings(bbb_server, current_timestamp, meetings):
    class MockBBB(BigBlueButton):
        @staticmethod
        def validate_get_meetings(response):
            return meetings

        def get_meetings(self):
            return None

    def create_bbb(dns, secret):
        return MockBBB(dns, secret)

    with freeze_time(current_timestamp):
        collect_server_stats(create_bbb)

    actual_server = Server.objects.get(dns=bbb_server.dns)

    assert actual_server.state == SERVER_STATE_UP
    assert actual_server.last_health_check == current_timestamp

from types import SimpleNamespace

import core
import pytest
from core.models import ExternalMeeting, Meeting
from core.services import create_getrecordings_response_xml


@pytest.mark.django_db
def test_parse_recordings_xml_empty():
    _ex_meeting = ExternalMeeting.objects.create(external_meeting_id="6509c39d754c0d2d70fa025d347b94ecd976dce4-2-1")
    empty_xml_response = create_getrecordings_response_xml(_ex_meeting.meeting_set.all())
    assert empty_xml_response is None


@pytest.mark.django_db
def test_parse_recordings_xml_not_empty(mocker):
    def mock_meeting_recording_xml(replay_url):
        class Response:
            text = """
            <recording>
              <id>dfa4a5fa252036814f3c749fec01bb3325cb3d8d-1634057825061</id>
              <state>published</state>
              <published>true</published>
              <start_time>1634057825061</start_time>
              <end_time>1634059335155</end_time>
              <participants>3</participants>
              <meeting id="dfa4a5fa252036814f3c749fec01bb3325cb3d8d-1634057825061"
              externalId="00563b64075043708825c10d67859721" name="Example Meeting" breakout="false"/>
              <meta>
                <creator>ExampleUser</creator>
                <meetingId>00563b64075043708825c10d67859721</meetingId>
                <roomsmeetingid>92654</roomsmeetingid>
                <streamingurl>None</streamingurl>
              </meta>
              <playback>
                <format>presentation</format>
                <link>https://example.org/playback/presentation/2.3/dfa4a5fa252036814f3c749fec01bb3325cb3d8d-163
                4057825061</link>
                <processing_time>8576</processing_time>
                <duration>22796</duration>
                <extensions>
                  <preview>
                    <images>
                      <image width="176" height="136" alt="Test">
                      https://example.org/presentation/dfa4a5fa252036814f3c749fec01bb3325cb3d8d-1634057825061/
                      thumbnails/thumb-1.png
                      </image>
                      <image width="176" height="136" alt="Test">
                      https://example.org/presentation/dfa4a5fa252036814f3c749fec01bb3325cb3d8d-1634057825061/
                      thumbnails/thumb-2.png
                      </image>
                      <image width="176" height="136" alt="Test">
                      https://example.org/presentation/dfa4a5fa252036814f3c749fec01bb3325cb3d8d-1634057825061/
                      thumbnails/thumb-3.png
                      </image>
                    </images>
                  </preview>
                </extensions>
                <size>1474295</size>
              </playback>
              <raw_size>2912966</raw_size>
            </recording>
            """

        return Response()

    mocker.patch("core.models.meeting_recording_xml", mock_meeting_recording_xml)
    _ex_meeting = ExternalMeeting.objects.create(external_meeting_id="6509c39d754c0d2d70fa025d347b94ecd976dce4-2-1")
    Meeting.objects.create(
        room_name="Example Recording",
        replay_url="example.org",
        replay_id="dfa4a5fa252036814f3c749fec01bb3325cb3d8d-1634057825061",
        external_meeting=_ex_meeting,
    )
    Meeting.objects.create(room_name="Example Recording", external_meeting=_ex_meeting)

    response_xml = create_getrecordings_response_xml(_ex_meeting.meeting_set.all())
    assert len(response_xml["response"]["recordings"]) == 1
    recording = response_xml["response"]["recordings"]["recording"][0]
    assert "recordID" in recording.keys()
    assert recording["recordID"] == "dfa4a5fa252036814f3c749fec01bb3325cb3d8d-1634057825061"

    assert "meetingID" in recording.keys()
    assert recording["meetingID"] == "6509c39d754c0d2d70fa025d347b94ecd976dce4-2-1"

    assert "internalMeetingID" in recording.keys()
    assert recording["internalMeetingID"] == "dfa4a5fa252036814f3c749fec01bb3325cb3d8d-1634057825061"

    assert "name" in recording.keys()
    assert recording["name"] == "Example Meeting"

    assert "isBreakout" in recording.keys()
    assert recording["isBreakout"] == "false"

    assert "published" in recording.keys()
    assert recording["published"] == "true"

    assert "state" in recording.keys()
    assert recording["state"] == "published"

    assert "startTime" in recording.keys()
    assert recording["startTime"] == "1634057825061"

    assert "endTime" in recording.keys()
    assert recording["endTime"] == "1634059335155"

    assert "participants" in recording.keys()
    assert recording["participants"] == "3"

    assert "metadata" in recording.keys()
    metadata = recording["metadata"]

    assert "creator" in metadata.keys()
    assert metadata["creator"] == "ExampleUser"

    assert "streamingurl" in metadata.keys()
    assert metadata["streamingurl"] == "None"

    assert "meetingId" in metadata.keys()
    assert metadata["meetingId"] == "00563b64075043708825c10d67859721"

    assert "roomsmeetingid" in metadata.keys()
    assert metadata["roomsmeetingid"] == "92654"

    assert "playback" in recording.keys()
    playback = recording["playback"]
    assert "format" in playback.keys()
    _format = playback["format"]
    assert "type" in _format.keys()
    assert "url" in _format.keys()
    assert "processingTime" in _format.keys()
    assert "length" in _format.keys()
    assert "size" in _format.keys()
    assert "preview" in _format.keys()
    preview = _format["preview"]
    assert "images" in preview.keys()
    assert len(preview["images"]) == 1


@pytest.mark.django_db
def test_parse_recordings_xml_without_preview(mocker):
    mocker.patch("core.models.meeting_recording_xml")

    _ex_meeting = ExternalMeeting.objects.create(external_meeting_id="6509c39d754c0d2d70fa025d347b94ecd976dce4-2-1")
    Meeting.objects.create(
        room_name="Example Recording",
        replay_url="example.org",
        replay_id="dfa4a5fa252036814f3c749fec01bb3325cb3d8d-1634057825061",
        external_meeting=_ex_meeting,
    )
    Meeting.objects.create(room_name="Example Recording", external_meeting=_ex_meeting)

    # Return recording without .playback.extensions
    core.models.meeting_recording_xml.return_value = SimpleNamespace(
        text="""
            <recording>
              <id>dfa4a5fa252036814f3c749fec01bb3325cb3d8d-1634057825061</id>
              <state>published</state>
              <published>true</published>
              <start_time>1634057825061</start_time>
              <end_time>1634059335155</end_time>
              <participants>3</participants>
              <meeting id="dfa4a5fa252036814f3c749fec01bb3325cb3d8d-1634057825061"
              externalId="00563b64075043708825c10d67859721" name="Example Meeting" breakout="false"/>
              <meta>
                <creator>ExampleUser</creator>
                <meetingId>00563b64075043708825c10d67859721</meetingId>
                <roomsmeetingid>92654</roomsmeetingid>
                <streamingurl>None</streamingurl>
              </meta>
              <playback>
                <format>presentation</format>
                <link>https://example.org/playback/presentation/2.3/dfa4a5fa252036814f3c749fec01bb3325cb3d8d-163
                4057825061</link>
                <processing_time>8576</processing_time>
                <duration>22796</duration>
                <size>1474295</size>
              </playback>
              <raw_size>2912966</raw_size>
            </recording>
            """
    )

    response_xml = create_getrecordings_response_xml(_ex_meeting.meeting_set.all())
    assert len(response_xml["response"]["recordings"]) == 1
    recording = response_xml["response"]["recordings"]["recording"][0]
    assert "preview" not in recording["playback"]["format"]

    # Return recording without .playback.extensions.preview
    core.models.meeting_recording_xml.return_value = SimpleNamespace(
        text="""
            <recording>
              <id>dfa4a5fa252036814f3c749fec01bb3325cb3d8d-1634057825061</id>
              <state>published</state>
              <published>true</published>
              <start_time>1634057825061</start_time>
              <end_time>1634059335155</end_time>
              <participants>3</participants>
              <meeting id="dfa4a5fa252036814f3c749fec01bb3325cb3d8d-1634057825061"
              externalId="00563b64075043708825c10d67859721" name="Example Meeting" breakout="false"/>
              <meta>
                <creator>ExampleUser</creator>
                <meetingId>00563b64075043708825c10d67859721</meetingId>
                <roomsmeetingid>92654</roomsmeetingid>
                <streamingurl>None</streamingurl>
              </meta>
              <playback>
                <format>presentation</format>
                <link>https://example.org/playback/presentation/2.3/dfa4a5fa252036814f3c749fec01bb3325cb3d8d-163
                4057825061</link>
                <processing_time>8576</processing_time>
                <duration>22796</duration>
                <size>1474295</size>
                <extensions>
                </extensions>
              </playback>
              <raw_size>2912966</raw_size>
            </recording>
            """
    )

    response_xml = create_getrecordings_response_xml(_ex_meeting.meeting_set.all())
    assert len(response_xml["response"]["recordings"]) == 1
    recording = response_xml["response"]["recordings"]["recording"][0]
    assert "preview" not in recording["playback"]["format"]

    # Return recording without .playback.extensions.preview.images
    core.models.meeting_recording_xml.return_value = SimpleNamespace(
        text="""
            <recording>
              <id>dfa4a5fa252036814f3c749fec01bb3325cb3d8d-1634057825061</id>
              <state>published</state>
              <published>true</published>
              <start_time>1634057825061</start_time>
              <end_time>1634059335155</end_time>
              <participants>3</participants>
              <meeting id="dfa4a5fa252036814f3c749fec01bb3325cb3d8d-1634057825061"
              externalId="00563b64075043708825c10d67859721" name="Example Meeting" breakout="false"/>
              <meta>
                <creator>ExampleUser</creator>
                <meetingId>00563b64075043708825c10d67859721</meetingId>
                <roomsmeetingid>92654</roomsmeetingid>
                <streamingurl>None</streamingurl>
              </meta>
              <playback>
                <format>presentation</format>
                <link>https://example.org/playback/presentation/2.3/dfa4a5fa252036814f3c749fec01bb3325cb3d8d-163
                4057825061</link>
                <processing_time>8576</processing_time>
                <duration>22796</duration>
                <size>1474295</size>
                <extensions>
                  <preview>
                  </preview>
                </extensions>
              </playback>
              <raw_size>2912966</raw_size>
            </recording>
            """
    )

    response_xml = create_getrecordings_response_xml(_ex_meeting.meeting_set.all())
    assert len(response_xml["response"]["recordings"]) == 1
    recording = response_xml["response"]["recordings"]["recording"][0]
    assert "preview" not in recording["playback"]["format"]

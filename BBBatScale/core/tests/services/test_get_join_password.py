import pytest
from core.constants import MODERATION_MODE_ALL, MODERATION_MODE_MODERATORS, MODERATION_MODE_STARTER
from core.models import Meeting, MeetingConfiguration, Room, SchedulingStrategy, User, get_default_room_config
from core.services import get_join_password
from django.conf import settings
from django.contrib.auth.models import Group


@pytest.fixture(scope="function")
def moderator_group(db) -> Group:
    return Group.objects.create(name=settings.MODERATORS_GROUP)


@pytest.fixture(scope="function")
def example(db) -> SchedulingStrategy:
    return SchedulingStrategy.objects.create(
        name="example",
    )


@pytest.fixture(scope="function")
def user_bbb_mod(db, moderator_group, testserver2_tenant) -> User:
    user = User.objects.create_user(
        username="bbb_mod", email="bbb_mod@example.org", password="bbb_mod", tenant=testserver2_tenant
    )
    user.groups.add(moderator_group)
    return user


@pytest.fixture(scope="function")
def user_bbb_mod_tenant(db, moderator_group, testserver_tenant) -> User:
    user = User.objects.create_user(
        username="bbb_mod_tenant", email="bbb_mod_tenant@example.org", password="bbb_mod", tenant=testserver_tenant
    )
    user.groups.add(moderator_group)
    return user


@pytest.fixture(scope="function")
def user_no_bbb_mod(db, testserver2_tenant) -> User:
    user = User.objects.create_user(
        username="no_bbb_mod", email="no_bbb_mod@example.org", password="no_bbb_mod", tenant=testserver2_tenant
    )
    return user


@pytest.fixture(scope="function")
def user_superuser(db, testserver2_tenant) -> User:
    user = User.objects.create(
        username="user_superuser", email="user_superuser@example.org", is_superuser=True, tenant=testserver2_tenant
    )
    return user


@pytest.fixture(scope="function")
def user_staff(db, testserver2_tenant) -> User:
    user = User.objects.create(
        username="user_staff",
        email="user_staff@example.org",
        is_superuser=False,
        is_staff=True,
        tenant=testserver2_tenant,
    )
    return user


@pytest.fixture(scope="function")
def user_staff_tenant(db, testserver_tenant) -> User:
    user = User.objects.create(
        username="user_staff_tenant",
        email="user_staff_tenant@example.org",
        is_superuser=False,
        is_staff=True,
        tenant=testserver_tenant,
    )
    return user


@pytest.fixture(scope="function")
def room(db, example) -> Room:
    return Room.objects.create(
        scheduling_strategy=example,
        name="room_every_one_can_start",
        default_meeting_configuration=get_default_room_config(),
    )


@pytest.fixture(scope="function")
def room_config_room(db, example) -> MeetingConfiguration:
    return MeetingConfiguration.objects.create(
        name="room_every_one_can_start_config", moderation_mode=MODERATION_MODE_MODERATORS, everyone_can_start=True
    )


@pytest.fixture(scope="function")
def meeting_room(db, room, room_config_room) -> Meeting:
    return Meeting.objects.create(
        room=room,
        room_name="room_every_one_can_start",
        attendee_pw="test_attendee_password",
        moderator_pw="test_moderator_password",
        configuration=room_config_room,
    )


@pytest.fixture(scope="function")
def room_all_are_mods(db, example) -> Room:
    return Room.objects.create(
        scheduling_strategy=example,
        name="room_all_mods",
        default_meeting_configuration=get_default_room_config(),
    )


@pytest.fixture(scope="function")
def room_config_room_all_are_mods(db, example) -> MeetingConfiguration:
    return MeetingConfiguration.objects.create(
        name="room_config_room_all_are_mods", moderation_mode=MODERATION_MODE_ALL, everyone_can_start=True
    )


@pytest.fixture(scope="function")
def meeting_room_all_are_mods(db, room_all_are_mods, room_config_room_all_are_mods) -> Meeting:
    return Meeting.objects.create(
        room=room_all_are_mods,
        room_name="room_all_mods",
        attendee_pw="test_attendee_password",
        moderator_pw="test_moderator_password",
        configuration=room_config_room_all_are_mods,
    )


@pytest.fixture(scope="function")
def room_ask_mod(db, example) -> Room:
    return Room.objects.create(
        scheduling_strategy=example,
        name="room_ask_mod",
        default_meeting_configuration=get_default_room_config(),
    )


@pytest.fixture(scope="function")
def room_config_room_ask_mod(db, example) -> MeetingConfiguration:
    return MeetingConfiguration.objects.create(
        name="room_config_room_ask_mod",
        moderation_mode=MODERATION_MODE_MODERATORS,
        everyone_can_start=True,
        guest_policy="ASK_MODERATOR",
    )


@pytest.fixture(scope="function")
def meeting_room_ask_mod(db, room_ask_mod, room_config_room_ask_mod) -> Meeting:
    return Meeting.objects.create(
        room=room_ask_mod,
        room_name="room_ask_mod",
        attendee_pw="test_attendee_password",
        moderator_pw="test_moderator_password",
        configuration=room_config_room_ask_mod,
    )


@pytest.fixture(scope="function")
def room_starter_is_mod(db, example) -> Room:
    return Room.objects.create(
        scheduling_strategy=example,
        name="room_starter_is_mod",
        default_meeting_configuration=get_default_room_config(),
    )


@pytest.fixture(scope="function")
def room_config_room_starter_is_mod(db, example) -> MeetingConfiguration:
    return MeetingConfiguration.objects.create(
        name="room_config_room_starter_is_mod",
        moderation_mode=MODERATION_MODE_STARTER,
        everyone_can_start=True,
    )


@pytest.fixture(scope="function")
def meeting_room_starter_is_mod(db, room_starter_is_mod, room_config_room_starter_is_mod) -> Meeting:
    return Meeting.objects.create(
        room=room_starter_is_mod,
        room_name="room_ask_mod",
        attendee_pw="test_attendee_password",
        moderator_pw="test_moderator_password",
        configuration=room_config_room_starter_is_mod,
    )


@pytest.mark.django_db
def test_get_join_password(
    user_bbb_mod,
    user_no_bbb_mod,
    user_superuser,
    user_staff,
    meeting_room,
    meeting_room_all_are_mods,
    meeting_room_ask_mod,
    meeting_room_starter_is_mod,
    testserver_tenant,
    user_bbb_mod_tenant,
    user_staff_tenant,
):
    meeting_room.creator = user_bbb_mod.username
    meeting_room.tenants.add(testserver_tenant)
    meeting_room.save()
    # Test case User is not in tenant of Room
    assert get_join_password(user_bbb_mod, meeting_room, user_bbb_mod.__str__()) == "test_attendee_password"
    assert get_join_password(user_no_bbb_mod, meeting_room, user_no_bbb_mod.__str__()) == "test_attendee_password"
    assert get_join_password(user_superuser, meeting_room, user_superuser.__str__()) == "test_moderator_password"
    assert get_join_password(user_staff, meeting_room, user_staff.__str__()) == "test_attendee_password"

    # Test case User is in tenant of Room
    assert get_join_password(user_bbb_mod_tenant, meeting_room, user_bbb_mod.__str__()) == "test_moderator_password"
    assert get_join_password(user_no_bbb_mod, meeting_room, user_no_bbb_mod.__str__()) == "test_attendee_password"
    assert get_join_password(user_superuser, meeting_room, user_superuser.__str__()) == "test_moderator_password"
    assert get_join_password(user_staff_tenant, meeting_room, user_staff.__str__()) == "test_moderator_password"

    meeting_room_all_are_mods.creator = user_bbb_mod.username
    meeting_room_all_are_mods.save()

    # Test case all user are moderators room setting
    assert (
        get_join_password(user_bbb_mod, meeting_room_all_are_mods, user_bbb_mod.__str__()) == "test_moderator_password"
    )
    assert (
        get_join_password(user_no_bbb_mod, meeting_room_all_are_mods, user_no_bbb_mod.__str__())
        == "test_moderator_password"
    )
    assert (
        get_join_password(user_superuser, meeting_room_all_are_mods, user_superuser.__str__())
        == "test_moderator_password"
    )
    assert get_join_password(user_staff, meeting_room_all_are_mods, user_staff.__str__()) == "test_moderator_password"

    meeting_room_ask_mod.creator = user_bbb_mod.username
    meeting_room_ask_mod.save()

    # Test case ask moderator for entry
    assert get_join_password(user_bbb_mod, meeting_room_ask_mod, user_bbb_mod.username) == "test_attendee_password"
    assert (
        get_join_password(user_no_bbb_mod, meeting_room_ask_mod, user_no_bbb_mod.username) == "test_attendee_password"
    )
    assert get_join_password(user_superuser, meeting_room_ask_mod, user_superuser.username) == "test_moderator_password"
    assert get_join_password(user_staff, meeting_room_ask_mod, user_staff.username) == "test_attendee_password"

    meeting_room_starter_is_mod.creator = user_no_bbb_mod.username
    meeting_room_starter_is_mod.save()

    assert (
        get_join_password(user_bbb_mod, meeting_room_starter_is_mod, user_bbb_mod.__str__()) == "test_attendee_password"
    )
    assert (
        get_join_password(user_no_bbb_mod, meeting_room_starter_is_mod, user_no_bbb_mod.username)
        == "test_moderator_password"
    )
    assert (
        get_join_password(user_superuser, meeting_room_starter_is_mod, user_superuser.__str__())
        == "test_moderator_password"
    )
    assert get_join_password(user_staff, meeting_room_starter_is_mod, user_staff.__str__()) == "test_attendee_password"

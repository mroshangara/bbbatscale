import pytest
from core.filters import GroupFilter
from django.contrib.auth.models import Group
from django.http import QueryDict


@pytest.mark.django_db
def test_group_filter():
    Group.objects.create(name="Example")
    Group.objects.create(name="exam")
    Group.objects.create(name="Group")
    get_params = QueryDict("", mutable=True)
    get_params.update({"name": "Example"})
    f = GroupFilter(get_params, Group.objects.all())
    assert sorted(f.qs.values_list("name", flat=True)) == ["Example"]
    get_params.pop("name")
    get_params.update({"name": "ExAm"})
    f = GroupFilter(get_params, Group.objects.all())
    assert sorted(f.qs.values_list("name", flat=True)) == ["Example", "exam"]

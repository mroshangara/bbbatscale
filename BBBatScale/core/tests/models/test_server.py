import pytest
from core.models import Server
from django.db import transaction


@pytest.mark.django_db
@pytest.mark.parametrize("dns_variation", ["asdf.com", "ASDF.COM", "AsDf.CoM", "asdf.com.", "asdf.com.."])
def test_server_dns_field(dns_variation):
    with transaction.atomic():
        created = Server.objects.create(dns=dns_variation)

    with transaction.atomic():
        by_valid_dns = Server.objects.get(dns="asdf.com")

    with transaction.atomic():
        by_dns_variation = Server.objects.get(dns=dns_variation)

    with transaction.atomic():
        by_raw = Server.objects.raw("SELECT * FROM public.core_server WHERE dns='asdf.com'")[0]

    assert created == by_valid_dns == by_dns_variation == by_raw

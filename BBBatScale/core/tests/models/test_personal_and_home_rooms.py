import pytest
from core.constants import SCHEDULING_STRATEGY_LEAST_PARTICIPANTS, SCHEDULING_STRATEGY_LEAST_UTILIZATION
from core.models import HomeRoom, MeetingConfigurationTemplate, SchedulingStrategy, User


@pytest.mark.django_db
def test_home_room_create_with_email_name(gp_test_tenant, example_moderator_user):
    assert "home-example-staff@example.org" == example_moderator_user.homeroom.name


@pytest.mark.django_db
def test_home_room_create_with_email_not_set(example_staff_user, gp_test_tenant, moderator_group):

    example_staff_user.email = ""
    example_staff_user.first_name = "Jane J."
    example_staff_user.last_name = "Doe"
    example_staff_user.groups.add(moderator_group)
    example_staff_user.save()
    assert "home-Doe-" in example_staff_user.homeroom.name


@pytest.mark.django_db
def test_home_room_create_two_users_same_email(
    example_moderator_user, gp_test_tenant, moderator_group, testserver_tenant
):

    assert "home-example-staff@example.org" == example_moderator_user.homeroom.name

    example_staff_user_2 = User.objects.create_user(
        "example-staff-2",
        "example-staff@example.org",
        "EXAMPLEPASSWORD",
        tenant=testserver_tenant,
        is_staff=True,
        first_name="Jane J.",
        last_name="Doe",
    )
    example_staff_user_2.groups.add(moderator_group)
    example_staff_user_2.save()

    assert "home-Doe-" in example_staff_user_2.homeroom.name


@pytest.mark.django_db
def test_home_room_created_for_teacher_only(example_moderator_user, example_user, gp_test_tenant, moderator_group):

    assert HomeRoom.objects.count() == 0
    assert example_moderator_user.homeroom is not None
    assert example_user.homeroom is None
    assert HomeRoom.objects.count() == 1


@pytest.mark.django_db
def test_home_room_created_for_all_users(example_moderator_user, example_user, gp_test_tenant):

    gp_test_tenant.home_room_teachers_only = False
    gp_test_tenant.save()

    assert HomeRoom.objects.count() == 0
    assert example_moderator_user.homeroom is not None
    assert example_user.homeroom is not None
    assert HomeRoom.objects.count() == 2


@pytest.mark.django_db
def test_home_room_delete_on_user_delete(example_moderator_user, example_user, gp_test_tenant):

    assert example_moderator_user.homeroom is not None
    assert example_user.homeroom is None

    example_moderator_user.delete()
    example_user.delete()

    assert HomeRoom.objects.count() == 0


@pytest.mark.django_db
def test_home_room_can_not_be_accessed(example_moderator_user, example_user, gp_test_tenant):

    assert example_moderator_user.homeroom is not None
    assert example_user.homeroom is None
    assert HomeRoom.objects.count() == 1

    gp_test_tenant.home_room_enabled = False
    gp_test_tenant.save()

    assert example_moderator_user.homeroom is None
    assert example_user.homeroom is None
    assert HomeRoom.objects.count() == 1


@pytest.mark.django_db
def test_home_room_can_be_created_by_super_user(example_superuser, example_user, gp_test_tenant):
    assert example_user.homeroom is None
    assert example_superuser.homeroom is not None
    assert HomeRoom.objects.count() == 1


@pytest.mark.django_db
def test_change_home_room_scheduling_strategy(
    example_moderator_user, example_user, gp_test_tenant, example_scheduling_strategy
):

    assert example_moderator_user.homeroom.scheduling_strategy.pk == example_scheduling_strategy.pk

    scheduling_strategy_2 = SchedulingStrategy.objects.create(name="New Tenant")

    gp_test_tenant.home_room_teachers_only = False
    gp_test_tenant.home_room_scheduling_strategy = scheduling_strategy_2
    gp_test_tenant.save()

    # preserve already set scheduling_strategies
    assert example_moderator_user.homeroom.scheduling_strategy.pk == example_scheduling_strategy.pk
    assert example_user.homeroom.scheduling_strategy.pk == scheduling_strategy_2.pk


@pytest.mark.django_db
def test_unset_home_room_scheduling_strategy(
    example_moderator_user, example_user, gp_test_tenant, example_scheduling_strategy
):

    assert example_moderator_user.homeroom.scheduling_strategy.pk == example_scheduling_strategy.pk

    gp_test_tenant.home_room_teachers_only = False
    gp_test_tenant.home_room_scheduling_strategy = None
    gp_test_tenant.save()

    # the student cannot create new homeroom although home_room_teachers_only is false
    assert example_moderator_user.homeroom.scheduling_strategy.pk == example_scheduling_strategy.pk
    assert example_user.homeroom is None


@pytest.mark.django_db
def test_change_home_room_room_configuration(
    example_moderator_user, example_user, gp_test_tenant, example_meeting_configuration_template
):

    assert example_moderator_user.homeroom.default_meeting_configuration.pk == example_meeting_configuration_template.pk

    room_config_2 = MeetingConfigurationTemplate.objects.create(name="config-2")

    gp_test_tenant.home_room_teachers_only = False
    gp_test_tenant.home_room_room_configuration = room_config_2
    gp_test_tenant.save()

    assert example_moderator_user.homeroom.default_meeting_configuration.pk == example_meeting_configuration_template.pk
    assert example_user.homeroom.default_meeting_configuration.pk == room_config_2.pk


@pytest.mark.django_db
def test_unset_home_room_room_configuration(
    example_moderator_user, example_user, gp_test_tenant, example_meeting_configuration_template
):

    assert example_moderator_user.homeroom.default_meeting_configuration.pk == example_meeting_configuration_template.pk

    gp_test_tenant.home_room_teachers_only = False
    gp_test_tenant.home_room_room_configuration = None
    gp_test_tenant.save()

    # the student cannot create new homeroom although home_room_teachers_only is false
    assert example_moderator_user.homeroom.default_meeting_configuration.pk == example_meeting_configuration_template.pk
    assert example_user.homeroom is None


@pytest.mark.django_db
def test_personal_room_rooms_max_number(
    example_staff_user,
    example_user,
    gp_test_tenant,
    testserver_tenant,
    moderator_group,
):
    gp_test_tenant.personal_rooms_teacher_max_number = 8
    gp_test_tenant.personal_rooms_non_teacher_max_number = 0
    gp_test_tenant.save()

    example_staff_user.groups.add(moderator_group)
    example_staff_user.save()

    assert example_staff_user.get_max_number_of_personal_rooms() == 8
    assert example_user.get_max_number_of_personal_rooms() == 0

    example_staff_user_2 = User.objects.create_user(
        "example-staff-2",
        "example-staff-2@example.org",
        "EXAMPLEPASSWORD",
        tenant=testserver_tenant,
        is_staff=True,
        personal_rooms_max_number=5,
    )

    example_user_2 = User.objects.create_user(
        "example-user-2",
        "example-user-2@example.org",
        "EXAMPLEPASSWORD",
        tenant=testserver_tenant,
        is_staff=False,
        personal_rooms_max_number=3,
    )
    assert example_staff_user_2.get_max_number_of_personal_rooms() == 5
    assert example_user_2.get_max_number_of_personal_rooms() == 3


@pytest.mark.django_db
def test_owner_shall_not_be_co_owner(example_staff_user, example_user, gp_test_tenant, example_personal_room):
    example_personal_room.owner = example_staff_user

    example_personal_room.co_owners.add(example_staff_user)
    example_personal_room.co_owners.add(example_user)
    example_personal_room.save()

    assert len(example_personal_room.co_owners.all()) == 1
    assert example_personal_room.has_co_owner(example_staff_user) is False
    assert example_personal_room.has_co_owner(example_user) is True


@pytest.mark.django_db
def test_personal_room_scheduling_strategy(example_personal_room, gp_test_tenant, example_scheduling_strategy):
    gp_test_tenant.personal_rooms_scheduling_strategy = example_scheduling_strategy

    assert example_personal_room.scheduling_strategy.name == "example"
    assert example_personal_room.scheduling_strategy.scheduling_strategy == SCHEDULING_STRATEGY_LEAST_UTILIZATION

    gp_test_tenant.personal_rooms_scheduling_strategy.scheduling_strategy = SCHEDULING_STRATEGY_LEAST_PARTICIPANTS
    gp_test_tenant.save()

    example_personal_room.save()

    assert example_personal_room.scheduling_strategy.scheduling_strategy == SCHEDULING_STRATEGY_LEAST_PARTICIPANTS

from core.models import User
from django.contrib.auth.models import Permission
from django.contrib.sites.models import Site


def test_user_has_perm(db, testserver_tenant: Site, testserver2_tenant: Site) -> None:
    user = User.objects.create(username="example-user", tenant=testserver_tenant, is_staff=False, is_superuser=False)

    assert user.has_tenant_based_perms(["core.view_room"], testserver_tenant) is False
    assert user.has_tenant_based_perms(["core.view_room"], testserver2_tenant) is False
    assert user.has_tenant_based_perms(["core.view_room"], testserver_tenant, elevate_staff=False) is False
    assert user.has_tenant_based_perms(["core.view_room"], testserver2_tenant, elevate_staff=False) is False

    user.user_permissions.add(Permission.objects.get_by_natural_key("view_room", "core", "room"))

    user = User.objects.get(pk=user.pk)  # re-fetch to clear the permissions cache

    assert user.has_tenant_based_perms(["core.view_room"], testserver_tenant) is True
    assert user.has_tenant_based_perms(["core.view_room"], testserver2_tenant) is False
    assert user.has_tenant_based_perms(["core.view_room"], testserver_tenant, elevate_staff=False) is True
    assert user.has_tenant_based_perms(["core.view_room"], testserver2_tenant, elevate_staff=False) is False


def test_staff_user_has_perm(db, testserver_tenant: Site, testserver2_tenant: Site) -> None:
    user = User.objects.create(username="example-user", tenant=testserver_tenant, is_staff=True, is_superuser=False)

    assert user.has_tenant_based_perms(["core.view_room"], testserver_tenant) is True
    assert user.has_tenant_based_perms(["core.view_room"], testserver2_tenant) is False
    assert user.has_tenant_based_perms(["core.view_room"], testserver_tenant, elevate_staff=False) is False
    assert user.has_tenant_based_perms(["core.view_room"], testserver2_tenant, elevate_staff=False) is False

    user.user_permissions.add(Permission.objects.get_by_natural_key("view_room", "core", "room"))

    user = User.objects.get(pk=user.pk)  # re-fetch to clear the permissions cache

    assert user.has_tenant_based_perms(["core.view_room"], testserver_tenant) is True
    assert user.has_tenant_based_perms(["core.view_room"], testserver2_tenant) is False
    assert user.has_tenant_based_perms(["core.view_room"], testserver_tenant, elevate_staff=False) is True
    assert user.has_tenant_based_perms(["core.view_room"], testserver2_tenant, elevate_staff=False) is False


def test_superuser_has_perm(db, testserver_tenant: Site, testserver2_tenant: Site) -> None:
    user = User.objects.create(username="example-user", tenant=testserver_tenant, is_staff=False, is_superuser=True)

    assert user.has_tenant_based_perms(["core.view_room"], testserver_tenant) is True
    assert user.has_tenant_based_perms(["core.view_room"], testserver2_tenant) is True
    assert user.has_tenant_based_perms(["core.view_room"], testserver_tenant, elevate_staff=False) is True
    assert user.has_tenant_based_perms(["core.view_room"], testserver2_tenant, elevate_staff=False) is True

    user.user_permissions.add(Permission.objects.get_by_natural_key("view_room", "core", "room"))

    user = User.objects.get(pk=user.pk)  # re-fetch to clear the permissions cache

    assert user.has_tenant_based_perms(["core.view_room"], testserver_tenant) is True
    assert user.has_tenant_based_perms(["core.view_room"], testserver2_tenant) is True
    assert user.has_tenant_based_perms(["core.view_room"], testserver_tenant, elevate_staff=False) is True
    assert user.has_tenant_based_perms(["core.view_room"], testserver2_tenant, elevate_staff=False) is True


def test_staff_superuser_has_perm(db, testserver_tenant: Site, testserver2_tenant: Site) -> None:
    user = User.objects.create(username="example-user", tenant=testserver_tenant, is_staff=True, is_superuser=True)

    assert user.has_tenant_based_perms(["core.view_room"], testserver_tenant) is True
    assert user.has_tenant_based_perms(["core.view_room"], testserver2_tenant) is True
    assert user.has_tenant_based_perms(["core.view_room"], testserver_tenant, elevate_staff=False) is True
    assert user.has_tenant_based_perms(["core.view_room"], testserver2_tenant, elevate_staff=False) is True

    user.user_permissions.add(Permission.objects.get_by_natural_key("view_room", "core", "room"))

    user = User.objects.get(pk=user.pk)  # re-fetch to clear the permissions cache

    assert user.has_tenant_based_perms(["core.view_room"], testserver_tenant) is True
    assert user.has_tenant_based_perms(["core.view_room"], testserver2_tenant) is True
    assert user.has_tenant_based_perms(["core.view_room"], testserver_tenant, elevate_staff=False) is True
    assert user.has_tenant_based_perms(["core.view_room"], testserver2_tenant, elevate_staff=False) is True

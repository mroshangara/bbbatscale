import datetime

import pytest
from core.constants import MEETING_STATE_RUNNING
from core.models import Meeting


@pytest.mark.django_db
def test_room_get_running_meeting_single(example_room, example_meeting):
    example_meeting.room = example_room
    running_meeting = example_room.get_running_meeting()
    assert str(running_meeting.pk) == str(example_meeting.pk)


@pytest.mark.django_db
def test_room_get_running_meeting_multiple(example_room, example_meeting, current_timestamp):
    example_meeting.started = current_timestamp - datetime.timedelta(seconds=1)
    example_meeting.room = example_room
    example_meeting.save()

    example_meeting2 = Meeting.objects.create()
    example_meeting2.room = example_room
    example_meeting2.started = current_timestamp
    example_meeting2.state = MEETING_STATE_RUNNING
    example_meeting2.save()

    running_meeting = example_room.get_running_meeting()
    assert running_meeting.pk == example_meeting2.pk


@pytest.mark.django_db
def test_room_get_running_meeting_no_meeting(example_room):
    running_meeting = example_room.get_running_meeting()
    assert running_meeting is None

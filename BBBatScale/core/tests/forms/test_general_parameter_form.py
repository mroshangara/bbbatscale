import pytest
from core.forms import GeneralParametersForm
from core.utils import get_permissions
from django.contrib.auth.models import Group


@pytest.mark.parametrize(
    "data, is_valid",
    [
        ({"app_title": "Virtual Rooms", "recording_deletion_period": 0}, True),
        ({"app_title": "Virtual Rooms", "recording_deletion_period": -1}, True),
        ({"app_title": None, "recording_deletion_period": 0}, False),
        ({"app_title": "", "recording_deletion_period": 0}, False),
        ({"app_title": "Virtual Rooms", "recording_deletion_period": None}, False),
        ({"app_title": "Virtual Rooms", "recording_deletion_period": 2147483648}, False),
        ({"recording_deletion_period": 0}, False),
        ({"app_title": "Virtual Rooms"}, False),
        ({}, False),
    ],
)
def test_general_parameters_creation_form(
    data,
    is_valid,
    example_superuser,
    example_theme,
):
    form_data = {
        "default_theme": example_theme,
        "personal_rooms_teacher_max_number": 2,
        "personal_rooms_non_teacher_max_number": 2,
    }
    form_data.update(data)
    form = GeneralParametersForm(
        data=form_data,
        user=example_superuser,
    )

    assert form.is_valid() is is_valid


@pytest.mark.parametrize(
    "data, is_valid",
    [
        ({"app_title": "Virtual Rooms", "recording_deletion_period": 0}, True),
        ({"app_title": "Virtual Rooms", "recording_deletion_period": -1}, True),
        ({"app_title": None, "recording_deletion_period": 0}, False),
        ({"app_title": "", "recording_deletion_period": 0}, False),
        ({"app_title": "Virtual Rooms", "recording_deletion_period": None}, False),
        ({"app_title": "Virtual Rooms", "recording_deletion_period": 2147483648}, False),
        ({"app_title": "Virtual Rooms"}, False),
        ({"recording_deletion_period": 0}, False),
        ({}, False),
    ],
)
def test_general_parameters_update_form(
    data,
    is_valid,
    example_superuser,
    example_theme,
    gp_test_tenant,
):
    form_data = {
        "default_theme": example_theme,
        "personal_rooms_teacher_max_number": 2,
        "personal_rooms_non_teacher_max_number": 2,
    }
    form_data.update(data)
    form = GeneralParametersForm(
        data=form_data,
        user=example_superuser,
        instance=gp_test_tenant,
    )

    assert form.is_valid() is is_valid


def test_settings_media_disabled(example_user, settings):
    settings.BBBATSCALE_MEDIA_ENABLED = False
    form = GeneralParametersForm(user=example_user)
    assert "media_enabled" not in form.fields


def test_settings_media_enabled_gp_media_disabled(example_user, settings, gp_test_tenant):
    settings.BBBATSCALE_MEDIA_ENABLED = True
    gp_test_tenant.media_enabled = False
    form = GeneralParametersForm(user=example_user, instance=gp_test_tenant)
    assert "media_enabled" in form.fields
    assert "moderators_can_upload_slides" not in form.fields
    assert "slides_max_upload_size" not in form.fields


def test_settings_media_enabled_gp_media_enabled_user_no_perms_slides(example_user, settings, gp_test_tenant):
    settings.BBBATSCALE_MEDIA_ENABLED = True
    gp_test_tenant.media_enabled = True
    form = GeneralParametersForm(user=example_user, instance=gp_test_tenant)
    assert "media_enabled" in form.fields
    assert "moderators_can_upload_slides" in form.fields
    assert "slides_max_upload_size" in form.fields
    assert "slides" not in form.fields


def test_settings_media_enabled_gp_media_enabled_user_has_perms_slides(example_superuser, settings, gp_test_tenant):
    settings.BBBATSCALE_MEDIA_ENABLED = True
    gp_test_tenant.media_enabled = True
    form = GeneralParametersForm(user=example_superuser, instance=gp_test_tenant)
    assert "media_enabled" in form.fields
    assert "moderators_can_upload_slides" in form.fields
    assert "slides_max_upload_size" in form.fields
    assert "slides" in form.fields


@pytest.mark.django_db
def test__save_m2m_add_permissions(settings, gp_test_tenant, example_superuser):
    settings.MODERATORS_GROUP = "moderator"
    form = GeneralParametersForm(
        user=example_superuser,
        instance=gp_test_tenant,
        data={"media_enabled": True, "moderators_can_upload_slides": True},
    )
    form.is_valid()
    form._save_m2m()
    assert len(Group.objects.filter(name=settings.MODERATORS_GROUP)) == 1
    group = Group.objects.filter(name=settings.MODERATORS_GROUP)[0]
    permissions = group.permissions.filter(
        codename__in=["add_own_slides", "change_own_slides", "delete_own_slides", "view_own_slides"]
    )
    assert len(permissions) == 4


@pytest.mark.django_db
def test__save_m2m_remove_permissions(settings, gp_test_tenant, example_superuser):
    settings.MODERATORS_GROUP = "moderator"
    form = GeneralParametersForm(
        user=example_superuser,
        instance=gp_test_tenant,
        data={
            "media_enabled": True,
        },
    )
    gp_test_tenant.moderators_can_upload_slides = False
    group = Group.objects.create(name=settings.MODERATORS_GROUP)
    permission_pks = get_permissions(
        "core.add_own_slides", "core.change_own_slides", "core.delete_own_slides", "core.view_own_slides"
    ).values_list("pk", flat=True)
    group.permissions.add(*permission_pks)
    form.is_valid()
    form._save_m2m()
    permissions = group.permissions.filter(
        codename__in=["add_own_slides", "change_own_slides", "delete_own_slides", "view_own_slides"]
    )
    assert len(permissions) == 0

import pytest
from core.forms import RecordingForm
from core.models import Meeting


@pytest.mark.django_db
@pytest.mark.parametrize(
    "data, is_valid",
    [
        ({"replay_title": "Example"}, True),
        ({"replay_title": ""}, False),
        ({}, False),
        ({"replay_title": 1337}, True),
        ({"replay_title": None}, False),
    ],
)
def test_recording_update_form(
    data,
    is_valid,
):

    example_recording = Meeting.objects.create(
        room_name="Example Room",
        replay_id="example-replay-id",
        replay_url="https://example.org/my-awesome-recording",
        replay_title="Example recording title",
    )
    form = RecordingForm(
        data=data,
        instance=example_recording,
    )

    assert form.is_valid() is is_valid

import pytest
from core.constants import ROOM_VISIBILITY_PRIVATE, ROOM_VISIBILITY_PUBLIC
from core.forms import HomeRoomForm


@pytest.mark.django_db
@pytest.mark.parametrize(
    "data, is_valid",
    [
        ({"visibility": ROOM_VISIBILITY_PRIVATE}, True),
        ({"visibility": None}, False),
        ({}, False),
    ],
)
def test_home_room_update_form(
    data,
    is_valid,
    gp_test_tenant,
    example_superuser,
    example_scheduling_strategy,
    example_meeting_configuration_template,
    testserver_tenant,
):
    form_data = {
        "default_meeting_configuration": example_meeting_configuration_template,
        "name": "Example HomeRoom Name",
    }
    form_data.update(data)
    form = HomeRoomForm(
        data=form_data, general_parameter=gp_test_tenant, user=example_superuser, instance=example_superuser.homeroom
    )

    assert form.is_valid() is is_valid


@pytest.mark.django_db
def test_room_update_form_shared_resources(
    gp_test_tenant,
    example_superuser,
    example_meeting_configuration_template,
    testserver2_tenant,
    testserver_tenant,
):
    example_meeting_configuration_template.tenants.add(testserver2_tenant)
    form = HomeRoomForm(
        data={
            "name": "Example Strategy",
            "default_meeting_configuration": example_meeting_configuration_template,
            "visibility": ROOM_VISIBILITY_PUBLIC,
        },
        general_parameter=gp_test_tenant,
        user=example_superuser,
        instance=example_superuser.homeroom,
    )

    assert form.is_valid() is True
    assert form.fields["default_meeting_configuration"].queryset.count() == 1

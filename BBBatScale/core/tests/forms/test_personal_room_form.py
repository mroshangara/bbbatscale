import pytest
from core.constants import ROOM_VISIBILITY_PRIVATE, ROOM_VISIBILITY_PUBLIC
from core.forms import PersonalRoomForm


@pytest.mark.django_db
@pytest.mark.parametrize(
    "data, is_valid",
    [
        ({"visibility": ROOM_VISIBILITY_PRIVATE}, True),
        ({"visibility": None}, False),
        ({}, False),
    ],
)
def test_personal_room_creation_form(
    data,
    is_valid,
    gp_test_tenant,
    example_superuser,
    example_scheduling_strategy,
    example_meeting_configuration_template,
):
    form_data = {
        "scheduling_strategy": example_scheduling_strategy,
        "name": "Example PersonalRoom",
        "default_meeting_configuration": example_meeting_configuration_template,
    }
    form_data.update(data)
    form = PersonalRoomForm(
        data=form_data,
        co_qs=True,
        general_parameter=gp_test_tenant,
        user=example_superuser,
    )

    assert form.is_valid() is is_valid


@pytest.mark.django_db
@pytest.mark.parametrize(
    "data, is_valid",
    [
        ({"visibility": ROOM_VISIBILITY_PRIVATE}, True),
        ({"visibility": None}, False),
        ({}, False),
    ],
)
def test_personal_room_update_form(
    data,
    is_valid,
    gp_test_tenant,
    example_superuser,
    example_scheduling_strategy,
    example_meeting_configuration_template,
    example_personal_room,
    testserver_tenant,
):
    form_data = {
        "owner": example_superuser,
        "scheduling_strategy": example_scheduling_strategy,
        "name": "Example PersonalRoom",
        "default_meeting_configuration": example_meeting_configuration_template,
    }
    form_data.update(data)
    form = PersonalRoomForm(
        data=form_data,
        co_qs=True,
        general_parameter=gp_test_tenant,
        user=example_superuser,
        instance=example_personal_room,
    )

    assert form.is_valid() is is_valid


@pytest.mark.django_db
def test_personal_room_update_form_shared_resources(
    gp_test_tenant,
    example_superuser,
    example_scheduling_strategy,
    example_meeting_configuration_template,
    example_personal_room,
    testserver_tenant,
    testserver2_tenant,
):
    example_scheduling_strategy.tenants.add(testserver2_tenant)
    example_meeting_configuration_template.tenants.add(testserver2_tenant)
    form = PersonalRoomForm(
        data={
            "owner": example_superuser,
            "scheduling_strategy": example_scheduling_strategy,
            "name": "Example PersonalRoom",
            "default_meeting_configuration": example_meeting_configuration_template,
            "visibility": ROOM_VISIBILITY_PUBLIC,
        },
        co_qs=True,
        general_parameter=gp_test_tenant,
        user=example_superuser,
        instance=example_personal_room,
    )

    assert form.is_valid() is True
    assert form.fields["default_meeting_configuration"].queryset.count() == 1


def test_personal_room_co_owner_update_form(
    gp_test_tenant,
    example_superuser,
    example_scheduling_strategy,
    example_meeting_configuration_template,
    example_personal_room,
    example_user,
):
    form_data = {
        "scheduling_strategy": example_scheduling_strategy,
        "name": "Example PersonalRoom",
        "default_meeting_configuration": example_meeting_configuration_template,
        "visibility": ROOM_VISIBILITY_PUBLIC,
    }
    example_personal_room.co_owners.add(example_user)
    form = PersonalRoomForm(
        data=form_data,
        general_parameter=gp_test_tenant,
        user=example_user,
        instance=example_personal_room,
        co_qs=True,
    )

    assert form.is_valid() is True

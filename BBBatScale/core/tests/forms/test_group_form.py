import pytest
from core.forms import GroupForm


@pytest.mark.django_db
@pytest.mark.parametrize(
    "data, is_valid",
    [({"name": "Example"}, True), ({"name": ""}, False), ({}, False), ({"name": 1337}, True), ({"name": None}, False)],
)
def test_group_creation_form(
    data,
    is_valid,
):
    form = GroupForm(data=data)

    assert form.is_valid() is is_valid


@pytest.mark.django_db
@pytest.mark.parametrize(
    "data, is_valid",
    [({"name": "Example"}, True), ({"name": ""}, False), ({}, False), ({"name": 1337}, True), ({"name": None}, False)],
)
def test_group_update_form(
    data,
    is_valid,
    example_group,
):

    form = GroupForm(
        data=data,
        instance=example_group,
    )

    assert form.is_valid() is is_valid

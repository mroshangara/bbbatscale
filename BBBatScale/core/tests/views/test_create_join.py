import json

import freezegun
from core.constants import MEETING_STATE_RUNNING
from core.utils import generate_login_url
from core.views import create_join
from django.urls import reverse


def test_create_meeting_via_direct_join_inactive_room(client, example_room, current_timestamp, mocker):
    expected_timestamp = round(current_timestamp.timestamp()) * 1000

    s = client.session
    s.update(
        {
            "join_parameters": {
                "room_id": example_room.pk,
                "timestamp": expected_timestamp,
                "direct_join": True,
                "valid_secret": True,
            },
            "username": "testuser",
        }
    )
    s.save()

    mock_meeting_create = mocker.patch.object(create_join, "meeting_create")

    with freezegun.freeze_time(current_timestamp):
        response = client.get(reverse(create_join.create_meeting, kwargs={"room_id": example_room.id}))

    assert response.url == f"/core/join/waitingroom/{example_room.id}"
    assert mock_meeting_create.call_count == 1


def test_join_waiting_room_redirect_create_meeting(client, example_room, example_superuser):
    client.force_login(example_superuser)
    response = client.get(reverse(create_join.join_waiting_room, kwargs={"room_id": example_room.id}))
    assert response.status_code == 200
    assert response.context["login_url"] == generate_login_url("join_or_create_meeting", example_room.id)


def test_join_waiting_room_render_waiting_room(client, example_room, example_user):
    client.force_login(example_user)
    response = client.get(reverse(create_join.join_waiting_room, kwargs={"room_id": example_room.id}))
    assert response.status_code == 200
    assert response.context["login_url"] == generate_login_url("join_or_create_meeting", example_room.id)


def test_join_waiting_room_no_room(client, example_user):
    client.force_login(example_user)
    response = client.get(reverse(create_join.join_waiting_room, kwargs={"room_id": 666}))
    assert response.status_code == 404


def test_join_waiting_room_redirect_join_meeting(client, example_user, example_room, example_meeting):
    example_meeting.room = example_room
    example_meeting.state = MEETING_STATE_RUNNING
    example_meeting.save()

    client.force_login(example_user)
    response = client.get(reverse(create_join.join_waiting_room, kwargs={"room_id": example_room.id}))
    assert response.status_code == 302
    assert response.url == reverse(create_join.join_meeting, kwargs={"meeting_id": example_meeting.id})


def test_get_room_status_meeting_is_running(client, example_user, example_room, example_meeting):
    example_meeting.room = example_room
    example_meeting.state = MEETING_STATE_RUNNING
    example_meeting.save()

    client.force_login(example_user)
    response = client.get(reverse(create_join.get_room_status, kwargs={"room_id": example_room.id}))
    assert response.status_code == 200
    content = json.loads(response.content)
    assert content == {"is_running": True}


def test_get_room_status_no_meeting(client, example_user, example_room):
    client.force_login(example_user)
    response = client.get(reverse(create_join.get_room_status, kwargs={"room_id": example_room.id}))
    assert response.status_code == 200
    content = json.loads(response.content)
    assert content == {"is_running": False}


def test_get_room_status_no_room(client, example_user):
    client.force_login(example_user)
    response = client.get(reverse(create_join.get_room_status, kwargs={"room_id": 666}))
    assert response.status_code == 404

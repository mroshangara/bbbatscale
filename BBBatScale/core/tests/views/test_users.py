import json
from operator import itemgetter

import pytest
from core.models import User
from core.views import users
from django.conf import settings
from django.contrib.auth.models import Group
from django.contrib.sites.models import Site
from django.http import HttpRequest
from django.urls import get_resolver, reverse


@pytest.mark.django_db
def test_users_user_search_json(testserver_tenant: Site, testserver2_tenant: Site) -> None:
    user = User.objects.create(first_name="-", last_name="-", username="-", email="-", tenant=testserver_tenant)

    tenant_users = []

    for i in range(20):
        tenant_users.append(
            User.objects.create(
                first_name="User%02d" % i,
                last_name="Tenant01",
                username="user%02d-tenant01" % i,
                email="email%02d@tenant01.example.org" % i,
                tenant=testserver_tenant,
            )
        )
        User.objects.create(
            first_name="User%02d" % i,
            last_name="Tenant02",
            username="user%02d-tenant02" % i,
            email="email%02d@tenant02.example.org" % i,
            tenant=testserver2_tenant,
        )

    request = HttpRequest()
    request.META["HTTP_HOST"] = testserver_tenant.name
    request.user = user
    request.resolver_match = get_resolver().resolve(reverse(users.user_search_json))

    assert json.loads(users.user_search_json(request).content) == {"users": []}

    request.GET["q"] = "u"
    assert json.loads(users.user_search_json(request).content) == {"users": []}

    request.GET["q"] = "us"
    assert sorted(json.loads(users.user_search_json(request).content)["users"], key=itemgetter("id")) == sorted(
        {"users": [{"id": tenant_user.id, "display_name": str(tenant_user)} for tenant_user in tenant_users]}["users"],
        key=itemgetter("id"),
    )

    request.GET["q"] = "email"
    assert sorted(json.loads(users.user_search_json(request).content)["users"], key=itemgetter("id")) == sorted(
        {"users": [{"id": tenant_user.id, "display_name": str(tenant_user)} for tenant_user in tenant_users]}["users"],
        key=itemgetter("id"),
    )

    request.GET["q"] = "TeNaNt01"
    assert sorted(json.loads(users.user_search_json(request).content)["users"], key=itemgetter("id")) == sorted(
        {"users": [{"id": tenant_user.id, "display_name": str(tenant_user)} for tenant_user in tenant_users]}["users"],
        key=itemgetter("id"),
    )

    request.GET["q"] = "tenant02"
    assert json.loads(users.user_search_json(request).content) == {"users": []}

    request.GET["q"] = "uSeR0"
    assert sorted(json.loads(users.user_search_json(request).content)["users"], key=itemgetter("id")) == sorted(
        {"users": [{"id": tenant_user.id, "display_name": str(tenant_user)} for tenant_user in tenant_users[:10]]}[
            "users"
        ],
        key=itemgetter("id"),
    )


@pytest.mark.django_db
def test_update_moderator_group_from_user(client, example_superuser, example_group):
    group = Group.objects.create(name=settings.MODERATORS_GROUP)
    client.force_login(example_superuser)
    response = client.post(
        reverse(users.user_update, args=[example_superuser.pk]),
        data={"username": example_superuser.get_username(), "groups": {group.pk, example_group.pk}, "theme": 1},
    )
    assert response.status_code == 302
    assert example_superuser.is_moderator() is True


@pytest.mark.django_db
def test_add_moderator_group_to_user(client, example_superuser, example_group):
    group = Group.objects.create(name=settings.MODERATORS_GROUP)
    client.force_login(example_superuser)
    response = client.post(
        reverse(users.user_create),
        data={"username": "testuser", "password": 123, "groups": {group.pk, example_group.pk}, "theme": 1},
    )
    assert response.status_code == 302
    assert User.objects.filter(username="testuser").count() == 1
    assert User.objects.get(username="testuser").is_moderator() is True


@pytest.mark.django_db
def test_delete_moderator_group_from_user(client, example_superuser, example_group):
    group = Group.objects.create(name=settings.MODERATORS_GROUP)
    client.force_login(example_superuser)
    response = client.post(
        reverse(users.user_update, args=[example_superuser.pk]),
        data={"username": example_superuser.get_username(), "groups": {group.pk}, "theme": 1},
    )
    assert response.status_code == 302
    assert example_superuser.is_moderator() is True

    response = client.post(
        reverse(users.user_update, args=[example_superuser.pk]),
        data={"username": example_superuser.get_username(), "groups": {example_group.pk}, "theme": 1},
    )
    assert response.status_code == 302
    assert example_superuser.is_moderator() is False

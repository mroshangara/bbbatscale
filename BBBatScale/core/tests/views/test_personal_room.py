import pytest
from core.constants import ROOM_VISIBILITY_PRIVATE, ROOM_VISIBILITY_PUBLIC
from core.forms import PersonalRoomForm
from core.models import PersonalRoom
from core.views import personal_room
from django.urls import reverse


@pytest.mark.django_db
def test_personal_room_create_get(client, example_personal_room, example_user, testserver_tenant):
    client.force_login(example_user)
    response = client.get(reverse(personal_room.personal_room_create), SERVER_NAME=testserver_tenant.name)

    assert response.status_code == 200

    assert type(response.context["form"]) is PersonalRoomForm


@pytest.mark.django_db
def test_personal_room_create_post(
    client,
    example_meeting_configuration_template,
    example_superuser,
    example_scheduling_strategy,
    gp_test_tenant,
    testserver_tenant,
):
    PersonalRoom.objects.all().delete()
    client.force_login(example_superuser)

    gp_test_tenant.personal_rooms_enabled = True
    gp_test_tenant.personal_rooms_scheduling_strategy = example_scheduling_strategy
    gp_test_tenant.save()

    form_data = {
        "name": ["test_room_name"],
        "default_meeting_configuration": [str(example_meeting_configuration_template.pk)],
        "visibility": ["private"],
        "comment_public": [""],
        "filter_owner": [""],
    }

    response = client.post(
        reverse(personal_room.personal_room_create), SERVER_NAME=testserver_tenant.name, data=form_data
    )

    assert response.status_code == 302
    assert PersonalRoom.objects.all().count() == 1
    assert PersonalRoom.objects.filter(name="test_room_name").exists() is True


@pytest.mark.django_db
def test_personal_room_update_as_owner_with_get(
    client, example_personal_room, example_meeting_configuration_template, example_user, testserver_tenant
):
    example_personal_room.owner = example_user

    example_personal_room.save()

    client.force_login(example_user)

    response = client.get(
        reverse("personal_room_update", args=[example_personal_room.id]), SERVER_NAME=testserver_tenant.name
    )

    assert response.status_code == 200

    assert type(response.context["form"]) is PersonalRoomForm


@pytest.mark.django_db
def test_personal_room_update_as_owner_with_post(
    client, example_personal_room, example_meeting_configuration_template, example_superuser, testserver_tenant
):
    example_personal_room.visibility = ROOM_VISIBILITY_PUBLIC
    example_personal_room.save()

    client.force_login(example_superuser)

    form_data = {
        "owner": [example_superuser.pk],
        "name": [str(example_personal_room.name)],
        "default_meeting_configuration": [example_meeting_configuration_template.pk],
        "visibility": ["private"],
        "comment_public": [""],
        "filter_owner": [""],
    }

    response = client.post(
        reverse("personal_room_update", args=[example_personal_room.id]),
        SERVER_NAME=testserver_tenant.name,
        data=form_data,
        user=example_superuser,
        instance=example_personal_room,
    )

    example_personal_room = PersonalRoom.objects.get(id=example_personal_room.id)

    assert response.status_code == 302

    assert example_personal_room.visibility == ROOM_VISIBILITY_PRIVATE


@pytest.mark.django_db
def test_personal_room_delete(client, example_personal_room, example_user):
    example_personal_room.owner = example_user
    example_personal_room.save()

    client.force_login(example_user)

    response = client.get(reverse("personal_room_delete", args=[example_personal_room.id]))

    assert response.status_code == 302

    assert PersonalRoom.objects.filter(id=example_personal_room.id).exists() is False

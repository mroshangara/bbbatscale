import pytest
from core.models import Meeting
from core.views import recordings
from django.urls import reverse


@pytest.mark.django_db
def test_post_recording_update_title(client, example_superuser, example_group, gp_test_tenant, mocker):
    def mock_update_publish_recordings(params, rpc, meta):
        pass

    mocker.patch("core.views.recordings.update_publish_recordings", mock_update_publish_recordings)

    client.force_login(example_superuser)
    example_recording = Meeting.objects.create(
        room_name="Example Room",
        replay_id="example-replay-id",
        replay_url="https://example.org/my-awesome-recording",
        replay_title="Test title",
    )
    response = client.post(
        reverse(recordings.recording_update, args=[example_recording.id]),
        data={"replay_title": "Changed example title"},
    )
    assert response.status_code == 302
    assert Meeting.objects.get(replay_id=example_recording.replay_id).replay_title == "Changed example title"

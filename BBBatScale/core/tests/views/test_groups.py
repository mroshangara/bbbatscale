import pytest
from core.views import groups
from django.contrib.auth.models import Group
from django.urls import reverse


@pytest.mark.django_db
def test_get_group_overview_normal_user(client, example_user, testserver_tenant):
    client.force_login(example_user)
    response = client.get(reverse(groups.groups_overview), SERVER_NAME=testserver_tenant.name)
    assert response.status_code == 302
    assert response.url == "/admin/login/?next=/core/groups/overview/"


@pytest.mark.django_db
def test_get_group_overview_super_user(client, example_superuser, testserver_tenant):
    client.force_login(example_superuser)
    response = client.get(reverse(groups.groups_overview), SERVER_NAME=testserver_tenant.name)
    assert response.status_code == 200


@pytest.mark.django_db
def test_get_group_overview_staff_user(client, example_staff_user, testserver_tenant):
    client.force_login(example_staff_user)
    response = client.get(reverse(groups.groups_overview), SERVER_NAME=testserver_tenant.name)
    assert response.status_code == 200


@pytest.mark.django_db
def test_post_group_create_normal_user(client, example_user, testserver_tenant):
    client.force_login(example_user)
    response = client.post(reverse(groups.group_create), data={"name": "Example group"})
    assert response.status_code == 302
    assert response.url == "/admin/login/?next=/core/group/create/"


@pytest.mark.django_db
def test_post_group_create_super_user(client, example_superuser, testserver_tenant):
    client.force_login(example_superuser)
    response = client.post(reverse(groups.group_create), data={"name": "Example group"})
    assert response.status_code == 302
    assert Group.objects.filter(name="Example group").count() == 1


@pytest.mark.django_db
def test_post_group_create_staff_user(client, example_staff_user, testserver_tenant):
    client.force_login(example_staff_user)
    response = client.post(reverse(groups.group_create), data={"name": "Example group"})
    assert response.status_code == 302
    assert Group.objects.filter(name="Example group").count() == 1


@pytest.mark.django_db
def test_post_group_create_false_post_data(client, example_staff_user, testserver_tenant):
    client.force_login(example_staff_user)
    response = client.post(reverse(groups.group_create), data={"asdf": "Example group"})
    assert response.status_code == 200
    assert Group.objects.filter(name="Example group").count() == 0


@pytest.mark.django_db
def test_get_group_delete_normal_user(client, example_user, example_group, testserver_tenant):
    client.force_login(example_user)
    response = client.get(reverse(groups.group_delete, args=[example_group.pk]), SERVER_NAME=testserver_tenant.name)
    assert response.status_code == 302
    assert Group.objects.filter(name="Example group").count() == 1


@pytest.mark.django_db
def test_get_group_delete_staff_user(client, example_staff_user, example_group, testserver_tenant):
    client.force_login(example_staff_user)
    response = client.get(reverse(groups.group_delete, args=[example_group.pk]), SERVER_NAME=testserver_tenant.name)
    assert response.status_code == 302
    assert Group.objects.filter(name="Example group").count() == 0


@pytest.mark.django_db
def test_get_group_delete_super_user(client, example_superuser, example_group, testserver_tenant):
    client.force_login(example_superuser)
    response = client.get(reverse(groups.group_delete, args=[example_group.pk]), SERVER_NAME=testserver_tenant.name)
    assert response.status_code == 302
    assert Group.objects.filter(name="Example group").count() == 0


@pytest.mark.django_db
def test_get_group_delete_false_get_data(client, example_superuser, example_group, testserver_tenant):
    client.force_login(example_superuser)
    response = client.get(reverse(groups.group_delete, args=[1337]), SERVER_NAME=testserver_tenant.name)
    assert response.status_code == 404
    assert Group.objects.filter(name="Example group").count() == 1


@pytest.mark.django_db
def test_post_group_update_normal_user(client, example_user, example_group, testserver_tenant):
    client.force_login(example_user)
    response = client.post(
        reverse(groups.group_update, args=[example_group.pk]), data={"name": "Changed example group"}
    )
    assert response.status_code == 302
    assert Group.objects.filter(name="Example group").count() == 1
    assert Group.objects.filter(name="Changed example group").count() == 0


@pytest.mark.django_db
def test_post_group_update_staff_user(client, example_staff_user, example_group, testserver_tenant):
    client.force_login(example_staff_user)
    response = client.post(
        reverse(groups.group_update, args=[example_group.pk]), data={"name": "Changed example group"}
    )
    assert response.status_code == 302
    assert Group.objects.filter(name="Example group").count() == 0
    assert Group.objects.filter(name="Changed example group").count() == 1


@pytest.mark.django_db
def test_post_group_update_super_user(client, example_superuser, example_group, testserver_tenant):
    client.force_login(example_superuser)
    response = client.post(
        reverse(groups.group_update, args=[example_group.pk]), data={"name": "Changed example group"}
    )
    assert response.status_code == 302
    assert Group.objects.filter(name="Example group").count() == 0
    assert Group.objects.filter(name="Changed example group").count() == 1


@pytest.mark.django_db
def test_post_group_update_false_post_data(client, example_staff_user, example_group, testserver_tenant):
    client.force_login(example_staff_user)
    response = client.post(
        reverse(groups.group_update, args=[example_group.pk]), data={"asdf": "Changed example group"}
    )
    assert response.status_code == 200
    assert Group.objects.filter(name="Example group").count() == 1
    assert Group.objects.filter(name="Changed example group").count() == 0

# encoding: utf-8
import logging

from core.services import delete_recordings_internal
from django.core.management.base import BaseCommand

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = "Deletes all recordings whose termination has been reached."

    def handle(self, *args, **options):
        logger.info("Start delete_recordings command ")

        delete_recordings_internal()

        logger.info("Finished delete_recordings command ")

# encoding: utf-8
import logging
from datetime import timedelta

from core.constants import MEETING_STATE_RUNNING
from core.models import Meeting
from core.services import end_meeting
from core.utils import BigBlueButton
from django.core.management.base import BaseCommand
from django.db import transaction
from django.utils import timezone

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = "Perform health check on all servers."

    @transaction.non_atomic_requests
    def handle(self, *args, **options):
        logger.info("Start house_keeping command ")

        # clear monitoring values on inactive rooms
        for meeting in Meeting.objects.filter(
            last_health_check__lte=timezone.now() - timedelta(seconds=120), state=MEETING_STATE_RUNNING
        ):
            logger.debug("Meeting ({}) loaded from DB to check if house cleaning is necessary".format(meeting))
            try:
                if not BigBlueButton.validate_is_meeting_running(
                    BigBlueButton(meeting.server.dns, meeting.server.shared_secret).is_meeting_running(meeting.id)
                ):
                    logger.debug("Room ({}) is a candidate for house keeping. Room is orphaned.")
                    with transaction.atomic():
                        end_meeting(meeting)
                        logger.warning(
                            "Meeting ({} - {}) was reset. ".format(meeting, meeting.id)
                            + "Possible indication that webhooks are not working properly"
                        )
            except AttributeError:
                end_meeting(meeting)
                logger.warning(
                    "Meeting ({} - {}) was reset because it had no server associated.".format(meeting, meeting.id)
                )
        logger.info("End house_keeping command")

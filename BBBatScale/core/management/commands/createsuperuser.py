from typing import Any

from django.contrib.auth.management.commands import createsuperuser
from django.contrib.sites.models import Site
from django.db.models import Field
from django.utils.text import capfirst


class Command(createsuperuser.Command):
    def get_input_data(self, field: Field, message: Any, default: Any = None) -> Any:
        if field.name == "tenant":
            raw_value = input("%s: " % capfirst(field.verbose_name))
            if default and raw_value == "":
                raw_value = default
            try:
                val = Site.objects.get(domain=raw_value)
            except Site.DoesNotExist:
                self.stderr.write("Error: %s “%s” does not exist." % (capfirst(field.verbose_name), raw_value))
                val = None

            return val

        return super().get_input_data(field, message, default)

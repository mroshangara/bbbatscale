import logging
import re
from typing import Iterable, Optional, Union

from crispy_forms.bootstrap import FieldWithButtons, StrictButton
from crispy_forms.helper import FormHelper
from crispy_forms.layout import HTML, Div, Field, Fieldset, Layout, MultiWidgetField, Submit
from django import forms
from django.conf import settings
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth.models import Group
from django.contrib.sites.models import Site
from django.core import validators
from django.core.exceptions import ValidationError
from django.core.validators import FileExtensionValidator
from django.db.models import Q, Subquery
from django.utils import tree
from django.utils.formats import get_format
from django.utils.translation import gettext_lazy as _

from .models import (
    AgentConfiguration,
    ApiToken,
    GeneralParameter,
    Meeting,
    MeetingConfiguration,
    MeetingConfigurationTemplate,
    PersonalRoom,
    Room,
    SchedulingStrategy,
    Server,
    ServerType,
    Slides,
    User,
    mark_slides_file_for_removal,
)
from .utils import (
    format_human_readable_size,
    get_permissions,
    has_tenant_based_perm,
    is_media_enabled,
    parse_human_readable_size,
)

logger = logging.getLogger(__name__)


class IntRepresentedAsByteSize(int):
    def __new__(cls, x: Union[str, int] = 0) -> "IntRepresentedAsByteSize":
        return int.__new__(cls, parse_human_readable_size(x) if isinstance(x, str) else x)

    def __repr__(self) -> str:
        return format_human_readable_size(int(self))

    def __str__(self) -> str:
        return format_human_readable_size(int(self))


class ByteSizeInput(forms.fields.TextInput):
    def format_value(self, value):
        return str(IntRepresentedAsByteSize(value))

    def get_context(self, name, value, attrs):
        context = super().get_context(name, value, attrs)

        ts = re.escape(get_format("THOUSAND_SEPARATOR"))
        ds = re.escape(get_format("DECIMAL_SEPARATOR"))
        byte_number_regex = f"(?:\\d+|\\d{{1,3}}(?:{ts}\\d{{3}})+)(?:(?:{ds}\\d+)?[kMGTPEZY]i?)?B"

        context["widget"]["attrs"] = self.build_attrs(
            context["widget"]["attrs"],
            {"pattern": f"^{byte_number_regex}$"},
        )
        return context


class ByteSizeField(forms.IntegerField):
    widget = ByteSizeInput

    def __init__(self, *, max_value=None, min_value=None, **kwargs):
        self.max_value, self.min_value = max_value, min_value
        # Localized number input is not well-supported on most browsers
        kwargs["localize"] = False
        super().__init__(**kwargs)

        if max_value is not None:
            self.validators.append(validators.MaxValueValidator(IntRepresentedAsByteSize(max_value)))
        if min_value is not None:
            self.validators.append(validators.MinValueValidator(IntRepresentedAsByteSize(min_value)))

    def has_changed(self, initial, data):
        if self.disabled:
            return False
        try:
            if not isinstance(data, str):
                data = str(IntRepresentedAsByteSize(data))
        except ValidationError:
            return True
        # For purposes of seeing whether something has changed, None is
        # the same as an empty string, if the data or initial value we get
        # is None, replace it with "".
        initial_value = str(IntRepresentedAsByteSize(initial)) if initial is not None else ""
        data_value = data if data is not None else ""
        return initial_value != data_value

    def clean(self, value):
        value = super().clean(value)
        # convert into string and back to be sure, "what you see is what you get"
        return int(IntRepresentedAsByteSize(str(IntRepresentedAsByteSize(value))))

    def to_python(self, value):
        value = super(forms.IntegerField, self).to_python(value)
        if value in self.empty_values:
            return None
        try:
            value = int(IntRepresentedAsByteSize(value))
        except (ValueError, TypeError):
            raise ValidationError(self.error_messages["invalid"], code="invalid")
        return value


def init_slides_fields(form: forms.ModelForm, slides_query_filter: tree.Node = Q()) -> None:
    if form.instance.pk is not None:
        slides_query_filter |= Q(pk__in=Subquery(form.instance.slides.values("pk")))

    form._meta.fields.append("slides")
    form.fields["slides"] = form._meta.model._meta.get_field("slides").formfield()
    form.fields["slides"].label = _("Slides")
    form.fields["slides"].queryset = form.fields["slides"].queryset.filter(slides_query_filter)
    form.fields["slides"].widget.option_template_name = "file_select_option.html"

    form._meta.fields.append("initial_slides")
    form.fields["initial_slides"] = form._meta.model._meta.get_field("initial_slides").formfield()
    form.fields["initial_slides"].label = _("Initial Slides")
    form.fields["initial_slides"].queryset = form.fields["slides"].queryset.filter(slides_query_filter)
    form.fields["initial_slides"].widget = forms.HiddenInput()


class GeneralParametersForm(forms.ModelForm):
    class Meta:
        model = GeneralParameter
        fields = [
            "latest_news",
            "jitsi_url",
            "faq_url",
            "feedback_email",
            "footer",
            "logo_link",
            "favicon_link",
            "page_title",
            "home_room_enabled",
            "playback_url",
            "home_room_teachers_only",
            "home_room_scheduling_strategy",
            "enable_occupancy",
            "download_url",
            "home_room_room_configuration",
            "jitsi_enable",
            "default_theme",
            "personal_rooms_enabled",
            "personal_rooms_scheduling_strategy",
            "personal_rooms_teacher_max_number",
            "personal_rooms_non_teacher_max_number",
            "enable_recordings",
            "enable_adminunmute",
            "hide_statistics_enable",
            "hide_statistics_logged_in_users",
            "show_personal_rooms",
            "show_home_rooms",
            "app_title",
            "recording_management_url",
            "recording_cert",
            "recording_key",
            "recording_deletion_period",
        ]

    def __init__(self, *args, user: User, **kwargs):
        super(GeneralParametersForm, self).__init__(*args, **kwargs)

        if settings.BBBATSCALE_MEDIA_ENABLED:
            self._meta.fields.append("media_enabled")
            self.fields["media_enabled"] = self._meta.model._meta.get_field("media_enabled").formfield()

        if is_media_enabled(self.instance):
            self._meta.fields.append("moderators_can_upload_slides")
            self.fields["moderators_can_upload_slides"] = self._meta.model._meta.get_field(
                "moderators_can_upload_slides"
            ).formfield()

            self._meta.fields.append("slides_max_upload_size")
            self.fields["slides_max_upload_size"] = self._meta.model._meta.get_field(
                "slides_max_upload_size"
            ).formfield(form_class=ByteSizeField)

            if has_tenant_based_perm(user, "core.view_slides", self.instance.tenant):
                init_slides_fields(self, Q(owner__isnull=True))

        self.helper = FormHelper()
        self.helper.form_class = "form-horizontal"
        self.helper.label_class = "col-lg-3"
        self.helper.field_class = "col-lg-9"
        self.helper.layout = Layout(
            Field("latest_news"),
            HTML(
                '<hr><div class="row mb-5"><div class="col-lg-3"></div><div class="col-lg-9">'
                "<h4>{}</h4></div></div>".format(_("General site settings"))
            ),
            Field("page_title"),
            Field("app_title"),
            Field("logo_link"),
            Field("favicon_link"),
            Field("default_theme"),
            Field("jitsi_enable"),
            Field("jitsi_url"),
            Field("faq_url"),
            Field("hide_statistics_logged_in_users"),
            Field("hide_statistics_enable"),
            Field("enable_adminunmute"),
            HTML(
                '<hr><div class="row mb-5"><div class="col-lg-3"></div><div class="col-lg-9">'
                "<h4>{}</h4></div></div>".format(_("Feature: Recordings"))
            ),
            Field("enable_recordings"),
            Field("recording_deletion_period"),
            Field("recording_management_url"),
            Field("recording_cert"),
            Field("recording_key"),
            Field("playback_url"),
            Field("download_url"),
            Field("footer"),
            HTML(
                '<hr><div class="row mb-5"><div class="col-lg-3"></div><div class="col-lg-9">'
                "<h4>{}</h4></div></div>".format(_("Feature: Home room"))
            ),
            Field("home_room_enabled"),
            Field("show_home_rooms"),
            Field("home_room_teachers_only"),
            Field("home_room_scheduling_strategy"),
            Field("home_room_room_configuration"),
            HTML(
                '<hr><div class="row mb-5"><div class="col-lg-3"></div><div class="col-lg-9">'
                "<h4>{}</h4></div></div>".format(_("Feature: Personal room"))
            ),
            Field("personal_rooms_enabled"),
            Field("show_personal_rooms"),
            Field("personal_rooms_scheduling_strategy"),
            Field("personal_rooms_teacher_max_number"),
            Field("personal_rooms_non_teacher_max_number"),
            Field("enable_occupancy"),
            *(
                [
                    HTML(
                        '<hr><div class="row mb-5"><div class="col-lg-3"></div><div class="col-lg-9">'
                        "<h4>{}</h4></div></div>".format(_("Feature: Media"))
                    ),
                    Field("media_enabled"),
                ]
                if "media_enabled" in self.fields
                else []
            ),
            *([Field("moderators_can_upload_slides")] if "moderators_can_upload_slides" in self.fields else []),
            *([Field("slides_max_upload_size")] if "slides_max_upload_size" in self.fields else []),
            *([Field("slides")] if "slides" in self.fields else []),
            *([Field("initial_slides")] if "initial_slides" in self.fields else []),
            Submit("save", _("Save"), css_class="btn-primary"),
        )

    def _save_m2m(self):
        super()._save_m2m()

        if self.instance.media_enabled:
            group = Group.objects.get_or_create(name=settings.MODERATORS_GROUP)[0]
            permission_pks = get_permissions(
                "core.add_own_slides", "core.change_own_slides", "core.delete_own_slides", "core.view_own_slides"
            ).values_list("pk", flat=True)
            if self.instance.moderators_can_upload_slides:
                group.permissions.add(*permission_pks)
            else:
                group.permissions.remove(*permission_pks)


class ServerForm(forms.ModelForm):
    class Meta:
        model = Server
        fields = [
            "dns",
            "datacenter",
            "shared_secret",
            "scheduling_strategy",
            "participant_count_max",
            "videostream_count_max",
            "server_types",
            "machine_id",
        ]

    def __init__(self, *args, general_parameter: GeneralParameter, **kwargs):
        super(ServerForm, self).__init__(*args, **kwargs)

        self.fields["dns"].label = _("DNS")
        self.fields["machine_id"].label = _("Maschine Id")
        self.fields["scheduling_strategy"].label = _("Scheduling Strategy")
        scheduling_strategy_filter = Q(tenants__in=[general_parameter.tenant])
        if self.instance.scheduling_strategy_id:
            scheduling_strategy_filter |= Q(id=self.instance.scheduling_strategy_id)

        self.fields["scheduling_strategy"].queryset = SchedulingStrategy.objects.filter(
            scheduling_strategy_filter
        ).distinct()
        self.fields["datacenter"].label = _("Datacenter")
        self.fields["shared_secret"].label = _("Shared Secret")
        self.fields["shared_secret"].required = True
        self.fields["participant_count_max"].label = _("Max participants")
        self.fields["videostream_count_max"].label = _("Max videostreams")
        self.fields["server_types"].label = _("Server Types")
        self.fields["server_types"].queryset = ServerType.objects.all()
        self.fields["server_types"].help_text = _(
            "Server types are determining which work is scheduled on that server."
        )

        self.helper = FormHelper()
        self.helper.form_class = "form-horizontal"
        self.helper.label_class = "col-lg-4"
        self.helper.field_class = "col-lg-8"
        self.helper.layout = Layout(
            Field("dns"),
            Field("machine_id"),
            Field("scheduling_strategy"),
            Field("datacenter"),
            Field("shared_secret"),
            MultiWidgetField("server_types"),
            Field("participant_count_max"),
            Field("videostream_count_max"),
            Submit("save", _("Save"), css_class="btn-primary"),
        )


class RoomForm(forms.ModelForm):
    class Meta:
        model = Room
        fields = [
            "scheduling_strategy",
            "name",
            "default_meeting_configuration",
            "visibility",
            "state",
            "direct_join_secret",
            "comment_public",
            "comment_private",
            "click_counter",
            "event_collection_strategy",
            "event_collection_parameters",
            "last_running",
            "tenants",
        ]

    def __init__(self, *args, general_parameter: GeneralParameter, user: User, **kwargs):
        super(RoomForm, self).__init__(*args, **kwargs)

        self.fields["scheduling_strategy"].label = _("Scheduling Strategy")
        scheduling_strategy_filter = Q(tenants__in=[general_parameter.tenant])
        if self.instance.scheduling_strategy_id:
            scheduling_strategy_filter |= Q(id=self.instance.scheduling_strategy_id)

        self.fields["scheduling_strategy"].queryset = SchedulingStrategy.objects.filter(
            scheduling_strategy_filter
        ).distinct()
        self.fields["tenants"].label = _("Tenants")
        self.fields["name"].label = _("Name")
        self.fields["default_meeting_configuration"].label = _("Configuration")
        default_meeting_configuration_filter = Q(tenants__in=[general_parameter.tenant])
        if self.instance.default_meeting_configuration_id:
            default_meeting_configuration_filter |= Q(id=self.instance.default_meeting_configuration_id)
        self.fields["default_meeting_configuration"].queryset = MeetingConfigurationTemplate.objects.filter(
            default_meeting_configuration_filter
        ).distinct()
        self.fields["visibility"].label = _("Visible on front page for")
        self.fields["comment_private"].label = _("Comment private")
        self.fields["comment_public"].label = _("Comment public")
        self.fields["event_collection_strategy"].label = _("Collection Strategy")
        self.fields["event_collection_parameters"].label = _("Collection Parameters")

        # System automatic configs
        self.fields["state"].label = _("State")
        self.fields["direct_join_secret"].label = _("Direct Join Secret")
        self.fields["click_counter"].label = _("Click counter")
        self.fields["last_running"].label = _("Last running")

        if is_media_enabled(general_parameter) and has_tenant_based_perm(
            user, "core.view_slides", general_parameter.tenant
        ):
            init_slides_fields(self, Q(owner__isnull=True))

        self.helper = FormHelper()
        self.helper.form_class = "form-horizontal"
        self.helper.label_class = "col-lg-4"
        self.helper.field_class = "col-lg-8"
        self.helper.layout = Layout(
            Field("scheduling_strategy"),
            Field("name"),
            Field("default_meeting_configuration"),
            Field("tenants"),
            Field("visibility"),
            Field("comment_private"),
            Field("comment_public"),
            HTML(
                '<hr><div class="row mb-5"><div class="col-lg-4"></div><div class="col-lg-8">'
                "<h4>{}</h4></div></div>".format(_("Event Collection Configs"))
            ),
            Field("event_collection_strategy"),
            Field("event_collection_parameters"),
            HTML(
                '<hr><div class="row mb-5"><div class="col-lg-4"></div><div class="col-lg-8">'
                "<h4>{}</h4></div></div>".format(_("System provided values"))
            ),
            Field("state"),
            FieldWithButtons(
                Field("direct_join_secret", id="direct_join_secret"),
                StrictButton(_("Generate Secret"), css_class="btn-secondary", id="direct_join_secret_generate"),
            ),
            Field("click_counter"),
            Field("last_running"),
            *([Field("slides")] if "slides" in self.fields else []),
            *([Field("initial_slides")] if "initial_slides" in self.fields else []),
            Submit("save", _("Save"), css_class="btn-primary"),
        )


class HomeRoomForm(forms.ModelForm):
    class Meta:
        model = Room
        fields = ["name", "default_meeting_configuration", "visibility", "comment_public"]

    def __init__(self, *args, general_parameter: GeneralParameter, user: User, **kwargs):
        super(HomeRoomForm, self).__init__(*args, **kwargs)

        self.fields["name"].label = _("Name")
        self.fields["default_meeting_configuration"].label = _("Configuration")
        default_meeting_configuration_filter = Q(tenants__in=[general_parameter.tenant])
        if self.instance.default_meeting_configuration_id:
            default_meeting_configuration_filter |= Q(id=self.instance.default_meeting_configuration_id)
        self.fields["default_meeting_configuration"].queryset = MeetingConfigurationTemplate.objects.filter(
            default_meeting_configuration_filter
        ).distinct()
        self.fields["visibility"].label = _("Visible on front page for")
        self.fields["comment_public"].label = _("Comment")

        if is_media_enabled(general_parameter) and has_tenant_based_perm(
            user, "core.view_own_slides", general_parameter.tenant
        ):
            init_slides_fields(self, Q(owner=self.instance.owner))

        self.helper = FormHelper()
        self.helper.form_class = "form-horizontal"
        self.helper.label_class = "col-lg-4"
        self.helper.field_class = "col-lg-8"
        self.helper.layout = Layout(
            Field("name", readonly=True, diabled=True),
            Field("default_meeting_configuration"),
            Field("visibility"),
            Field("comment_public"),
            *([Field("slides")] if "slides" in self.fields else []),
            *([Field("initial_slides")] if "initial_slides" in self.fields else []),
            Submit("save", _("Save"), css_class="btn-primary"),
        )


class PersonalRoomForm(forms.ModelForm):
    class Meta:
        model = PersonalRoom
        fields = ["owner", "name", "default_meeting_configuration", "visibility", "comment_public", "co_owners"]

    def view_owner(self, user):
        if self.instance.id is None:
            self.fields["owner"].widget = forms.HiddenInput()
            self.fields["owner"].required = False
            return

        if self.instance.has_co_owner(user):
            self.fields["owner"].label = _("Owner")
            self.fields["owner"].disabled = True
            self.fields["owner"].required = True

            self.fields["name"].disabled = True

            self.fields["visibility"].disabled = True

            self.fields["comment_public"].disabled = True
            return

        self.fields["owner"].widget = forms.HiddenInput()

    def __init__(self, *args, co_qs: bool, user: User, general_parameter: GeneralParameter, **kwargs):
        super(PersonalRoomForm, self).__init__(*args, **kwargs)

        self.view_owner(user)

        self.fields["name"].required = True
        self.fields["visibility"].required = True
        self.fields["name"].label = _("Name")
        self.fields["default_meeting_configuration"].label = _("Room configuration")
        default_meeting_configuration_filter = Q(tenants__in=[general_parameter.tenant])
        if self.instance.default_meeting_configuration_id:
            default_meeting_configuration_filter |= Q(id=self.instance.default_meeting_configuration_id)
        self.fields["default_meeting_configuration"].queryset = MeetingConfigurationTemplate.objects.filter(
            default_meeting_configuration_filter
        ).distinct()
        self.fields["visibility"].label = _("Visible on front page for")
        self.fields["comment_public"].label = _("Comment")
        self.fields["co_owners"].label = _("Co-Owners")
        if co_qs:
            self.fields["co_owners"].queryset = User.objects.all()
        else:
            self.fields["co_owners"].queryset = (
                User.objects.none() if not self.instance.pk else self.instance.co_owners.all()
            )

        if is_media_enabled(general_parameter) and has_tenant_based_perm(
            user, "core.view_own_slides", general_parameter.tenant
        ):
            init_slides_fields(self, Q(owner=user))

        self.helper: FormHelper = FormHelper()
        self.helper.form_class = "form-horizontal"
        self.helper.label_class = "col-lg-4"
        self.helper.field_class = "col-lg-8"
        self.helper.layout = Layout(
            Field("owner"),
            Field("name"),
            Field("default_meeting_configuration"),
            Field("visibility"),
            Field("comment_public"),
            HTML(
                '<hr><div class="form-group row "><label for="id_filter_owner" '
                'class="col-form-label col-lg-4">Filter Users</label>'
                '<div class="col-lg-8"><input type="text" name="filter_owner" '
                'class="textinput textInput form-control" id="id_filter_owner"></div></div>'
            ),
            MultiWidgetField("co_owners"),
            *([Field("slides")] if "slides" in self.fields else []),
            *([Field("initial_slides")] if "initial_slides" in self.fields else []),
            Submit("save", _("Save"), css_class="btn-primary"),
        )


class MeetingConfigurationsTemplateForm(forms.ModelForm):
    class Meta:
        model = MeetingConfigurationTemplate
        fields = [
            "name",
            "mute_on_start",
            "moderation_mode",
            "meetingLayout",
            "everyone_can_start",
            "allow_guest_entry",
            "access_code",
            "only_prompt_guests_for_access_code",
            "disable_mic",
            "disable_cam",
            "allow_recording",
            "disable_note",
            "disable_private_chat",
            "disable_public_chat",
            "allow_unmuteusers",
            "allow_ejectcam",
            "allow_learningdashboard",
            "authenticated_user_can_start",
            "guest_policy",
            "dialNumber",
            "welcome_message",
            "logoutUrl",
            "maxParticipants",
            "streamingUrl",
        ]

    def __init__(self, *args, **kwargs):
        super(MeetingConfigurationsTemplateForm, self).__init__(*args, **kwargs)

        self.fields["name"].label = _("Name")
        self.fields["mute_on_start"].label = _("Everyone is muted on start")
        self.fields["moderation_mode"].label = _("Moderation mode")
        self.fields["meetingLayout"].label = _("Meeting Layout")
        self.fields["everyone_can_start"].label = _("Everyone can start meeting")
        self.fields["authenticated_user_can_start"].label = _("Registered user can start meeting")
        self.fields["allow_guest_entry"].label = _("Allow guests")
        self.fields["access_code"].label = _("Access code")
        self.fields["only_prompt_guests_for_access_code"].label = _("Only prompt guests for access code")
        self.fields["disable_cam"].label = _("Disable camera for non moderators")
        self.fields["disable_mic"].label = _("Disable microphone for non moderators")
        self.fields["allow_recording"].label = _("Allow recording")
        self.fields["disable_note"].label = _("Disable shared notes - editing only for moderators")
        self.fields["disable_public_chat"].label = _("Disable public chat - editing only for moderators")
        self.fields["disable_private_chat"].label = _("Disable private chat - editing only for moderators")
        self.fields["allow_unmuteusers"].label = _("Allow Moderators to unmute Users")
        self.fields["allow_ejectcam"].label = _("Allow Moderators to eject Users-Webcams")
        self.fields["allow_learningdashboard"].label = _("Enable Learning Dashboard")
        self.fields["guest_policy"].label = _("Guest policy")
        self.fields["logoutUrl"].label = _("Logout URL")
        self.fields["dialNumber"].label = _("Dial number")
        self.fields["welcome_message"].label = _("Welcome message")
        self.fields["welcome_message"].help_text = _("Max. 255 characters. Limited support of HTML.")
        self.fields["maxParticipants"].label = _("Max. Number of participants")
        self.fields["streamingUrl"].label = _("RTMP Streaming URL")

        self.helper = FormHelper()
        self.helper.form_class = "form-horizontal"
        self.helper.label_class = "col-lg-4"
        self.helper.field_class = "col-lg-4"
        self.helper.layout = Layout(
            Field("name"),
            Field("mute_on_start"),
            Field("moderation_mode"),
            Field("meetingLayout"),
            Field("everyone_can_start"),
            Field("authenticated_user_can_start"),
            Field("disable_cam"),
            Field("disable_mic"),
            Field("disable_note"),
            Field("disable_public_chat"),
            Field("disable_private_chat"),
            Field("allow_unmuteusers"),
            Field("allow_ejectcam"),
            Field("allow_learningdashboard"),
            Field("allow_recording"),
            Field("allow_guest_entry"),
            Field("access_code"),
            Field("only_prompt_guests_for_access_code"),
            Field("guest_policy"),
            Field("welcome_message"),
            Field("logoutUrl"),
            Field("dialNumber"),
            Field("maxParticipants"),
            Field("streamingUrl", placeholder="rtmp://streaming.server.tld/url/key"),
            Submit("save", _("Save"), css_class="btn-primary"),
        )


class SchedulingStrategyForm(forms.ModelForm):
    class Meta:
        model = SchedulingStrategy
        fields = [
            "name",
            "scheduling_strategy",
            "description",
            "notifications_emails",
            "tenants",
        ]

    tenants = forms.ModelMultipleChoiceField(queryset=Site.objects.all(), required=True)

    def __init__(self, *args, **kwargs):
        super(SchedulingStrategyForm, self).__init__(*args, **kwargs)

        self.fields["name"].label = _("Name")
        self.fields["tenants"].label = _("Tenants")
        self.fields["scheduling_strategy"].label = _("Scheduling Strategy")
        self.fields["description"].label = _("Description")
        self.fields["notifications_emails"].label = _("Email Notifications")

        self.helper = FormHelper()
        self.helper.form_class = "form-horizontal"
        self.helper.label_class = "col-lg-4"
        self.helper.field_class = "col-lg-8"
        self.helper.layout = Layout(
            Field("name"),
            Field("scheduling_strategy"),
            Field("tenants"),
            Field("description"),
            Field("notifications_emails"),
            Submit("save", _("Save"), css_class="btn-primary"),
        )


class AgentConfigurationForm(forms.ModelForm):
    class Meta:
        model = AgentConfiguration
        fields = [
            "registration_interval",
            "stats_interval",
        ]

    def __init__(self, *args, **kwargs):
        super(AgentConfigurationForm, self).__init__(*args, **kwargs)
        self.fields["registration_interval"].label = _("Registration Interval")
        self.fields["stats_interval"].label = _("Stats Interval")

        self.helper = FormHelper()
        self.helper.form_class = "form-horizontal"
        self.helper.label_class = "col-lg-4"
        self.helper.field_class = "col-lg-8"
        self.helper.layout = Layout(
            Field("registration_interval"),
            Field("stats_interval"),
            Submit("save", _("Save"), css_class="btn-primary"),
        )


class ConfigureMeetingForm(forms.ModelForm):
    class Meta:
        model = MeetingConfiguration
        fields = [
            "mute_on_start",
            "moderation_mode",
            "meetingLayout",
            "allow_guest_entry",
            "access_code",
            "only_prompt_guests_for_access_code",
            "disable_mic",
            "disable_cam",
            "allow_recording",
            "disable_note",
            "disable_private_chat",
            "disable_public_chat",
            "allow_unmuteusers",
            "allow_ejectcam",
            "allow_learningdashboard",
            "guest_policy",
            "welcome_message",
            "logoutUrl",
            "dialNumber",
            "maxParticipants",
            "streamingUrl",
        ]

    def __init__(
        self,
        *args,
        enable_recordings: bool,
        enable_adminunmute: bool,
        enforced_slides: Iterable[Slides],
        initial_slides: Optional[Slides],
        selectable_slides_pks: Optional[Iterable[int]],
        general_parameter: GeneralParameter,
        **kwargs,
    ):
        self.enforced_slides = None
        self.initial_slides = None

        super(ConfigureMeetingForm, self).__init__(*args, **kwargs)

        self.fields["mute_on_start"].label = _("Everyone is muted on start")
        self.fields["moderation_mode"].label = _("Moderation mode")
        self.fields["meetingLayout"].label = _("Meeting layout")
        self.fields["allow_guest_entry"].label = _("Allow guests")
        self.fields["access_code"].label = _("Access code")
        self.fields["only_prompt_guests_for_access_code"].label = _("Only prompt guests for access code")
        self.fields["disable_cam"].label = _("Disable camera for non-moderators")
        self.fields["disable_mic"].label = _("Disable microphone for non-moderators")
        self.fields["allow_unmuteusers"].label = _("Allow Moderators to unmute Users")
        self.fields["allow_ejectcam"].label = _("Allow Moderators to eject Users-Webcams")
        self.fields["allow_learningdashboard"].label = _("Enable Learning Dashboard")
        self.fields["allow_recording"].label = _("Allow recording")
        self.fields["disable_note"].label = _("Disable shared notes - editing only for moderators")
        self.fields["disable_public_chat"].label = _("Disable public chat - editing only for moderators")
        self.fields["disable_private_chat"].label = _("Disable private chat - editing only for moderators")
        self.fields["guest_policy"].label = _("Guest policy")
        self.fields["logoutUrl"].label = _("Logout URL")
        self.fields["dialNumber"].label = _("Dial number")
        self.fields["welcome_message"].label = _("Welcome message")
        self.fields["welcome_message"].help_text = _("Max. 255 characters. Limited support of HTML.")
        self.fields["maxParticipants"].label = _("Max. Number of participants")
        self.fields["streamingUrl"].label = _("RTMP Streaming URL")

        if is_media_enabled(general_parameter) and selectable_slides_pks is not None:
            self.enforced_slides = enforced_slides
            self.initial_slides = initial_slides

            init_slides_fields(self, Q(pk__in=selectable_slides_pks))

        self.helper = FormHelper()
        self.helper.form_class = "form-horizontal"
        self.helper.label_class = "col-lg-4"
        self.helper.field_class = "col-lg-4"
        self.helper.layout = Layout(
            Field("mute_on_start"),
            Field("moderation_mode"),
            Field("meetingLayout"),
            Field("disable_cam"),
            Field("disable_mic"),
            Field("disable_note"),
            Field("disable_public_chat"),
            Field("disable_private_chat"),
            Field("allow_unmuteusers") if enable_adminunmute else None,
            Field("allow_ejectcam"),
            Field("allow_learningdashboard"),
            Field("allow_recording") if enable_recordings else None,
            Field("allow_guest_entry"),
            Field("access_code"),
            Field("only_prompt_guests_for_access_code"),
            Field("guest_policy"),
            Field("welcome_message"),
            Field("logoutUrl"),
            Field("dialNumber"),
            Field("maxParticipants"),
            Field("streamingUrl", placeholder="rtmp://streaming.server.tld/url/key"),
            *([Field("slides")] if "slides" in self.fields else []),
            *([Field("initial_slides")] if "initial_slides" in self.fields else []),
            Submit("save", _("Start"), css_class="btn-primary"),
        )

    def _post_clean(self):
        super(ConfigureMeetingForm, self)._post_clean()

        if self.initial_slides and not self.instance.initial_slides:
            self.instance.initial_slides = self.initial_slides

    def _save_m2m(self):
        super(ConfigureMeetingForm, self)._save_m2m()
        if self.enforced_slides:
            self.instance.slides.add(*self.enforced_slides)


class RoomFilterFormHelper(FormHelper):
    _form_method = "GET"
    layout = Layout(
        Fieldset(
            "",
            Div(
                Div("name", css_class="col"),
                Div("scheduling_strategy", css_class="col"),
                Div("default_meeting_configuration", css_class="col"),
                css_class="form-row",
            ),
        ),
        Submit("submit", _("Filter"), css_class="btn-primary btn-small"),
    )


class ServerFilterFormHelper(FormHelper):
    _form_method = "GET"
    layout = Layout(
        Fieldset(
            "",
            Div(
                Div("scheduling_strategy", css_class="col"),
                Div("dns", css_class="col"),
                Div("state", css_class="col"),
                css_class="form-row",
            ),
        ),
        Submit("submit", _("Filter"), css_class="btn-primary btn-small"),
    )


class SchedulingStrategyFilterFormHelper(FormHelper):
    _form_method = "GET"
    layout = Layout(
        Fieldset(
            "",
            Div(
                Div("name", css_class="col"),
                css_class="form-row",
            ),
        ),
        Submit("submit", _("Filter"), css_class="btn-primary btn-small"),
    )


class GroupFilterFormHelper(FormHelper):
    _form_method = "GET"
    layout = Layout(
        Fieldset(
            "",
            Div(
                Div("name", css_class="col"),
                css_class="form-row",
            ),
        ),
        Submit("submit", _("Filter"), css_class="btn-primary btn-small"),
    )


class GroupForm(forms.ModelForm):
    class Meta:
        model = Group
        fields = ["name"]

    def __init__(self, *args, **kwargs):
        super(GroupForm, self).__init__(*args, **kwargs)

        self.fields["name"].label = _("Name")

        self.helper = FormHelper()
        self.helper.form_class = "form-horizontal"
        self.helper.label_class = "col-lg-4"
        self.helper.field_class = "col-lg-8"
        self.helper.layout = Layout(
            Field("name"),
            Submit("save", _("Save"), css_class="btn-primary"),
        )


class RecordingForm(forms.ModelForm):
    class Meta:
        model = Meeting
        fields = ["replay_title"]

    def __init__(self, *args, **kwargs):
        super(RecordingForm, self).__init__(*args, **kwargs)

        self.fields["replay_title"].label = _("Title")
        self.fields["replay_title"].required = True

        self.helper = FormHelper()
        self.helper.form_class = "form-horizontal"
        self.helper.label_class = "col-lg-4"
        self.helper.field_class = "col-lg-8"
        self.helper.layout = Layout(
            Field("replay_title"),
            Submit("save", _("Save"), css_class="btn-primary"),
        )


class UserSettings(forms.ModelForm):
    class Meta:
        model = User
        fields = [
            "theme",
            "bbb_auto_join_audio",
            "bbb_listen_only_mode",
            "bbb_skip_check_audio",
            "bbb_skip_check_audio_on_first_join",
            "bbb_auto_share_webcam",
            "bbb_record_video",
            "bbb_skip_video_preview",
            "bbb_skip_video_preview_on_first_join",
            "bbb_mirror_own_webcam",
            "bbb_force_restore_presentation_on_new_events",
            "bbb_auto_swap_layout",
            "bbb_show_participants_on_login",
            "bbb_show_public_chat_on_login",
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.fields["theme"].label = _("Change Theme")
        self.fields["theme"].empty_label = None
        self.fields["bbb_auto_join_audio"].label = _("Auto join audio")
        self.fields["bbb_listen_only_mode"].label = _("Listen only mode")
        self.fields["bbb_skip_check_audio"].label = _("Skip check audio")
        self.fields["bbb_skip_check_audio_on_first_join"].label = _("Skip check audio on first join")
        self.fields["bbb_auto_share_webcam"].label = _("Auto share webcam")
        self.fields["bbb_record_video"].label = _("Record video")
        self.fields["bbb_skip_video_preview"].label = _("Skip video preview")
        self.fields["bbb_skip_video_preview_on_first_join"].label = _("Skip video preview on first join")
        self.fields["bbb_mirror_own_webcam"].label = _("Mirror own webcam")
        self.fields["bbb_force_restore_presentation_on_new_events"].label = _(
            "Force restore presentation on new events"
        )
        self.fields["bbb_auto_swap_layout"].label = _("Auto swap layout")
        self.fields["bbb_show_participants_on_login"].label = _("Show participants on first join")
        self.fields["bbb_show_public_chat_on_login"].label = _("Show public chat on first join")

        self.helper = FormHelper()
        self.helper.form_class = "form-horizontal"
        self.helper.label_class = "col-lg-4"
        self.helper.field_class = "col-lg-8"
        self.helper.layout = Layout(
            Field("theme"),
            Field("bbb_auto_join_audio"),
            Field("bbb_listen_only_mode"),
            Field("bbb_skip_check_audio"),
            Field("bbb_skip_check_audio_on_first_join"),
            Field("bbb_auto_share_webcam"),
            Field("bbb_record_video"),
            Field("bbb_skip_video_preview"),
            Field("bbb_skip_video_preview_on_first_join"),
            Field("bbb_mirror_own_webcam"),
            Field("bbb_force_restore_presentation_on_new_events"),
            Field("bbb_auto_swap_layout"),
            Field("bbb_show_participants_on_login"),
            Field("bbb_show_public_chat_on_login"),
            Submit("save", _("Save"), css_class="btn-primary"),
        )


class UserForm(UserSettings):
    class Meta(UserSettings.Meta):
        fields = [
            "username",
            "first_name",
            "last_name",
            "email",
            "groups",
            "is_staff",
            "personal_rooms_max_number",
            *UserSettings.Meta.fields,
        ]

    groups = forms.ModelMultipleChoiceField(queryset=Group.objects.all(), required=False)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.fields["username"].label = _("Username")
        # only display username if it has no usable password (e.i. the user is from OIDC)
        if not self.instance.has_usable_password():
            self.fields["username"].disabled = True
            self.fields["username"].readonly = True
            self.fields["username"].validators = []
            self.fields["username"].help_text = ""
        self.fields["first_name"].label = _("First name")
        self.fields["last_name"].label = _("Last name")
        self.fields["groups"].label = _("Groups")
        self.fields["is_staff"].label = _("Staff status")
        self.fields["email"].label = _("Email")
        self.fields["personal_rooms_max_number"].label = _("Maximum number of personal rooms")

        self.helper.layout.fields = [
            Field("username"),
            Field("first_name"),
            Field("last_name"),
            Field("email"),
            Field("groups"),
            Field("is_staff"),
            Field("personal_rooms_max_number"),
            *self.helper.layout.fields,
        ]

    def _get_validation_exclusions(self):
        exclude = super()._get_validation_exclusions()

        # exclude the username validation if it has no usable password (e.i. the user is from OIDC)
        if not self.instance.has_usable_password():
            exclude.append("username")

        return exclude


class UserFormWithPassword(UserForm):
    class Meta(UserForm.Meta):
        fields = [
            "password",
            *UserForm.Meta.fields,
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.fields["password"].label = _("Password")

        self.helper.layout.fields.insert(1, Field("password"))


class UserResetPasswordAdminForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ["username"]

    password = forms.CharField(min_length=8, help_text=_("Minimum password length 8 character"))
    password_confirm = forms.CharField(min_length=8, help_text=_("Confirm password so they match"))

    def __init__(self, *args, **kwargs):
        super(UserResetPasswordAdminForm, self).__init__(*args, **kwargs)

        self.fields["username"].label = _("Username")
        self.fields["password"].label = _("New password")
        self.fields["password_confirm"].label = _("New password confirm")

        self.helper = FormHelper()
        self.helper.form_class = "form-horizontal"
        self.helper.label_class = "col-lg-4"
        self.helper.field_class = "col-lg-8"
        self.helper.layout = Layout(
            Field("username", readonly=True, diabled=True),
            Field("password"),
            Field("password_confirm"),
            Submit("save", _("Save"), css_class="btn-primary"),
        )


class PasswordChangeCrispyForm(PasswordChangeForm):
    def __init__(self, *args, **kwargs):
        super(PasswordChangeCrispyForm, self).__init__(*args, **kwargs)
        self.fields["old_password"].label = _("Old password")
        self.fields["new_password1"].label = _("New password")
        self.fields["new_password2"].label = _("Repeat new password")

        self.helper = FormHelper()
        self.helper.form_class = "form-horizontal"
        self.helper.label_class = "col-lg-4"
        self.helper.field_class = "col-lg-8"
        self.helper.layout = Layout(
            Field("old_password"),
            Field("new_password1"),
            Field("new_password2"),
            Submit("save", _("Save"), css_class="btn-primary"),
        )


class TenantForm(forms.ModelForm):
    class Meta:
        model = Site
        fields = ["domain", "name"]

    def __init__(self, *args, **kwargs):
        super(TenantForm, self).__init__(*args, **kwargs)

        self.fields["domain"].label = _("Domain")
        self.fields["name"].label = _("Name")

        self.helper = FormHelper()
        self.helper.form_class = "form-horizontal"
        self.helper.label_class = "col-lg-4"
        self.helper.field_class = "col-lg-8"
        self.helper.layout = Layout(
            Field("name"),
            Field("domain"),
            Submit("save", _("Save"), css_class="btn-primary"),
        )


class ApiTokenForm(forms.ModelForm):
    class Meta:
        model = ApiToken
        fields = ["name", "tenants", "scheduling_strategy"]

    tenants = forms.ModelMultipleChoiceField(queryset=Site.objects.all(), required=True)

    def __init__(self, *args, general_parameter: GeneralParameter, **kwargs):
        super(ApiTokenForm, self).__init__(*args, **kwargs)
        self.fields["name"].label = _("Name")
        self.fields["tenants"].label = _("Tenants")
        self.fields["scheduling_strategy"].label = _("Scheduling Strategy")
        scheduling_strategy_filter = Q(tenants__in=[general_parameter.tenant])
        if self.instance.scheduling_strategy_id:
            scheduling_strategy_filter |= Q(id=self.instance.scheduling_strategy_id)
        self.fields["scheduling_strategy"].queryset = SchedulingStrategy.objects.filter(
            scheduling_strategy_filter
        ).distinct()

        self.helper = FormHelper()
        self.helper.form_class = "form-horizontal"
        self.helper.label_class = "col-lg-4"
        self.helper.field_class = "col-lg-8"
        self.helper.layout = Layout(
            Field("name"),
            Field("tenants"),
            Field("scheduling_strategy"),
            Submit("save", _("Save"), css_class="btn-primary"),
        )


class FileMaxSizeValidator:
    message = _("Ensure this file does not exceed the size of %(max_size)s (it has %(size)s).")
    code = "max_size"
    max_size = 10

    def __init__(self, max_size=None, message=None, code=None):
        self.max_size = max_size
        if message is not None:
            self.message = message
        if code is not None:
            self.code = code

    def __call__(self, value):
        if self.max_size is not None and value.size > self.max_size:
            raise ValidationError(
                self.message,
                code=self.code,
                params={
                    "size": str(IntRepresentedAsByteSize(value.size)),
                    "max_size": str(IntRepresentedAsByteSize(self.max_size)),
                    "value": value,
                },
            )


class SlidesForm(forms.ModelForm):
    class Meta:
        model = Slides
        fields = [
            "name",
            "file",
        ]

    def __init__(self, *args, tenant: Site, **kwargs):
        super().__init__(*args, **kwargs)

        self.tenant = tenant
        self.initial_file_path = self.instance.file.path if self.instance.file else None

        self.fields["name"].label = _("Name")
        allowed_extensions = ["doc", "docx", "jpeg", "jpg", "pdf", "png", "pptx", "txt"]

        self.fields["file"].label = _("File")
        self.fields["file"].widget.attrs["accept"] = ",".join(
            map(lambda extension: f".{extension}", allowed_extensions)
        )
        self.fields["file"].validators.append(FileExtensionValidator(allowed_extensions=allowed_extensions))
        self.fields["file"].validators.append(FileMaxSizeValidator(tenant.generalparameter.slides_max_upload_size))

        self.helper = FormHelper()
        self.helper.form_id = "id-slides-form"
        self.helper.form_class = "form-horizontal"
        self.helper.label_class = "col-lg-4"
        self.helper.field_class = "col-lg-8"
        self.helper.layout = Layout(
            Field("name"),
            Field("file", template="clearable_file_input.html"),
            Submit("save", _("Save"), css_class="btn-primary"),
        )

    def _post_clean(self):
        super()._post_clean()

        self.instance.tenant = self.tenant

    def save(self, commit=True):
        super().save(commit)

        if commit and self.initial_file_path and "file" in self.changed_data:
            mark_slides_file_for_removal(path=self.initial_file_path)

        return self.instance


class SlidesAdminForm(SlidesForm):
    class Meta(SlidesForm.Meta):
        fields = [
            "owner",
            *SlidesForm.Meta.fields,
        ]

    def __init__(self, *args, tenant, **kwargs):
        super().__init__(*args, tenant=tenant, **kwargs)

        self.fields["owner"].label = _("Owner")

        self.helper.layout.fields.insert(0, Field("owner"))

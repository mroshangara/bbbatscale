import django_filters
from core.models import MeetingConfigurationTemplate, Room, SchedulingStrategy, Server
from django.contrib.sites.shortcuts import get_current_site
from django.db.models.query_utils import Q
from django.utils.translation import gettext_lazy as _


def name_search(queryset, name, value):
    q = queryset.filter(Q(name__icontains=value))
    return q


def dns_search(queryset, name, value):
    q = queryset.filter(Q(dns__icontains=value) | Q(datacenter__icontains=value))
    return q


def user_search(queryset, name, value):
    q = queryset.filter(Q(username__icontains=value) | Q(first_name__icontains=value) | Q(last_name__icontains=value))
    return q


def tenant_scheduling_strategy(request):
    if request is None:
        return SchedulingStrategy.objects.none()
    return SchedulingStrategy.objects.filter(tenants__in=[get_current_site(request)])


def tenant_default_meeting_configuration(request):
    if request is None:
        return MeetingConfigurationTemplate.objects.none()
    return MeetingConfigurationTemplate.objects.filter(Q(tenants__in=[get_current_site(request)]) | Q(name="Default"))


class RoomFilter(django_filters.FilterSet):
    name = django_filters.CharFilter(method=name_search, label=_("Name"))
    scheduling_strategy = django_filters.ModelChoiceFilter(queryset=tenant_scheduling_strategy)
    default_meeting_configuration = django_filters.ModelChoiceFilter(queryset=tenant_default_meeting_configuration)

    class Meta:
        model = Room
        fields = ("scheduling_strategy", "default_meeting_configuration")


class ServerFilter(django_filters.FilterSet):
    dns = django_filters.CharFilter(method=dns_search, label=_("DNS|Datacenter"))
    scheduling_strategy = django_filters.ModelChoiceFilter(queryset=tenant_scheduling_strategy)

    class Meta:
        model = Server
        fields = (
            "scheduling_strategy",
            "state",
        )


class SchedulingStrategyFilter(django_filters.FilterSet):
    name = django_filters.CharFilter(method=name_search, label=_("Name"))


class UserFilter(django_filters.FilterSet):
    name = django_filters.CharFilter(method=user_search, label=_("Name"))


class GroupFilter(django_filters.FilterSet):
    name = django_filters.CharFilter(method=name_search, label=_("Name"))

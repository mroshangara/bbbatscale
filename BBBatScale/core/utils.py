from __future__ import annotations

import itertools
import logging
import re
from functools import reduce
from hashlib import sha1
from typing import TYPE_CHECKING, Callable, Dict, Iterable, Iterator, Optional, Tuple, TypeVar, Union
from urllib.parse import urlencode

import requests
import xmltodict
from django.conf import settings
from django.contrib.auth import REDIRECT_FIELD_NAME
from django.contrib.auth.models import AbstractUser, AnonymousUser, Permission
from django.contrib.sites.models import Site
from django.core.paginator import Paginator
from django.db.models import Q, QuerySet
from django.http import JsonResponse
from django.urls import reverse
from django.utils.formats import get_format, number_format, sanitize_separators
from google.protobuf.field_mask_pb2 import FieldMask
from grpc import secure_channel, ssl_channel_credentials
from recordings.v1.recordings_pb2 import (
    DeleteRecordingRequest,
    GetRecordingRequest,
    ListRecordingsRequest,
    ListRecordingsResponse,
    Recording,
    UpdateRecordingRequest,
)
from recordings.v1.recordings_pb2_grpc import RecordingsStub

if TYPE_CHECKING:
    from core.models import GeneralParameter

logger = logging.getLogger(__name__)

T = TypeVar("T")


class BigBlueButton:
    def __init__(self, bbb_server_url, bbb_server_salt):
        if not bbb_server_url.startswith("https://"):
            if bbb_server_url.startswith("http://"):
                bbb_server_url = bbb_server_url[:4] + "s" + bbb_server_url[4:]
            else:
                bbb_server_url = "https://{}".format(bbb_server_url)
        if not bbb_server_url.endswith("/bigbluebutton/api/"):
            bbb_server_url = "{}/bigbluebutton/api/".format(bbb_server_url)

        self.__url = bbb_server_url
        self.__salt = bbb_server_salt
        logger.debug(
            "BigBlueButton API Connector object initializes with Values url=%s; salt=%s", self.__url, self.__salt
        )

    def get_api_call_url(self, api_method, api_params):
        checksum = self.create_salt(api_method, api_params)
        if api_params:
            api_params = "{}&".format(api_params)
        api_call_url = "{}{}?{}checksum={}".format(self.__url, api_method, api_params, checksum)

        logger.debug("api_call_url=%s created", api_call_url)
        return api_call_url

    # BBB get meetings
    def get_meetings(self):
        api_call = self.get_api_call_url("getMeetings", "")
        response = self.get_post_request(api_call)

        logger.debug("Execute API call %s; response=%s", api_call, response)
        return response

    @staticmethod
    def validate_get_meetings(response):
        meetings = []
        data = xmltodict.parse(response.text)
        logger.debug("Validate response of getMeeting API call (response=%s)", data)

        if data["response"]["returncode"] != "SUCCESS":
            logger.error("Error while make api call getMeetings. data['response']['returncode'] != 'SUCCESS'")
            raise Exception("API Call failed")

        try:
            if data["response"]["meetings"] is not None:
                if type(data["response"]["meetings"]["meeting"]) == list:
                    meetings = data["response"]["meetings"]["meeting"]
                else:
                    meetings.append(data["response"]["meetings"]["meeting"])
        except Exception as e:
            logger.error("Error while validating API call for 'getMeetings' %s", e)
            return []
        logger.debug("Validate response of getMeeting API call completed successfully")

        return meetings

    # BBB api
    def api(self):
        api_call = self.get_api_call_url("", "")
        response = self.get_post_request(api_call)

        logger.debug("Execute API call %s; response=%s", api_call, response)
        return response

    @staticmethod
    def validate_api(response):
        data = xmltodict.parse(response.text)
        logger.debug("Validate response of getMeeting API call (response=%s)", data)

        if data["response"]["returncode"] != "SUCCESS":
            logger.error("Error while make api call api. data['response']['returncode'] != 'SUCCESS'")
            raise Exception("API Call failed")

        try:
            api_version = data["response"].get("apiVersion", None)
        except Exception as e:
            logger.error("Error while validating API call for 'api' %s", e)
            return None
        logger.debug("Validate response of api API call completed successfully")

        return api_version

    # BBB create
    def create(self, params, slides: Iterator[Tuple[str, str]] = None, body=None):
        api_call = self.get_api_call_url("create", self.create_params(params))

        # Unpack first element but not all to improve performance while checking if slides have been passed.
        first_slides = next(slides, None)

        if first_slides:
            slides = itertools.chain([first_slides], slides)
            document_dicts = [{"@url": url, "@filename": filename} for url, filename in slides]

            parsed_body = xmltodict.parse(body, force_list=("module", "document")) if body else dict()
            if "modules" not in parsed_body:
                parsed_body["modules"] = {"module": [{"@name": "presentation", "document": document_dicts}]}
            elif "module" not in parsed_body["modules"]:
                parsed_body["modules"]["module"] = [{"@name": "presentation", "document": document_dicts}]
            elif "presentation" in map(lambda module: module["@name"], parsed_body["modules"]["module"]):
                parsed_body["modules"]["module"].append({"@name": "presentation", "document": document_dicts})
            elif "document" not in next(
                filter(lambda module: module["@name"] == "presentation", parsed_body["modules"]["module"])
            ):
                next(filter(lambda module: module["@name"] == "presentation", parsed_body["modules"]["module"]))[
                    "document"
                ] = document_dicts
            else:
                presentation_module = next(
                    filter(lambda module: module["@name"] == "presentation", parsed_body["modules"]["module"])
                )

                presentation_module["document"] = document_dicts + list(presentation_module["document"])

            body = xmltodict.unparse(parsed_body)

        # hotfix for creat meeting, since initial create of fresh bbb server takes > 2 sec
        response = self.get_post_request(api_call, body, 5)

        logger.debug("Execute 'create' API call %s; response=%s", api_call, response)

        return response

    @staticmethod
    def validate_create(response):
        data = xmltodict.parse(response.text)
        try:
            result = data["response"]["returncode"] == "SUCCESS"
            logger.debug(f"Validating 'create' API call; result={result}")
            return result
        except KeyError:
            logger.error("create did not respond with a valid XML.")
            return False

    # BBB join
    def join(self, params):
        api_call = self.get_api_call_url("join", self.create_params(params))
        logger.debug("Create 'join' API call=%s", api_call)

        return api_call

    # BBB end
    def end(self, meeting_id, pw):
        api_call = self.get_api_call_url("end", self.create_params({"meetingID": meeting_id, "password": pw}))
        response = self.get_post_request(api_call)
        logger.debug("Execute 'end' API call=%s; response=%s", api_call, response)

        return response

    @staticmethod
    def validate_end(response):
        data = xmltodict.parse(response.text)
        try:
            result = data["response"]["returncode"] == "SUCCESS"
            logger.debug(f"Validating 'end' API call; result={result}")
            return result
        except KeyError:
            logger.error("end did not respond with a valid XML.")
            return False

    # BBB is meeting running
    def is_meeting_running(self, meeting_id):
        api_call = self.get_api_call_url("isMeetingRunning", self.create_params({"meetingID": meeting_id}))
        response = self.get_post_request(api_call)
        logger.debug("Execute 'isMeetingRunning' API call=%s; response=%s", api_call, response)
        return response

    @staticmethod
    def validate_is_meeting_running(response):
        data = xmltodict.parse(response.text)
        try:
            result = data["response"]["running"] == "true"
            logger.debug(f"Validating 'isMeetingRunning' API call; result={result}")
            return result
        except KeyError:
            logger.error("isMeetingRunning did not respond with a valid XML.")
            return False

    # BBB get meeting info
    def get_meeting_infos(self, meeting_id):
        api_call = self.get_api_call_url("getMeetingInfo", self.create_params({"meetingID": meeting_id}))
        response = self.get_post_request(api_call)

        logger.debug("Execute 'getMeetingInfo' API call=%s; response=%s", api_call, response)
        return response

    @staticmethod
    def validate_get_meeting_infos(response):
        data = xmltodict.parse(response.text)
        logger.debug("Validate getMeetingInfo result=%s", data)
        try:
            result = data["response"]["returncode"] == "SUCCESS"
            logger.debug(f"Validating 'getMeetingInfo' API call; result={result}")
            return data if result else None
        except KeyError:
            logger.error("getMeetingInfos did not respond with a valid XML.")
            return None

    # BBB create web hook
    def create_web_hook(self, params):
        api_call = self.get_api_call_url("hooks/create", self.create_params(params))
        response = self.get_post_request(api_call)
        logger.debug("Execute 'hooks/create' API call=%s; response=%s", api_call, response)
        return response

    @staticmethod
    def validate_create_web_hook(response):
        data = xmltodict.parse(response.text)
        try:
            result = data["response"]["returncode"] == "SUCCESS"
            logger.debug(f"Validating 'hooks/create' API call; result={result}")
            return result
        except KeyError:
            logger.error("Webhook hooks/create did not respond with a valid XML.")
            return False

    # BBB get recordings
    def get_recordings(self, params):
        api_call = self.get_api_call_url("getRecordings", self.create_params(params))
        response = self.get_post_request(api_call, timeout=60)
        logger.debug("Execute 'getRecordings' API call=%s; response=%s", api_call, response)
        return response

    @staticmethod
    def validate_get_recordings(response):
        data = xmltodict.parse(response.text)
        try:
            result = data["response"]["returncode"] == "SUCCESS"
            logger.debug(f"Validating 'getRecordings' API call; result={result}")
            return data if result else None
        except KeyError:
            logger.error("getRecordings did not respond with a valid XML.")
            return None

    # BBB publish recordings
    def publish_recordings(self, params):
        api_call = self.get_api_call_url("publishRecordings", self.create_params(params))
        response = self.get_post_request(api_call)
        logger.debug("Execute 'publishRecordings' API call=%s; response=%s", api_call, response)
        return response

    @staticmethod
    def validate_publish_recordings(response):
        data = xmltodict.parse(response.text)
        logger.debug("Validate publishRecordings result=%s", data)
        try:
            result = data["response"]["published"] == "true"
            logger.debug(f"Validating 'publishRecordings' API call; result={result}")
            return result
        except KeyError:
            logger.error("publishRecordings did not respond with a valid XML.")
            return False

    # BBB delete recording
    def delete_recordings(self, params):
        api_call = self.get_api_call_url("deleteRecordings", self.create_params(params))
        response = self.get_post_request(api_call)
        logger.debug("Execute 'deleteRecordings' API call=%s; response=%s", api_call, response)
        return response

    @staticmethod
    def validate_delete_recordings(response):
        data = xmltodict.parse(response.text)
        logger.debug("Validate deleteRecordings result=%s", data)
        try:
            result = data["response"]["deleted"] == "true"
            logger.debug(f"Validating 'deleteRecordings' API call; result={result}")
            return result
        except KeyError:
            logger.error("deleteRecordings did not respond with a valid XML.")
            return False

    # BBB update recording
    def update_recordings(self, params):
        api_call = self.get_api_call_url("updateRecordings", self.create_params(params))
        response = self.get_post_request(api_call)
        logger.debug("Execute 'updateRecordings' API call=%s; response=%s", api_call, response)
        return response

    @staticmethod
    def validate_update_recordings(response):
        data = xmltodict.parse(response.text)
        logger.debug("Validate publishRecordings result=%s", data)
        try:
            result = data["response"]["updated"] == "true"
            logger.debug(f"Validating 'publishRecordings' API call; result={result}")
            return result
        except KeyError:
            logger.error("updateRecordings did not respond with a valid XML.")
            return False

    # BBB API utils
    def create_salt(self, api_method, api_params):
        return sha1("{}{}{}".format(api_method, api_params, self.__salt).encode("utf-8")).hexdigest()

    @staticmethod
    def create_params(params):
        return urlencode(params)

    @staticmethod
    def get_post_request(uri, body=None, timeout=2):
        try:
            if body:
                return requests.post(uri, data=body, timeout=timeout)
            return requests.get(uri, timeout=timeout)
        except (ConnectionError, TimeoutError) as e:
            logger.error(f"API Call '{uri}' failed. " + str(e))
            raise e

    @property
    def url(self):
        return self.__url

    @staticmethod
    def meeting_has_origin(meeting: Dict, origin: str) -> bool:
        return meeting.get("metadata", {}).get("bbb-origin", "") == origin

    @staticmethod
    def meeting_is_breakout(meeting: Dict) -> bool:
        return meeting.get("isBreakout", "") == "true"


class RecordingsGRPC:
    def __init__(self, url: str, key: str, cert: str):
        self.__creds = ssl_channel_credentials(None, key.encode(), cert.encode())
        self.__channel = secure_channel(url, self.__creds)
        self.rpc = RecordingsStub(self.__channel)
        logger.debug(f"Recordings GRPC channel initialized for url={url}")

    def __del__(self):
        logger.debug("Recordings GRPC channel closed.")
        self.__channel.close()

    def delete_recording(self, recording_id: str):
        logger.debug(f"DeleteRecordingRequest for {recording_id}")
        self.rpc.DeleteRecording(DeleteRecordingRequest(name=recording_id))

    def get_recording(self, recording_id: str):
        logger.debug(f"GetRecordingRequest for {recording_id}")
        response: Recording = self.rpc.GetRecording(GetRecordingRequest(name=recording_id))
        return response

    def update_recording(self, recording_id: str, metadata: bytes):
        logger.debug(f"UpdateRecording for {recording_id}")
        recording: Recording = self.rpc.GetRecording(GetRecordingRequest(name=recording_id))
        recording.metadata = metadata
        mask: FieldMask = FieldMask()
        mask.paths.append("metadata")
        response: Recording = self.rpc.UpdateRecording(UpdateRecordingRequest(recording=recording, update_mask=mask))
        return response

    def list_recordings(self):
        response: ListRecordingsResponse = self.rpc.ListRecordings(
            ListRecordingsRequest(parent="1", page_size=25, page_token="1")
        )
        return response


def meeting_recording_xml(replay_url):
    logger.debug(f"requesting xml data for recording with replay url: {replay_url}")
    return requests.get(f"{replay_url}/metadata.xml", timeout=2)


def check_tenant_permissions(user, tenant) -> bool:
    return user.is_superuser or user.tenant == tenant


def get_permission(perm: str) -> Permission:
    app_label, _, codename = perm.partition(".")
    return Permission.objects.get(content_type__app_label=app_label, codename=codename)


def get_permissions(*perms: str) -> QuerySet:
    prepared_permissions = dict()

    for perm in perms:
        app_label, _, codename = perm.partition(".")
        if app_label not in prepared_permissions:
            prepared_permissions[app_label] = list()
        prepared_permissions[app_label].append(codename)

    return Permission.objects.filter(
        reduce(
            Q.__or__,
            map(lambda item: Q(content_type__app_label=item[0], codename__in=item[1]), prepared_permissions.items()),
        )
    )


def has_tenant_based_perm(
    user: Union[AnonymousUser, AbstractUser], perm: str, tenant: Site, *, elevate_staff: bool = True
) -> bool:
    return hasattr(user, "has_tenant_based_perm") and user.has_tenant_based_perm(
        perm, tenant, elevate_staff=elevate_staff
    )


def has_tenant_based_perms(
    user: Union[AnonymousUser, AbstractUser], perms: Iterable[str], tenant: Site, *, elevate_staff: bool = True
) -> bool:
    return hasattr(user, "has_tenant_based_perms") and user.has_tenant_based_perms(
        perms, tenant, elevate_staff=elevate_staff
    )


def format_human_readable_size(
    size: int, binary: bool = False, suffix: str = "B", decimal_pos: Optional[int] = 1
) -> str:
    base = 1024 if binary else 1000

    if size < base:
        return f"{number_format(size, 0)}{suffix}"
    else:
        size /= base

    for unit in ["k", "M", "G", "T", "P", "E", "Z"]:
        if abs(size) < base:
            return f"{number_format(size, decimal_pos)}{unit}{'i' if binary else ''}{suffix}"
        size /= base

    return f"{number_format(size, decimal_pos)}Y{'i' if binary else ''}{suffix}"


def parse_human_readable_size(size: str, suffix: str = "B") -> int:
    ts = re.escape(get_format("THOUSAND_SEPARATOR"))
    ds = re.escape(get_format("DECIMAL_SEPARATOR"))
    singed_number_regex = f"[+-]?(?:\\d+|\\d{{1,3}}(?:{ts}\\d{{3}})+)(?:{ds}\\d+)?"

    match = re.fullmatch(
        f"^(?P<signed_number>{singed_number_regex})(?:(?P<suffix>[kMGTPEZY])(?P<binary>i?))?{suffix}$",
        size,
    )

    if not match:
        raise ValueError("The supplied string does not comply to the parsable format")

    value, _, decimal = sanitize_separators(match.group("signed_number")).partition(".")
    number = int(value + decimal)
    base = 1024 if match.group("binary") == "i" else 1000
    suffix_power = [None, "k", "M", "G", "T", "P", "E", "Z", "Y"].index(match.group("suffix"))

    return (number * (base**suffix_power)) // (10 ** len(decimal))


def is_media_enabled(general_parameter: GeneralParameter) -> bool:
    return settings.BBBATSCALE_MEDIA_ENABLED and general_parameter.media_enabled


def paginate(query: QuerySet[T], page: int, map_entry: Callable[[T], dict]) -> JsonResponse:
    paginator = Paginator(query, 50)

    page_number = max(1, min(paginator.num_pages, page))

    return JsonResponse(
        {
            "page": page_number,
            "pageCount": paginator.num_pages,
            "totalCount": query.count(),
            "entries": [map_entry(entry) for entry in paginator.page(page_number).object_list],
        }
    )


def generate_login_url(
    view_name: str, *args, login_url: str = settings.LOGIN_URL, redirect_parameter: str = REDIRECT_FIELD_NAME
) -> str:
    return login_url + "?" + redirect_parameter + "=" + reverse(view_name, args=args)

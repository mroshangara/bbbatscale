from datetime import datetime, timezone
from typing import AsyncIterator

import pytest
from channels.db import database_sync_to_async
from core import constants
from core.constants import MEETING_STATE_RUNNING
from core.models import (
    GeneralParameter,
    HomeRoom,
    Meeting,
    MeetingConfiguration,
    MeetingConfigurationTemplate,
    PersonalRoom,
    Room,
    SchedulingStrategy,
    Server,
    Slides,
    Theme,
    User,
)
from django.conf import settings
from django.contrib.auth.models import Group
from django.contrib.sites.models import Site
from faker import Faker


@pytest.fixture(scope="function")
def testserver_tenant(db) -> Site:
    return Site.objects.create(name="testserver", domain="testserver")


@pytest.fixture(scope="function")
def testserver2_tenant(db) -> Site:
    return Site.objects.create(domain="testserver2", name="testserver2")


@pytest.mark.asyncio
@pytest.fixture(scope="function")
async def async_example_tenant(db) -> AsyncIterator[Site]:
    tenant = await database_sync_to_async(Site.objects.create)(name="example.org", domain="example.org")
    yield tenant
    await database_sync_to_async(tenant.delete)()


@pytest.mark.asyncio
@pytest.fixture(scope="function")
async def async_other_example_tenant(db) -> AsyncIterator[Site]:
    tenant = await database_sync_to_async(Site.objects.create)(name="other.example.org", domain="other.example.org")
    yield tenant
    await database_sync_to_async(tenant.delete)()


@pytest.fixture(scope="function")
def gp_test_tenant(
    db, testserver_tenant, example_scheduling_strategy, example_meeting_configuration_template
) -> GeneralParameter:
    return GeneralParameter.objects.create(
        tenant=testserver_tenant,
        default_theme_id=1,
        home_room_enabled=True,
        home_room_scheduling_strategy=example_scheduling_strategy,
        home_room_room_configuration=example_meeting_configuration_template,
        recording_key="asdf",
        recording_cert="asdf",
        recording_management_url="asdf",
    )


@pytest.fixture(scope="function")
def example_scheduling_strategy(db, testserver_tenant) -> SchedulingStrategy:
    scheduling_strategy = SchedulingStrategy.objects.create(name="example")
    scheduling_strategy.tenants.set([testserver_tenant])
    return scheduling_strategy


@pytest.fixture(scope="function")
def example_meeting_configuration_template(db, testserver_tenant) -> MeetingConfigurationTemplate:
    meeting_configuration_template = MeetingConfigurationTemplate.objects.create(name="Example Config")
    meeting_configuration_template.tenants.set([testserver_tenant])
    return meeting_configuration_template


@pytest.fixture(scope="function")
def example_superuser(db, testserver_tenant) -> User:
    return User.objects.create_superuser(
        "example-admin", "example-admin@example.org", "EXAMPLEPASSWORD", tenant=testserver_tenant
    )


@pytest.fixture(scope="function")
def example_staff_user(db, testserver_tenant) -> User:
    return User.objects.create_user(
        "example-staff", "example-staff@example.org", "EXAMPLEPASSWORD", tenant=testserver_tenant, is_staff=True
    )


@pytest.fixture(scope="function")
def example_moderator_user(db, testserver_tenant, moderator_group) -> User:
    example_moderator_user = User.objects.create_user(
        "example-staff", "example-staff@example.org", "EXAMPLEPASSWORD", tenant=testserver_tenant, is_staff=True
    )
    example_moderator_user.groups.add(moderator_group)
    example_moderator_user.save()
    return example_moderator_user


@pytest.fixture(scope="function")
def example_user(db, testserver_tenant) -> User:
    return User.objects.create_user(
        "example-user",
        "example-user@example.org",
        "EXAMPLEPASSWORD",
        tenant=testserver_tenant,
        is_staff=False,
        first_name="Example",
        last_name="User",
        display_name="example_user",
    )


@pytest.fixture(scope="function")
def example_server(db, example_scheduling_strategy) -> Server:
    return Server.objects.create(
        scheduling_strategy=example_scheduling_strategy,
        dns="example.org",
        shared_secret="123456789",
        state=constants.SERVER_STATE_UP,
    )


@pytest.fixture(scope="function")
def example_room(
    db, example_scheduling_strategy, example_server, testserver_tenant, example_meeting_configuration_template
) -> Room:
    room = Room.objects.create(
        scheduling_strategy=example_scheduling_strategy,
        name="room",
        default_meeting_configuration=example_meeting_configuration_template,
    )
    room.tenants.set([testserver_tenant])
    return room


@pytest.fixture(scope="function")
def example_personal_room(
    db, example_superuser, example_scheduling_strategy, testserver_tenant, example_meeting_configuration_template
) -> PersonalRoom:
    p_room = PersonalRoom.objects.create(
        name="Example PersonalRoom",
        owner=example_superuser,
        scheduling_strategy=example_scheduling_strategy,
        default_meeting_configuration=example_meeting_configuration_template,
    )
    p_room.tenants.set([testserver_tenant])
    return p_room


@pytest.fixture(scope="function")
def example_homeroom(db, example_moderator_user) -> HomeRoom:
    return example_moderator_user.homeroom


@pytest.fixture(scope="function")
def example_meeting(example_room, example_server):
    Faker.seed(1337)
    return Meeting.objects.create(
        room=example_room,
        room_name=example_room.name,
        server=example_server,
        id=Faker().uuid4(),
        state=MEETING_STATE_RUNNING,
        attendee_pw="test_attendee_password",
        moderator_pw="test_moderator_password",
        configuration=MeetingConfiguration.objects.create(name="Example Config"),
    )


@pytest.fixture
def current_timestamp():
    return datetime(2022, 1, 2, 3, 4, 5, tzinfo=timezone.utc)


@pytest.fixture(scope="function")
def example_group(db) -> Group:
    return Group.objects.create(name="Example group")


@pytest.fixture
def example_slide(testserver_tenant):
    return Slides.objects.create(name="example_slide", tenant=testserver_tenant)


@pytest.fixture
def example_theme() -> Theme:
    return Theme.objects.create(
        name="example_theme",
    )


@pytest.fixture
def moderator_group(db: object) -> object:
    return Group.objects.create(name=settings.MODERATORS_GROUP)

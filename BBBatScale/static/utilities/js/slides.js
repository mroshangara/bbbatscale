/**
 * @param {HTMLSelectElement} slidesElement
 * @param {HTMLInputElement} defaultSlidesElement
 */
function initSlidesInput(slidesElement, defaultSlidesElement) {
    const selectableSearch = document.createElement("input");
    selectableSearch.type = "text";
    selectableSearch.classList.add("form-control", "w-100", "mb-1");
    selectableSearch.autocomplete = "off";
    selectableSearch.placeholder = gettext("Search");

    const selectionSearch = document.createElement("input");
    selectionSearch.type = "text";
    selectionSearch.classList.add("form-control", "w-100", "mb-1");
    selectionSearch.autocomplete = "off";
    selectionSearch.placeholder = gettext("Search");

    const selectableLabel = document.createElement("div");
    selectableLabel.classList.add("w-100", "text-center", "mt-1");
    selectableLabel.textContent = gettext("Selectable");

    const selectionLabel = document.createElement("div");
    selectionLabel.classList.add("w-100", "text-center", "mt-1");
    selectionLabel.textContent = gettext("Selected");

    function toggleDefaultSlide(containerId, value = null, forceSelect = false) {
        const oldValue = defaultSlidesElement.value;
        if (oldValue) {
            const button = document.getElementById(containerId + "-default-button-" + oldValue);
            button.classList.remove("text-success");
        }

        if (value && (forceSelect || value !== oldValue)) {
            const button = document.getElementById(containerId + "-default-button-" + value);
            button.classList.add("text-success");
        } else {
            value = "";
        }

        defaultSlidesElement.value = value;
    }

    jQuery(slidesElement).multiSelect({
        selectableHeader: selectableSearch,
        selectionHeader: selectionSearch,
        selectableFooter: selectableLabel,
        selectionFooter: selectionLabel,
        afterInit() {
            const $container = this.$container;
            const $selectableUl = this.$selectableUl;
            const $selectionUl = this.$selectionUl;

            function prepareElement(element) {
                const children = Array.from(element.childNodes);
                element.innerText = "";

                const container = document.createElement("div");
                container.classList.add("d-flex", "justify-content-between", "align-items-center");
                element.append(container);

                const wrapper = document.createElement("div");
                wrapper.classList.add("d-block", "text-truncate", "mr-1");
                wrapper.append(...children);
                container.append(wrapper);

                return container;
            }

            function addShowButton(element, href) {
                const button = document.createElement("a");
                button.classList.add("btn", "p-0", "border-0");
                button.href = href;
                button.target = "_blank";
                button.title = gettext("Show");
                button.addEventListener("click", event => event.cancelBubble = true);
                jQuery(button).tooltip({ placement: "top", trigger: "hover" });
                element.append(button);

                const icon = document.createElement("em");
                icon.classList.add("fas", "fa-eye");
                button.append(icon);
            }

            function addDefaultButton(element, value) {
                const button = document.createElement("button");
                button.id = $container.attr("id") + "-default-button-" + value;
                button.classList.add("ml-2", "btn", "p-0", "border-0");
                button.title = gettext("Set as initial slides");
                button.type = "button";
                button.addEventListener("click", event => {
                    event.cancelBubble = true;
                    toggleDefaultSlide($container.attr("id"), value);
                });
                jQuery(button).tooltip({ placement: "top", trigger: "hover" });
                element.append(button);

                const icon = document.createElement("em");
                icon.classList.add("fas", "fa-check-circle");
                button.append(icon);
            }

            $selectableUl.children().each((_, element) => {
                const container = prepareElement(element);
                addShowButton(container, element.getAttribute("href"));
            });
            $selectionUl.children().each((_, element) => {
                const container = document.createElement("div");
                container.classList.add("d-flex", "align-items-center");
                prepareElement(element).append(container);

                addShowButton(container, element.getAttribute("href"));
                addDefaultButton(container, jQuery(element).data("ms-value"));
            });

            toggleDefaultSlide($container.attr("id"), defaultSlidesElement.value, true);

            selectableSearch.addEventListener("keyup", () => {
                $selectableUl.children().each(
                    (_, element) => {
                        element.hidden = !element.querySelector("span").textContent.includes(selectableSearch.value);
                    }
                );
            });

            selectionSearch.addEventListener("keyup", () => {
                $selectionUl.children().each(
                    (_, element) => {
                        element.hidden = !element.querySelector("span").textContent.includes(selectionSearch.value);
                    }
                );
            });
        },
        afterDeselect(elementValues) {
            if (defaultSlidesElement.value && elementValues.includes(defaultSlidesElement.value)) {
                toggleDefaultSlide(this.$container.attr("id"));
            }
        }
    });
}
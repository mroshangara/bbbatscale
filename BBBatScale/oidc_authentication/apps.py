from django.apps import AppConfig


class OidcAuthenticationConfig(AppConfig):
    name = "oidc_authentication"

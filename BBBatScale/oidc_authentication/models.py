import json
from typing import Any

from django.contrib.sites.models import SITE_CACHE, Site
from django.core.exceptions import ValidationError
from django.db import models
from django.db.models.signals import pre_delete, pre_save
from django.dispatch import receiver
from django.utils.translation import gettext_lazy as _


def validate_dict(value: Any) -> None:
    if not isinstance(value, dict):
        raise ValidationError(
            _("%(json_value)s is not a dict"), params={"value": value, "json_value": json.dumps(value)}
        )


class AuthParameter(models.Model):
    tenant = models.OneToOneField(
        Site, on_delete=models.CASCADE, verbose_name=_("Tenant"), related_name="auth_parameter"
    )
    client_id = models.CharField(max_length=255, verbose_name=_("Client ID"))
    client_secret = models.CharField(max_length=255, verbose_name=_("Client secret"))
    authorization_endpoint = models.CharField(max_length=255, verbose_name=_("Authorization endpoint"))
    token_endpoint = models.CharField(max_length=255, verbose_name=_("Token endpoint"))
    user_endpoint = models.CharField(max_length=255, verbose_name=_("User endpoint"))
    jwks_endpoint = models.CharField(max_length=255, verbose_name=_("JWKS endpoint"))
    end_session_endpoint = models.CharField(max_length=255, verbose_name=_("End session endpoint"))
    scopes = models.CharField(max_length=255, verbose_name=_("Scopes"), default="openid profile email")
    verify_ssl = models.BooleanField(verbose_name=_("Verify SSL"), default=True)
    sign_algo = models.CharField(max_length=255, verbose_name=_("Sign algo"), default="RS256")
    logout_url_method = models.CharField(
        max_length=255,
        verbose_name=_("Logout url method"),
        default="oidc_authentication.auth.provider_logout",
        help_text=_("Method import path (e.g. oidc_authentication.auth.provider_logout)"),
    )
    idp_sign_key = models.CharField(max_length=255, verbose_name=_("IDP sign key"), null=True, blank=True)
    refresh_enabled = models.BooleanField(verbose_name=_("Refresh enabled"), default=True)
    renew_id_token_expiry_seconds = models.PositiveIntegerField(
        verbose_name=_("Renew id token expiry seconds"), default=900, help_text=_("In seconds")
    )
    state_size = models.IntegerField(
        default=32,
        verbose_name=_("State size"),
        help_text=_("Sets the length of the random string used for OpenID Connect state verification"),
    )
    use_nonce = models.BooleanField(default=True, verbose_name=_("Use nonce"))
    nonce_size = models.IntegerField(default=32, verbose_name=_("Nonce size"))
    allow_unsecured_jwt = models.BooleanField(default=False, verbose_name=_("Allow unsecured JWT"))
    timeout = models.PositiveIntegerField(
        verbose_name=_("Timeout"),
        help_text=_("Defines a timeout for all requests to the OIDC provider."),
        blank=True,
        null=True,
    )
    create_user = models.BooleanField(
        default=True,
        verbose_name=_("Create user"),
        help_text=_("Enables or disables automatic user creation during authentication"),
    )
    oidc_username_standard = models.BooleanField(
        default=True,
        verbose_name=_("Use oidc username standard"),
        help_text=_('The oidc standard uses "sub" in combination with "iss" as username.'),
    )
    claim_unique_username_field = models.CharField(
        max_length=255,
        default="preferred_username",
        verbose_name=_("Claim unique username field"),
        help_text=_(
            "If the OIDC standard is not used for the username, you have to provide the claim field's name which will "
            "be combined with the domain of the corresponding tenant to create a unique username "
            "(e.g. preferred_username@domain). The provided claim field needs to be unique "
            "across the used tenant."
        ),
    )
    proxy = models.JSONField(
        blank=True,
        null=True,
        verbose_name=_("Proxy"),
        help_text=_(
            "Defines a proxy for all requests to the OpenID Connect provider"
            " (fetch JWS, retrieve JWT tokens, Userinfo Endpoint)."
            " An empty value means that no proxy will be used and a direct connection will be established."
            " For configuring a proxy check the Python requests documentation and supply it as JSON Object string:"
            " https://requests.readthedocs.io/en/master/user/advanced/#proxies"
        ),
        validators=[validate_dict],
    )
    token_use_basic_auth = models.BooleanField(
        default=False,
        verbose_name=_("Use HTTP Basic Authentication"),
        help_text=_("Use HTTP Basic Authentication instead of sending the client secret in token request POST body."),
    )

    def __str__(self):
        return self.__class__.__name__ + "(tenant=" + repr(self.tenant.name) + ")"


@receiver((pre_save, pre_delete), sender=AuthParameter)
def clear_site_cache_via_auth_parameter(instance, using, **_kwargs):
    """
    Clear the site cache (if primed) each time a auth parameter is saved or deleted.
    """

    try:
        del SITE_CACHE[instance.tenant_id]
    except KeyError:
        pass
    try:
        del SITE_CACHE[Site.objects.using(using).get(pk=instance.tenant_id).domain]
    except (KeyError, Site.DoesNotExist):
        pass

# Generated by Django 3.2.12 on 2022-03-28 11:50

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("support_chat", "0007_supportchatparameter_allow_supporter_to_inactivate_all"),
    ]

    operations = [
        migrations.AlterField(
            model_name="supportchatparameter",
            name="allow_supporter_to_inactivate_all",
            field=models.BooleanField(default=False, verbose_name="Allow supporter to inactivate all supporters"),
        ),
        migrations.AlterField(
            model_name="supportchatparameter",
            name="disable_chat_if_offline",
            field=models.BooleanField(default=False, verbose_name="Disable Support Chat if offline"),
        ),
        migrations.AlterField(
            model_name="supportchatparameter",
            name="enabled",
            field=models.BooleanField(default=False, verbose_name="Enable Support Chat"),
        ),
        migrations.AlterField(
            model_name="supportchatparameter",
            name="message_max_length",
            field=models.IntegerField(default=255, verbose_name="Maximum message length"),
        ),
    ]

from core.models import User
from django import template
from django.contrib.sites.shortcuts import get_current_site
from django.template import RequestContext
from support_chat.utils import can_inactivate_all_supporters

register = template.Library()


@register.simple_tag(name="can_inactivate_all_supporters", takes_context=True)
def do_can_inactivate_all_supporters(context: RequestContext, user: User) -> bool:
    return can_inactivate_all_supporters(get_current_site(context.request), user)

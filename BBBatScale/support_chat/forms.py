from crispy_forms.helper import FormHelper
from crispy_forms.layout import Div, Field, Layout, Submit
from django import forms
from django.utils.translation import gettext_lazy as _
from support_chat.models import SupportChatParameter


class SupportChatParameterForm(forms.ModelForm):
    class Meta:
        model = SupportChatParameter
        fields = [
            "enabled",
            "disable_chat_if_offline",
            "allow_supporter_to_inactivate_all",
            "message_max_length",
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.helper = FormHelper()
        self.helper.form_class = "form-horizontal"
        self.helper.label_class = "col-lg-4"
        self.helper.field_class = "col-lg-8"
        self.helper.layout = Layout(
            Field("enabled"),
            Div(
                Field("disable_chat_if_offline"),
                Field("allow_supporter_to_inactivate_all"),
                Field("message_max_length"),
                css_id="settings_container",
            ),
            Submit("save", _("Save"), css_class="btn-primary"),
        )

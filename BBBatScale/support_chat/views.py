import json
from functools import wraps

from core.decorators import tenant_based_permission_required
from django.contrib.auth.decorators import login_required
from django.contrib.sites.shortcuts import get_current_site
from django.db import DataError
from django.http import Http404, HttpResponse
from django.shortcuts import redirect, render
from django.views.decorators.http import require_http_methods
from support_chat.forms import SupportChatParameterForm
from support_chat.models import PreparedAnswer, SupportChatParameter, Supporter


def support_chat_enabled(view_func):
    @wraps(view_func)
    def _wrapped_view(request, *args, **kwargs):
        if SupportChatParameter.objects.load(get_current_site(request)).enabled:
            return view_func(request, *args, **kwargs)

        raise Http404

    return _wrapped_view


@support_chat_enabled
@login_required
@tenant_based_permission_required("support_chat.support_users", raise_exception=True)
def chats_overview(request) -> HttpResponse:
    prepared_answers = [
        {"title": prepared_answer.title, "text": prepared_answer.text}
        for prepared_answer in PreparedAnswer.objects.filter(tenant=get_current_site(request))
    ]

    return render(request, "support_chat/chats_overview.html", {"prepared_answers": json.dumps(prepared_answers)})


@support_chat_enabled
@login_required
@tenant_based_permission_required("support_chat.support_users", raise_exception=True)
@require_http_methods(["GET", "POST"])
def prepared_answers_overview(request) -> HttpResponse:
    tenant = get_current_site(request)

    if request.method == "POST":
        try:
            if "id" in request.POST:
                prepared_answers = PreparedAnswer.objects.get(id=int(request.POST["id"]))

                if "title" in request.POST and "text" in request.POST:
                    prepared_answers.title = request.POST["title"]
                    prepared_answers.text = request.POST["text"]
                    prepared_answers.save()
                else:
                    prepared_answers.delete()
            else:
                PreparedAnswer.objects.create(title=request.POST["title"], text=request.POST["text"], tenant=tenant)
        except (ValueError, KeyError, PreparedAnswer.DoesNotExist, DataError):
            pass
        return redirect("support_chat_prepared_answers_overview")

    return render(
        request,
        "support_chat/prepared_answers_overview.html",
        {"prepared_answers": list(PreparedAnswer.objects.filter(tenant=tenant))},
    )


@support_chat_enabled
@login_required
@tenant_based_permission_required("support_chat.support_users", raise_exception=True)
def supporters_overview(request) -> HttpResponse:
    active_supporters = []
    inactive_supporters = []
    for supporter in (
        Supporter.objects.filter_support_eligible(get_current_site(request)).prefetch_related("user").distinct()
    ):
        if supporter.is_status_active:
            active_supporters.append(str(supporter))
        else:
            inactive_supporters.append(str(supporter))

    return render(
        request,
        "support_chat/supporters_overview.html",
        {"active_supporters": active_supporters, "inactive_supporters": inactive_supporters},
    )


@login_required
@tenant_based_permission_required(
    ["support_chat.view_supportchatparameter", "support_chat.change_supportchatparameter"], raise_exception=True
)
@require_http_methods(["GET", "POST"])
def settings(request) -> HttpResponse:
    form = SupportChatParameterForm(
        request.POST or None, instance=SupportChatParameter.objects.load(get_current_site(request))
    )

    if request.method == "POST" and form.is_valid():
        form.save()
        return redirect("support_chat_settings")

    return render(request, "support_chat/settings.html", {"form": form})

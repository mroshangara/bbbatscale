[[source]]
url = "https://pypi.org/simple"
verify_ssl = true
name = "pypi"

[packages]
asgiref = "~=3.2"
beautifulsoup4 = "~=4.9"
channels = "~=2.4"
channels-redis = "~=2.4"
django = "~=3.2"
django-crispy-forms = "~=1.9"
django-filter = "~=2.4"
django-guid = "~=3.2"
django-rq = "~=2.3"
exchangelib = "~=4.4"
googleapis-common-protos = "~=1.56"
graypy = "~=2.1"
grpcio = "~=1.44"
icalendar = "~=4.0"
mozilla-django-oidc = "~=1.2"
paramiko = "~=2.7"
protobuf = "~=3.20"
psycopg2-binary = "~=2.8"
python-json-logger = "~=2.0"
recurring-ical-events = "==1.0.2b0"
requests = "~=2.24"
responses = "~=0.12"
rq = "==1.8.1" # Pinning version, since newer versions keep freezing. Pinning rq can be removed as soon as it runs stable again.
tendo = "~=0.2"
uvicorn = "~=0.17"
websockets = "~=10.3"
whitenoise = "~=5.2"
xmltodict = "~=0.12"

[dev-packages]
black = { version = "~=22.1", extras = ["d"] }
faker = "~=8.8"
flake8 = "~=3.8"
freezegun = "~=1.2"
isort = "~=5.8"
pytest = "~=6.1"
pytest-asyncio = "~=0.14"
pytest-cov = "~=2.10"
pytest-django = "~=4.4"
pytest-mock = "~=3.3"

[requires]
python_version = "3.9"

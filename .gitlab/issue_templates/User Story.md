# Description

**As a** _[User, Developer, Maintainer, Stakeholder, ...]_

**I want to** _[goal of the issue]_

**so that** _[motivation of this issue]_

# Details
_[A detailed description of the issue to tackle]_

# Acceptance criteria
- [ ] _[Acceptance criteria one]_
- [ ] _[Acceptance criteria two]_
- [ ] _[Acceptance criteria three]_

# Description

_[A description of the bug]_

# Expected behaviour
_[The behaviour expected when reproducing the steps leading to the bug]_

# Steps to reproduce
1. _[Step one]_
2. _[Step two]_
3. _[Step three]_

# Acceptance criteria
- [ ] _[Create issue]_
- [ ] _[Create spike]_
- [ ] _[Resolve the bug by its own - Acceptance criteria one]_
- [ ] _[Resolve the bug by its own - Acceptance criteria two]_
- [ ] _[Resolve the bug by its own - Acceptance criteria three]_

stages:
  - verify-contribution-compliance
  - build
  - test
  - analyse
  - review
  - release

variables:
  BBBATSCALE_IMAGE: $CI_REGISTRY_IMAGE/bbbatscale
  NGINX_IMAGE: $CI_REGISTRY_IMAGE/nginx

workflow:
  # The pipeline is only executed for branches (e.g. feature branches) and merge requests.
  # It is explicitly not executed for release commits and commits with a message starting with "wip".
  rules:
    - if: $CI_COMMIT_MESSAGE =~ /^wip/i
      when: never
    # RegEx checking if the commit is a release commit.
    # The RegEx to check the version string has been retrieved from https://semver.org/#is-there-a-suggested-regular-expression-regex-to-check-a-semver-string

    # Do not run a pipeline on the alpha-branch if the release commit is for an alpha, beta, or stable release.
    - if: >-
        $CI_COMMIT_BRANCH == "alpha" &&
        $CI_COMMIT_MESSAGE =~ /^release\((?P<major>0|[1-9]\d*)\.(?P<minor>0|[1-9]\d*)\.(?P<patch>0|[1-9]\d*)(?:-(?P<prerelease>(?:alpha|beta)(?:\.(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*))*))?(?:\+(?P<buildmetadata>[0-9a-zA-Z-]+(?:\.[0-9a-zA-Z-]+)*))?\): /
      when: never
    # Do not run a pipeline on the beta-branch if the release commit is for a beta or stable release.
    - if: >-
        $CI_COMMIT_BRANCH == "beta" &&
        $CI_COMMIT_MESSAGE =~ /^release\((?P<major>0|[1-9]\d*)\.(?P<minor>0|[1-9]\d*)\.(?P<patch>0|[1-9]\d*)(?:-(?P<prerelease>(?:beta)(?:\.(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*))*))?(?:\+(?P<buildmetadata>[0-9a-zA-Z-]+(?:\.[0-9a-zA-Z-]+)*))?\): /
      when: never
    # Do not run a pipeline on the main-branch if the release commit is for a stable release.
    - if: >-
        $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH &&
        $CI_COMMIT_MESSAGE =~ /^release\((?P<major>0|[1-9]\d*)\.(?P<minor>0|[1-9]\d*)\.(?P<patch>0|[1-9]\d*)(?:\+(?P<buildmetadata>[0-9a-zA-Z-]+(?:\.[0-9a-zA-Z-]+)*))?\): /
      when: never
    # Do not run a pipeline on any other branch for any release.
    - if: >-
        $CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH &&
        $CI_COMMIT_BRANCH != "alpha" &&
        $CI_COMMIT_BRANCH != "beta" &&
        $CI_COMMIT_MESSAGE =~ /^release\((?P<major>0|[1-9]\d*)\.(?P<minor>0|[1-9]\d*)\.(?P<patch>0|[1-9]\d*)(?:-(?P<prerelease>(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*)(?:\.(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*))*))?(?:\+(?P<buildmetadata>[0-9a-zA-Z-]+(?:\.[0-9a-zA-Z-]+)*))?\): /
      when: never
    - if: $CI_COMMIT_BRANCH
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"

include:
  - template: Security/Dependency-Scanning.gitlab-ci.yml
  - template: Security/License-Scanning.gitlab-ci.yml
  - template: Security/Secret-Detection.gitlab-ci.yml
  - local: .gitlab/utils/utils.gitlab-ci.yml
  - local: .gitlab/stages/verify-contribution-compliance.gitlab-ci.yml
  - local: .gitlab/stages/build.gitlab-ci.yml
  - local: .gitlab/stages/pytest.gitlab-ci.yml
  - local: .gitlab/stages/analyse.gitlab-ci.yml
  - local: .gitlab/stages/review.gitlab-ci.yml
  - local: .gitlab/stages/release.gitlab-ci.yml
